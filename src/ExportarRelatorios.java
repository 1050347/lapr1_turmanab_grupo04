import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class ExportarRelatorios {

    static final String CABECALHOFICHEIRODIARIO = "data,diario_infetado,diario_hospitalizado,diario_internado_UCI,diario_mortes";
    static final String CABECALHOFICHEIROACUMULADO = "data,acumulado_infetado,acumulado_hospitalizado,acumulado_internado_UCI,acumulado_mortes";
    static final String CABECALHOFICHEIROCOMPARATIVOTOTALDIA = "data.P1,diario_infetado.P1,diario_hospitalizado.P1,diario_internado_UCI.P1,diario_mortes.P1,data.P2,diario_infetado.P2,diario_hospitalizado.P2,diario_internado_UCI.P2,diario_mortes.P2";
    static final String CABECALHOFICHEIROCOMPARATIVOACUMULADO = "data.P1,acumulado_infetado.P1,acumulado_hospitalizado.P1,acumulado_internado_UCI.P1,acumulado_mortes.P1,data.P2,acumulado_infetado.P2,acumulado_hospitalizado.P2,acumulado_internado_UCI.P2,acumulado_mortes.P2";
    static final String CABECALHOCADEIASDEMARKOV = "infetados,hospitalizados,internados_UCI,mortes";
    static final String CABECALHOFATORIZACAODECROUT = "infetados,hospitalizados,internados_UCI,mortes";


    static Scanner ler = new Scanner(System.in);


    //********************************************************************************************
    //*****Metodo para exportar o relatorio do numero casos dia/semana****************************
    //********************************************************************************************

    public static void exportarRelatorioNumeroCasos_Dia(int[][] matrizDadosMenu) throws FileNotFoundException {

        System.out.println("Insira o nome do ficheiro a ser gerado:");
        String nomeFicheiro = ler.next();

        int tipoRelatorio = matrizDadosMenu[1][0];

        switch (tipoRelatorio) {
            case 1:
                File ficheiro = new File(nomeFicheiro + ".csv");
                PrintWriter gravar = new PrintWriter(ficheiro);
                gravar.println(CABECALHOFICHEIRODIARIO);

                int indiceLinha = CalcularIndiceRelatorios.calcularIndiceRelatorioNumeroCasos_DiaSemana(AppCovanalytics.dadosCovidDiaFinal, AbrirMenus.matrizDadosMenu[3][0]);

                gravar.print(AppCovanalytics.dadosCovidDiaFinal[indiceLinha][0]);
                gravar.print(",");
                gravar.print(AppCovanalytics.dadosCovidDiaFinal[indiceLinha][2]);
                gravar.print(",");
                gravar.print(AppCovanalytics.dadosCovidDiaFinal[indiceLinha][3]);
                gravar.print(",");
                gravar.print(AppCovanalytics.dadosCovidDiaFinal[indiceLinha][4]);
                gravar.print(",");
                gravar.print(AppCovanalytics.dadosCovidDiaFinal[indiceLinha][5]);
                gravar.close();
                break;

            case 2:
                ficheiro = new File(nomeFicheiro + ".csv");
                gravar = new PrintWriter(ficheiro);
                gravar.println(CABECALHOFICHEIROACUMULADO);

                indiceLinha = CalcularIndiceRelatorios.calcularIndiceRelatorioNumeroCasos_DiaSemana(AppCovanalytics.dadosCovidAcumuladosFinal, AbrirMenus.matrizDadosMenu[3][0]);

                gravar.print(AppCovanalytics.dadosCovidAcumuladosFinal[indiceLinha][0]);
                gravar.print(",");
                gravar.print(AppCovanalytics.dadosCovidAcumuladosFinal[indiceLinha][2]);
                gravar.print(",");
                gravar.print(AppCovanalytics.dadosCovidAcumuladosFinal[indiceLinha][3]);
                gravar.print(",");
                gravar.print(AppCovanalytics.dadosCovidAcumuladosFinal[indiceLinha][4]);
                gravar.print(",");
                gravar.print(AppCovanalytics.dadosCovidAcumuladosFinal[indiceLinha][5]);
                gravar.close();
                break;

        }
    }

    //********************************************************************************************
    //*****Metodo para exportar o relatorio do numero casos num intervalo de dias*****************
    //********************************************************************************************

    public static void exportarRelatorioNumeroCasos_IntervaloDias(int[][] matrizDadosMenu) throws FileNotFoundException {

        System.out.println("Insira o nome do ficheiro a ser gerado:");
        String nomeFicheiro = ler.next();

        int tipoRelatorio = matrizDadosMenu[1][0];

        switch (tipoRelatorio) {
            case 1:
                File ficheiro = new File(nomeFicheiro + ".csv");
                PrintWriter gravar = new PrintWriter(ficheiro);
                gravar.println(CABECALHOFICHEIRODIARIO);

                int[] indiceLinha = CalcularIndiceRelatorios.calcularIndiceRelatorioNumeroCasos_IntervaloDias(AppCovanalytics.dadosCovidDiaFinal, AppCovanalytics.contaLinhasDadosDiaFinal, AbrirMenus.matrizDadosMenu[3][0], AbrirMenus.matrizDadosMenu[3][1]);

                for (int linha = indiceLinha[0]; linha <= indiceLinha[1]; linha++) {
                    gravar.print(AppCovanalytics.dadosCovidDiaFinal[linha][0]);
                    gravar.print(",");
                    gravar.print(AppCovanalytics.dadosCovidDiaFinal[linha][2]);
                    gravar.print(",");
                    gravar.print(AppCovanalytics.dadosCovidDiaFinal[linha][3]);
                    gravar.print(",");
                    gravar.print(AppCovanalytics.dadosCovidDiaFinal[linha][4]);
                    gravar.print(",");
                    gravar.print(AppCovanalytics.dadosCovidDiaFinal[linha][5]);
                    gravar.println();
                }
                gravar.print("somatorio do periodo");
                gravar.print(",");
                gravar.print(ImprimirRelatorios.calcularSomaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceLinha, 2));
                gravar.print(",");
                gravar.print(ImprimirRelatorios.calcularSomaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceLinha, 3));
                gravar.print(",");
                gravar.print(ImprimirRelatorios.calcularSomaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceLinha, 4));
                gravar.print(",");
                gravar.print(ImprimirRelatorios.calcularSomaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceLinha, 5));
                gravar.println();

                gravar.print("media do periodo");
                gravar.print(",");
                gravar.print(ImprimirRelatorios.calcularMediaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceLinha, 2));
                gravar.print(",");
                gravar.print(ImprimirRelatorios.calcularMediaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceLinha, 3));
                gravar.print(",");
                gravar.print(ImprimirRelatorios.calcularMediaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceLinha, 4));
                gravar.print(",");
                gravar.print(ImprimirRelatorios.calcularMediaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceLinha, 5));

                gravar.close();
                break;

            case 2:
                ficheiro = new File(nomeFicheiro + ".csv");
                gravar = new PrintWriter(ficheiro);
                gravar.println(CABECALHOFICHEIROACUMULADO);

                indiceLinha = CalcularIndiceRelatorios.calcularIndiceRelatorioNumeroCasos_IntervaloDias(AppCovanalytics.dadosCovidAcumuladosFinal, AppCovanalytics.contaLinhasDadosAcumuladosFinal, AbrirMenus.matrizDadosMenu[3][0], AbrirMenus.matrizDadosMenu[3][1]);


                for (int linha = indiceLinha[0]; linha <= indiceLinha[1]; linha++) {
                    gravar.print(AppCovanalytics.dadosCovidAcumuladosFinal[linha][0]);
                    gravar.print(",");
                    gravar.print(AppCovanalytics.dadosCovidAcumuladosFinal[linha][2]);
                    gravar.print(",");
                    gravar.print(AppCovanalytics.dadosCovidAcumuladosFinal[linha][3]);
                    gravar.print(",");
                    gravar.print(AppCovanalytics.dadosCovidAcumuladosFinal[linha][4]);
                    gravar.print(",");
                    gravar.print(AppCovanalytics.dadosCovidAcumuladosFinal[linha][5]);
                    gravar.println();
                }
                gravar.close();
                break;
        }

    }

    //********************************************************************************************
    //*****Metodo para exportar o relatorio do numero casos agrupado semanal**********************
    //********************************************************************************************

    public static void exportarRelatorioNumeroCasos_Semana(int[][] matrizDadosMenu) throws FileNotFoundException {

        System.out.println("Insira o nome do ficheiro a ser gerado:");
        String nomeFicheiro = ler.next();

        int tipoRelatorio = matrizDadosMenu[1][0];
        int duracaoSemana = 7;
        int contadorLinhas = 0;


        switch (tipoRelatorio) {
            case 1:
                File ficheiro = new File(nomeFicheiro + ".csv");
                PrintWriter gravar = new PrintWriter(ficheiro);
                gravar.println(CABECALHOFICHEIRODIARIO);

                int indiceLinha = CalcularIndiceRelatorios.calcularIndiceRelatorioNumeroCasos_DiaSemana(AppCovanalytics.dadosCovidDiaFinal, AbrirMenus.matrizDadosMenu[3][0]);
                duracaoSemana = duracaoSemana - ImprimirRelatorios.calcularNumeroDiasDescontarSemana(AbrirMenus.matrizDadosMenu[3][0]);

                while (contadorLinhas < duracaoSemana && indiceLinha < AppCovanalytics.contaLinhasDadosDiaFinal) {
                    gravar.print(AppCovanalytics.dadosCovidDiaFinal[indiceLinha][0]);
                    gravar.print(",");
                    gravar.print(AppCovanalytics.dadosCovidDiaFinal[indiceLinha][2]);
                    gravar.print(",");
                    gravar.print(AppCovanalytics.dadosCovidDiaFinal[indiceLinha][3]);
                    gravar.print(",");
                    gravar.print(AppCovanalytics.dadosCovidDiaFinal[indiceLinha][4]);
                    gravar.print(",");
                    gravar.print(AppCovanalytics.dadosCovidDiaFinal[indiceLinha][5]);
                    gravar.println();
                    indiceLinha++;
                    contadorLinhas++;
                }
                gravar.close();
                break;

            case 2:
                ficheiro = new File(nomeFicheiro + ".csv");
                gravar = new PrintWriter(ficheiro);
                gravar.println(CABECALHOFICHEIROACUMULADO);

                indiceLinha = CalcularIndiceRelatorios.calcularIndiceRelatorioNumeroCasos_DiaSemana(AppCovanalytics.dadosCovidAcumuladosFinal, AbrirMenus.matrizDadosMenu[3][0]);
                duracaoSemana = duracaoSemana - ImprimirRelatorios.calcularNumeroDiasDescontarSemana(AbrirMenus.matrizDadosMenu[3][0]);

                while (contadorLinhas < duracaoSemana && indiceLinha < AppCovanalytics.contaLinhasDadosAcumuladosFinal) {

                    gravar.print(AppCovanalytics.dadosCovidAcumuladosFinal[indiceLinha][0]);
                    gravar.print(",");
                    gravar.print(AppCovanalytics.dadosCovidAcumuladosFinal[indiceLinha][2]);
                    gravar.print(",");
                    gravar.print(AppCovanalytics.dadosCovidAcumuladosFinal[indiceLinha][3]);
                    gravar.print(",");
                    gravar.print(AppCovanalytics.dadosCovidAcumuladosFinal[indiceLinha][4]);
                    gravar.print(",");
                    gravar.print(AppCovanalytics.dadosCovidAcumuladosFinal[indiceLinha][5]);
                    gravar.println();
                    indiceLinha++;
                    contadorLinhas++;
                }
                gravar.close();
                break;
        }

    }


    //********************************************************************************************
    //*****Metodo para exportar o relatorio do numero casos agrupado mensalmente******************
    //********************************************************************************************

    public static void exportarRelatorioNumeroCasos_Mes(int[][] matrizDadosMenu) throws FileNotFoundException {

        System.out.println("Insira o nome do ficheiro a ser gerado:");
        String nomeFicheiro = ler.next();

        int tipoRelatorio = matrizDadosMenu[1][0];

        switch (tipoRelatorio) {
            case 1:
                File ficheiro = new File(nomeFicheiro + ".csv");
                PrintWriter gravar = new PrintWriter(ficheiro);
                gravar.println(CABECALHOFICHEIRODIARIO);

                int[] indiceIntervaloMes = CalcularIndiceRelatorios.calcularIndiceRelatorioNumeroCasos_Mes(AppCovanalytics.dadosCovidDiaFinal, AbrirMenus.matrizDadosMenu[3][0]);


                gravar.print((AppCovanalytics.dadosCovidDiaFinal[indiceIntervaloMes[0]][0]) / 100);
                gravar.print(",");
                gravar.print(ImprimirRelatorios.calcularSomaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceIntervaloMes, 2));
                gravar.print(",");
                gravar.print(ImprimirRelatorios.calcularSomaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceIntervaloMes, 3));
                gravar.print(",");
                gravar.print(ImprimirRelatorios.calcularSomaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceIntervaloMes, 4));
                gravar.print(",");
                gravar.print(ImprimirRelatorios.calcularSomaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceIntervaloMes, 5));
                gravar.println();

                gravar.close();
                break;

            case 2:
                ficheiro = new File(nomeFicheiro + ".csv");
                gravar = new PrintWriter(ficheiro);
                gravar.println(CABECALHOFICHEIROACUMULADO);

                indiceIntervaloMes = CalcularIndiceRelatorios.calcularIndiceRelatorioNumeroCasos_Mes(AppCovanalytics.dadosCovidAcumuladosFinal, AbrirMenus.matrizDadosMenu[3][0]);

                gravar.print((AppCovanalytics.dadosCovidAcumuladosFinal[indiceIntervaloMes[0]][0]) / 100);
                gravar.print(",");
                gravar.print(AppCovanalytics.dadosCovidAcumuladosFinal[indiceIntervaloMes[1]][2]);
                gravar.print(",");
                gravar.print(AppCovanalytics.dadosCovidAcumuladosFinal[indiceIntervaloMes[1]][3]);
                gravar.print(",");
                gravar.print(AppCovanalytics.dadosCovidAcumuladosFinal[indiceIntervaloMes[1]][4]);
                gravar.print(",");
                gravar.print(AppCovanalytics.dadosCovidAcumuladosFinal[indiceIntervaloMes[1]][5]);
                gravar.println();

                gravar.close();
                break;
        }

    }


    //********************************************************************************************
    //*****Metodo para exportar o relatorio com comparação de períodos****************************
    //********************************************************************************************

    public static void exportarRelatorioComparativo_Dias(int[][] matrizDadosMenu) throws FileNotFoundException {

        System.out.println("Insira o nome do ficheiro a ser gerado:");
        String nomeFicheiro = ler.next();

        int tipoRelatorio = matrizDadosMenu[1][0];

        switch (tipoRelatorio) {
            case 1:
                File ficheiro = new File(nomeFicheiro + ".csv");
                PrintWriter gravar = new PrintWriter(ficheiro);
                gravar.println(CABECALHOFICHEIROCOMPARATIVOTOTALDIA);


                int[] indiceIntervaloMes = CalcularIndiceRelatorios.calcularIndiceRelatorioComparativo_IntervaloDias(AppCovanalytics.dadosCovidDiaFinal, AppCovanalytics.contaLinhasDadosDiaFinal, matrizDadosMenu[3][0], matrizDadosMenu[3][1], matrizDadosMenu[3][2], matrizDadosMenu[3][3]);

                int contadorP1 = indiceIntervaloMes[0];
                int contadorP2 = indiceIntervaloMes[2];


                while (contadorP1 <= indiceIntervaloMes[1] || contadorP2 <= indiceIntervaloMes[3]) {

                    if (contadorP1 > indiceIntervaloMes[1]) {
                        gravar.print("-");
                        gravar.print(",");
                        gravar.print("-");
                        gravar.print(",");
                        gravar.print("-");
                        gravar.print(",");
                        gravar.print("-");
                        gravar.print(",");
                        gravar.print("-");
                        gravar.print(",");
                    } else {
                        gravar.print(AppCovanalytics.dadosCovidDiaFinal[contadorP1][0]);
                        gravar.print(",");
                        gravar.print(AppCovanalytics.dadosCovidDiaFinal[contadorP1][2]);
                        gravar.print(",");
                        gravar.print(AppCovanalytics.dadosCovidDiaFinal[contadorP1][3]);
                        gravar.print(",");
                        gravar.print(AppCovanalytics.dadosCovidDiaFinal[contadorP1][4]);
                        gravar.print(",");
                        gravar.print(AppCovanalytics.dadosCovidDiaFinal[contadorP1][5]);
                        gravar.print(",");
                    }

                    if (contadorP2 > indiceIntervaloMes[3]) {
                        gravar.print("-");
                        gravar.print(",");
                        gravar.print("-");
                        gravar.print(",");
                        gravar.print("-");
                        gravar.print(",");
                        gravar.print("-");
                        gravar.print(",");
                        gravar.print("-");

                    } else {
                        gravar.print(AppCovanalytics.dadosCovidDiaFinal[contadorP2][0]);
                        gravar.print(",");
                        gravar.print(AppCovanalytics.dadosCovidDiaFinal[contadorP2][2]);
                        gravar.print(",");
                        gravar.print(AppCovanalytics.dadosCovidDiaFinal[contadorP2][3]);
                        gravar.print(",");
                        gravar.print(AppCovanalytics.dadosCovidDiaFinal[contadorP2][4]);
                        gravar.print(",");
                        gravar.print(AppCovanalytics.dadosCovidDiaFinal[contadorP2][5]);
                    }
                    gravar.println();
                    contadorP1++;
                    contadorP2++;
                }

                int[] indiceIntervaloMedia1 = new int[2];
                indiceIntervaloMedia1[0] = indiceIntervaloMes[0];
                indiceIntervaloMedia1[1] = indiceIntervaloMes[1];
                int[] indiceIntervaloMedia2 = new int[2];
                indiceIntervaloMedia2[0] = indiceIntervaloMes[2];
                indiceIntervaloMedia2[1] = indiceIntervaloMes[3];

                gravar.print("valores medios");
                gravar.print(",");
                gravar.print(ImprimirRelatorios.calcularMediaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceIntervaloMedia1, 2));
                gravar.print(",");
                gravar.print(ImprimirRelatorios.calcularMediaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceIntervaloMedia1, 3));
                gravar.print(",");
                gravar.print(ImprimirRelatorios.calcularMediaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceIntervaloMedia1, 4));
                gravar.print(",");
                gravar.print(ImprimirRelatorios.calcularMediaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceIntervaloMedia1, 5));
                gravar.print(",");

                gravar.print("valores medios");
                gravar.print(",");
                gravar.print(ImprimirRelatorios.calcularMediaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceIntervaloMedia2, 2));
                gravar.print(",");
                gravar.print(ImprimirRelatorios.calcularMediaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceIntervaloMedia2, 3));
                gravar.print(",");
                gravar.print(ImprimirRelatorios.calcularMediaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceIntervaloMedia2, 4));
                gravar.print(",");
                gravar.print(ImprimirRelatorios.calcularMediaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceIntervaloMedia2, 5));

                gravar.println();

                gravar.print("desvio padrao");
                gravar.print(",");
                gravar.print(ImprimirRelatorios.calcularDesvioPadraoIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceIntervaloMedia1, 2));
                gravar.print(",");
                gravar.print(ImprimirRelatorios.calcularDesvioPadraoIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceIntervaloMedia1, 3));
                gravar.print(",");
                gravar.print(ImprimirRelatorios.calcularDesvioPadraoIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceIntervaloMedia1, 4));
                gravar.print(",");
                gravar.print(ImprimirRelatorios.calcularDesvioPadraoIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceIntervaloMedia1, 5));
                gravar.print(",");
                gravar.print("desvio padrao");
                gravar.print(",");

                gravar.print(ImprimirRelatorios.calcularDesvioPadraoIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceIntervaloMedia2, 2));
                gravar.print(",");
                gravar.print(ImprimirRelatorios.calcularDesvioPadraoIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceIntervaloMedia2, 3));
                gravar.print(",");
                gravar.print(ImprimirRelatorios.calcularDesvioPadraoIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceIntervaloMedia2, 4));
                gravar.print(",");
                gravar.print(ImprimirRelatorios.calcularDesvioPadraoIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceIntervaloMedia2, 5));

                gravar.close();
                break;

            case 2:
                ficheiro = new File(nomeFicheiro + ".csv");
                gravar = new PrintWriter(ficheiro);
                gravar.println(CABECALHOFICHEIROCOMPARATIVOACUMULADO);

                indiceIntervaloMes = CalcularIndiceRelatorios.calcularIndiceRelatorioComparativo_IntervaloDias(AppCovanalytics.dadosCovidAcumuladosFinal, AppCovanalytics.contaLinhasDadosAcumuladosFinal, matrizDadosMenu[3][0], matrizDadosMenu[3][1], matrizDadosMenu[3][2], matrizDadosMenu[3][3]);


                contadorP1 = indiceIntervaloMes[0];
                contadorP2 = indiceIntervaloMes[2];

                while (contadorP1 <= indiceIntervaloMes[1] || contadorP2 <= indiceIntervaloMes[3]) {

                    if (contadorP1 > indiceIntervaloMes[1]) {
                        gravar.print("-");
                        gravar.print(",");
                        gravar.print("-");
                        gravar.print(",");
                        gravar.print("-");
                        gravar.print(",");
                        gravar.print("-");
                        gravar.print(",");
                        gravar.print("-");
                        gravar.print(",");
                    } else {
                        gravar.print(AppCovanalytics.dadosCovidAcumuladosFinal[contadorP1][0]);
                        gravar.print(",");
                        gravar.print(AppCovanalytics.dadosCovidAcumuladosFinal[contadorP1][2]);
                        gravar.print(",");
                        gravar.print(AppCovanalytics.dadosCovidAcumuladosFinal[contadorP1][3]);
                        gravar.print(",");
                        gravar.print(AppCovanalytics.dadosCovidAcumuladosFinal[contadorP1][4]);
                        gravar.print(",");
                        gravar.print(AppCovanalytics.dadosCovidAcumuladosFinal[contadorP1][5]);
                        gravar.print(",");
                    }

                    if (contadorP2 > indiceIntervaloMes[3]) {
                        gravar.print("-");
                        gravar.print(",");
                        gravar.print("-");
                        gravar.print(",");
                        gravar.print("-");
                        gravar.print(",");
                        gravar.print("-");
                        gravar.print(",");
                        gravar.print("-");

                    } else {
                        gravar.print(AppCovanalytics.dadosCovidAcumuladosFinal[contadorP2][0]);
                        gravar.print(",");
                        gravar.print(AppCovanalytics.dadosCovidAcumuladosFinal[contadorP2][2]);
                        gravar.print(",");
                        gravar.print(AppCovanalytics.dadosCovidAcumuladosFinal[contadorP2][3]);
                        gravar.print(",");
                        gravar.print(AppCovanalytics.dadosCovidAcumuladosFinal[contadorP2][4]);
                        gravar.print(",");
                        gravar.print(AppCovanalytics.dadosCovidAcumuladosFinal[contadorP2][5]);
                    }
                    gravar.println();
                    contadorP1++;
                    contadorP2++;
                }

                int[] indiceIntervaloMedia3 = new int[2];
                indiceIntervaloMedia3[0] = indiceIntervaloMes[0];
                indiceIntervaloMedia3[1] = indiceIntervaloMes[1];
                int[] indiceIntervaloMedia4 = new int[2];
                indiceIntervaloMedia4[0] = indiceIntervaloMes[2];
                indiceIntervaloMedia4[1] = indiceIntervaloMes[3];

                gravar.print("valores medios");
                gravar.print(",");
                gravar.print(ImprimirRelatorios.calcularMediaIntervaloDatas(AppCovanalytics.dadosCovidAcumuladosFinal, indiceIntervaloMedia3, 2));
                gravar.print(",");
                gravar.print(ImprimirRelatorios.calcularMediaIntervaloDatas(AppCovanalytics.dadosCovidAcumuladosFinal, indiceIntervaloMedia3, 3));
                gravar.print(",");
                gravar.print(ImprimirRelatorios.calcularMediaIntervaloDatas(AppCovanalytics.dadosCovidAcumuladosFinal, indiceIntervaloMedia3, 4));
                gravar.print(",");
                gravar.print(ImprimirRelatorios.calcularMediaIntervaloDatas(AppCovanalytics.dadosCovidAcumuladosFinal, indiceIntervaloMedia3, 5));
                gravar.print(",");

                gravar.print("valores medios");
                gravar.print(",");
                gravar.print(ImprimirRelatorios.calcularMediaIntervaloDatas(AppCovanalytics.dadosCovidAcumuladosFinal, indiceIntervaloMedia4, 2));
                gravar.print(",");
                gravar.print(ImprimirRelatorios.calcularMediaIntervaloDatas(AppCovanalytics.dadosCovidAcumuladosFinal, indiceIntervaloMedia4, 3));
                gravar.print(",");
                gravar.print(ImprimirRelatorios.calcularMediaIntervaloDatas(AppCovanalytics.dadosCovidAcumuladosFinal, indiceIntervaloMedia4, 4));
                gravar.print(",");
                gravar.print(ImprimirRelatorios.calcularMediaIntervaloDatas(AppCovanalytics.dadosCovidAcumuladosFinal, indiceIntervaloMedia4, 5));

                gravar.println();

                gravar.print("desvio padrao");
                gravar.print(",");
                gravar.print(ImprimirRelatorios.calcularDesvioPadraoIntervaloDatas(AppCovanalytics.dadosCovidAcumuladosFinal, indiceIntervaloMedia3, 2));
                gravar.print(",");
                gravar.print(ImprimirRelatorios.calcularDesvioPadraoIntervaloDatas(AppCovanalytics.dadosCovidAcumuladosFinal, indiceIntervaloMedia3, 3));
                gravar.print(",");
                gravar.print(ImprimirRelatorios.calcularDesvioPadraoIntervaloDatas(AppCovanalytics.dadosCovidAcumuladosFinal, indiceIntervaloMedia3, 4));
                gravar.print(",");
                gravar.print(ImprimirRelatorios.calcularDesvioPadraoIntervaloDatas(AppCovanalytics.dadosCovidAcumuladosFinal, indiceIntervaloMedia3, 5));
                gravar.print(",");

                gravar.print("desvio padrao");
                gravar.print(",");
                gravar.print(ImprimirRelatorios.calcularDesvioPadraoIntervaloDatas(AppCovanalytics.dadosCovidAcumuladosFinal, indiceIntervaloMedia4, 2));
                gravar.print(",");
                gravar.print(ImprimirRelatorios.calcularDesvioPadraoIntervaloDatas(AppCovanalytics.dadosCovidAcumuladosFinal, indiceIntervaloMedia4, 3));
                gravar.print(",");
                gravar.print(ImprimirRelatorios.calcularDesvioPadraoIntervaloDatas(AppCovanalytics.dadosCovidAcumuladosFinal, indiceIntervaloMedia4, 4));
                gravar.print(",");
                gravar.print(ImprimirRelatorios.calcularDesvioPadraoIntervaloDatas(AppCovanalytics.dadosCovidAcumuladosFinal, indiceIntervaloMedia4, 5));

                gravar.close();
                break;
        }
    }

    //********************************************************************************************
    //*****Metodo para exportar o relatorio previsional*******************************************
    //********************************************************************************************

    public static void exportarRelatorioPrevisional(int[][] matrizDadosMenu) throws FileNotFoundException {

        System.out.println("Insira o nome do ficheiro a ser gerado:");
        String nomeFicheiro = ler.next();

        int tipoRelatorio = matrizDadosMenu[1][0];

        switch (tipoRelatorio) {
            case 1:
                File ficheiro = new File(nomeFicheiro + ".csv");
                PrintWriter gravar = new PrintWriter(ficheiro);


                gravar.println("RelatorioPrevisional");
                gravar.println("PrevisaoDeTransicaoEntreEstados");
                gravar.println(CABECALHOCADEIASDEMARKOV);

                gravar.print((MetodoPrevisional.calculoCadeiaDeMarkov(LerFicheiroDados.converterData(AbrirMenus.matrizDadosMenu[2][2]), AbrirMenus.matrizDadosMenu[2][3])[1][0]));
                gravar.print(",");
                gravar.print((MetodoPrevisional.calculoCadeiaDeMarkov(LerFicheiroDados.converterData(AbrirMenus.matrizDadosMenu[2][2]), AbrirMenus.matrizDadosMenu[2][3])[2][0]));
                gravar.print(",");
                gravar.print((MetodoPrevisional.calculoCadeiaDeMarkov(LerFicheiroDados.converterData(AbrirMenus.matrizDadosMenu[2][2]), AbrirMenus.matrizDadosMenu[2][3])[3][0]));
                gravar.print(",");
                gravar.println(MetodoPrevisional.calculoCadeiaDeMarkov(LerFicheiroDados.converterData(AbrirMenus.matrizDadosMenu[2][2]), AbrirMenus.matrizDadosMenu[2][3])[4][0]);

                gravar.close();
                break;
            case 2:
                ficheiro = new File(nomeFicheiro + ".csv");
                gravar = new PrintWriter(ficheiro);


                gravar.println("RelatorioPrevisional");
                gravar.println("EvolucaoAteObito");
                gravar.println(CABECALHOFATORIZACAODECROUT);

                gravar.print(MetodoPrevisional.calculoFatorizacaoDeCrout()[0]);
                gravar.print(",");
                gravar.print(MetodoPrevisional.calculoFatorizacaoDeCrout()[1]);
                gravar.print(",");
                gravar.print(MetodoPrevisional.calculoFatorizacaoDeCrout()[2]);
                gravar.print(",");
                gravar.print(MetodoPrevisional.calculoFatorizacaoDeCrout()[3]);

                gravar.close();
                break;

        }


    }
}

