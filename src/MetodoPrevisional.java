import java.io.File;
import java.io.FileNotFoundException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class MetodoPrevisional {

    static final int NUMEROLINHAS = 4;
    static final int NUMEROCOLUNAS = 4;

    static LocalDate dataDisponivelMin = LerFicheiroDados.converterData(LerFicheiroDados.calcularLimitesDatasMatrizDados(AppCovanalytics.dadosCovidDiaFinal, AppCovanalytics.contaLinhasDadosDiaFinal)[0]);
    static LocalDate dataDisponivelMax = LerFicheiroDados.converterData(LerFicheiroDados.calcularLimitesDatasMatrizDados(AppCovanalytics.dadosCovidDiaFinal, AppCovanalytics.contaLinhasDadosDiaFinal)[1]);

    static Scanner ler = new Scanner(System.in);

//*********************************************************************************************************************
// [1] - PREVISÃO DE TRANSIÇÇÃO ENTRE ESTADOS (CADEIA DE MARKOV) - USER INTERFACE
//*********************************************************************************************************************

    public static void cadeiaDeMarkovUserInterface() throws FileNotFoundException {

        int verificadorDataPrevisao, valorMenuMarkovDataPrevisao;
        LocalDate dataFinal;

        System.out.println();
        System.out.println("==========================================");
        System.out.println("|   Previsão de Transição entre Estados  |");
        System.out.println("==========================================");
        System.out.println("|                                        |");
        System.out.println("|   1 - Definição de data para previsão  |");
        System.out.println("|                                        |");
        System.out.println("|   0 - Voltar ao Menu Principal         |");
        System.out.println("==========================================\n");
        System.out.println("Introduza a opção desejada\n");

        int selecao = ler.nextInt();
        while (selecao != 1 && selecao != 0) {
            System.out.println("Opção inválida, introduza novamente a opção pretendida");
            selecao = ler.nextInt();
        }

        switch (selecao) {
            case 1:
                Scanner teclado = new Scanner(System.in);

                //Informa o utilizador do período temporal de dados disponível
                System.out.println("Período de dados disponível:");
                System.out.println("Desde: " + dataDisponivelMin + "\nAté: " + dataDisponivelMax);
                System.out.println();

                System.out.println("Introduza a data pretendida para previsão (AAAA-MM-DD):\n");

                String dataPrevisao = teclado.nextLine();

                //Validação da data inserida pelo utilizador
                valorMenuMarkovDataPrevisao = LerFicheiroDados.conversorData(dataPrevisao);
                AbrirMenus.matrizDadosMenu[2][1] = 1;
                verificadorDataPrevisao = LerFicheiroDados.verificadorDatas(valorMenuMarkovDataPrevisao, AppCovanalytics.dadosCovidDiaFinal, AppCovanalytics.dadosCovidAcumuladosFinal, AbrirMenus.matrizDadosMenu);
                while (LocalDate.parse(dataPrevisao).isEqual(dataDisponivelMin) || LocalDate.parse(dataPrevisao).isBefore(dataDisponivelMin)) {
                    System.out.println("Data inserida inválida!\nPara efetuar a previsão, a data inserida terá de ser pelo menos um dia após a data do primeiro registo.");
                    System.out.println("Insira novamente a data pretendida:");
                    dataPrevisao = teclado.nextLine();
                    valorMenuMarkovDataPrevisao = LerFicheiroDados.conversorData(dataPrevisao);
                    AbrirMenus.matrizDadosMenu[2][1] = 1;
                    verificadorDataPrevisao = LerFicheiroDados.verificadorDatas(valorMenuMarkovDataPrevisao, AppCovanalytics.dadosCovidDiaFinal, AppCovanalytics.dadosCovidAcumuladosFinal, AbrirMenus.matrizDadosMenu);
                }
                if (verificadorDataPrevisao == 0) {
                    System.out.println();
                    System.out.println("Data de previsão fora do período disponível!\nSerá assumida a data do último registo (" + dataDisponivelMax + ").");
                    dataFinal = dataDisponivelMax;
                } else {
                    dataFinal = LocalDate.parse(dataPrevisao, DateTimeFormatter.ISO_LOCAL_DATE);
                    dataFinal = dataFinal.minusDays(1);
                }

                dataPrevisao = dataPrevisao.replace("-", "");
                int dataPrevisaoInt = Integer.parseInt(dataPrevisao);
                AbrirMenus.matrizDadosMenu[2][3] = dataPrevisaoInt;
                AbrirMenus.matrizDadosMenu[2][2] = Integer.parseInt(dataFinal.toString().replace("-", ""));

                calculoCadeiaDeMarkov(dataFinal, dataPrevisaoInt);

                // Imprimir previsão de estados (markov)
                System.out.println();
                System.out.println("***************** Previsão de Transição entre Estados ********************");
                System.out.println("**************************************************************************");
                System.out.println("||    Infetados   ||  Hospitalizados  ||  Internados UCI  ||   Mortes   ||");
                System.out.println("--------------------------------------------------------------------------");
                System.out.printf("|| %14.1f || %16.1f || %16.1f || %10.1f ||\n", calculoCadeiaDeMarkov(dataFinal, dataPrevisaoInt)[1][0], calculoCadeiaDeMarkov(dataFinal, dataPrevisaoInt)[2][0], calculoCadeiaDeMarkov(dataFinal, dataPrevisaoInt)[3][0], calculoCadeiaDeMarkov(dataFinal, dataPrevisaoInt)[4][0]);
                break;
            case 0:
                AbrirMenus.abrirMenu0();
                break;
        }
    }

//*********************************************************************************************************************
// [1.1] - CÁLCULO DAS CADEIAS DE MARKOV
//*********************************************************************************************************************

    public static double[][] calculoCadeiaDeMarkov(LocalDate dataInicio, int data) throws FileNotFoundException {

        double[][] mudancaTransposta = new double[5][1];
        int numeroEstados = 5;
        LocalDate dataPrevisao = LerFicheiroDados.converterData(data);

        // Cálculo do número de iterações a executar, em função da diferença de dias entre as datas a considerar
        Duration diferencaDias = Duration.between(dataInicio.atStartOfDay(), dataPrevisao.atStartOfDay());
        long iteracoes = diferencaDias.toDays();

        Scanner leitorFicheiro = new Scanner(new File(AbrirMenus.nomeficheiro));

        float[][] matrizDadosTransicao = new float[5][5];

        for (int linha = 0; linha < 5; linha++) {
            for (int coluna = 0; coluna < 5; ) {
                String variavelTemp = leitorFicheiro.nextLine();
                if (variavelTemp.trim().length() != 0) {
                    matrizDadosTransicao[linha][coluna] = Float.parseFloat(variavelTemp.substring(4));
                    coluna++;
                }
            }
        }

        leitorFicheiro.close();

        int dataInicialInt = LerFicheiroDados.conversorData(dataInicio.toString());


        for (int linha = 0; linha < AppCovanalytics.contaLinhasDadosDiaFinal; linha++) {
            if (AppCovanalytics.dadosCovidDiaFinal[linha][0] == dataInicialInt) {
                for (int index = 1; index <= mudancaTransposta.length; index++) {
                    mudancaTransposta[index - 1][0] = AppCovanalytics.dadosCovidDiaFinal[linha][index];
                }
            }
        }
        // Utilizar o método da potência para calcular mudança de estado.
        for (int t = 0; t < iteracoes; t++) {

            // Calcular efeito do próximo movimento na mudança de estados.
            double[][] novaMudanca = new double[numeroEstados][1];
            for (int j = 0; j < numeroEstados; j++) {
                //  Nova mudança do estado j é o produto dos estados anteriores somada à coluna j de p[][].
                for (int k = 0; k < numeroEstados; k++)
                    novaMudanca[k][0] += matrizDadosTransicao[k][j] * mudancaTransposta[j][0];
            }
            //Update mudança de estados.
            mudancaTransposta = novaMudanca;

        }
        return mudancaTransposta;
    }


//*********************************************************************************************************************
// [2] - EVOLUÇÃO ATÉ ÓBITO (FATORIZAÇÃO DE CROUT)
//*********************************************************************************************************************

    public static void fatorizacaoDeCroutUserInterface() throws FileNotFoundException {

        calculoFatorizacaoDeCrout();

        AbrirMenus.limparConsola();

        System.out.println();
        System.out.println("************************** Evolução até Óbito *********************************");
        System.out.println("*******************************************************************************");
        System.out.println("||  Não Infetados  ||    Infetados   ||  Hospitalizados  ||  Internados UCI  ||");
        System.out.println("-------------------------------------------------------------------------------");
        System.out.printf("|| %15.1f || %14.1f || %16.1f || %16.1f ||\n", calculoFatorizacaoDeCrout()[0], calculoFatorizacaoDeCrout()[1], calculoFatorizacaoDeCrout()[2], calculoFatorizacaoDeCrout()[3]);

    }

    public static float[] calculoFatorizacaoDeCrout() {

        float[][] matrizQ = calcularMatrizQ(AppCovanalytics.dadosMatrizTransicoes);
        int[][] matrizIdentidade = criarMatrizIdentidade();
        float[][] matrizA = calcularMatrizA(matrizIdentidade, matrizQ);
        float[][] matrizLower = new float[NUMEROLINHAS][NUMEROCOLUNAS];
        float[][] matrizUpper = new float[NUMEROLINHAS][NUMEROCOLUNAS];

        //-----------------

        calcularMatrizesLU(matrizLower, matrizUpper, matrizA);

        float[][] matrizLowerInversa = calcularInversaMatrizL(matrizLower);
        float[][] matrizUpperInversa = calcularInversaMatrizU(matrizUpper);
        float[][] matrizN = calculoMatrizN(matrizLowerInversa, matrizUpperInversa);

        return calcularMatrizM(matrizN);
    }

//*********************************************************************************************************************
// [2.1] CÁLCULO DA MATRIZ Q
//*********************************************************************************************************************

    private static float[][] calcularMatrizQ(float[][] matrizTransicao) {

        float[][] matrizQ = new float[NUMEROLINHAS][NUMEROCOLUNAS];

        for (int linhas = 0; linhas < NUMEROLINHAS; linhas++) {
            for (int colunas = 0; colunas < NUMEROCOLUNAS; colunas++) {
                matrizQ[linhas][colunas] = matrizTransicao[linhas][colunas];
            }
        }
        return matrizQ;
    }

//*********************************************************************************************************************
// [2.2] CRIAÇÃO DA MATRIZ IDENTIDADE
//*********************************************************************************************************************

    private static int[][] criarMatrizIdentidade() {

        int[][] matrizIdentidade = new int[NUMEROLINHAS][NUMEROCOLUNAS];

        for (int i = 0; i < NUMEROLINHAS; i++) {
            for (int j = 0; j < NUMEROCOLUNAS; j++) {
                if (i == j)
                    matrizIdentidade[i][j] = 1; // na diagonal principal
                else
                    matrizIdentidade[i][j] = 0;
            }
        }
        return matrizIdentidade;
    }

//*********************************************************************************************************************
// [2.3] CÁLCULO DA MATRIZ A
//*********************************************************************************************************************

    private static float[][] calcularMatrizA(int[][] matrizIdentidade, float[][] matrizQ) {

        float[][] matrizA = new float[NUMEROLINHAS][NUMEROCOLUNAS];

        for (int linhas = 0; linhas < NUMEROLINHAS; linhas++) {
            for (int colunas = 0; colunas < NUMEROCOLUNAS; colunas++) {
                matrizA[linhas][colunas] = matrizIdentidade[linhas][colunas] - matrizQ[linhas][colunas];
            }
        }
        return matrizA;
    }


//*********************************************************************************************************************
// [2.4] CÁLCULO DA MATRIZ L E U
//*********************************************************************************************************************

    private static void calcularMatrizesLU(float[][] matrizLower, float[][] matrizUpper, float[][] matrizA) {

        for (int linhas = 0; linhas < NUMEROLINHAS; linhas++) {
            for (int colunas = 0; colunas < NUMEROCOLUNAS; colunas++) {
                if (linhas == colunas)
                    matrizUpper[linhas][colunas] = 1; //Diagonal Principal
                else
                    matrizUpper[linhas][colunas] = 0;
            }
        }

        for (int linhas = 0; linhas < NUMEROLINHAS; linhas++) {
            matrizLower[linhas][0] = matrizA[linhas][0];
        }

        for (int colunas = 1; colunas < NUMEROLINHAS; colunas++) {
            matrizUpper[0][colunas] = matrizA[0][colunas] / matrizLower[0][0];
        }

        for (int linhas = 1; linhas < NUMEROLINHAS; linhas++) {
            matrizLower[linhas][1] = matrizA[linhas][1] - (matrizLower[linhas][0] * matrizUpper[0][1]);
        }

        for (int colunas = 2; colunas < NUMEROCOLUNAS; colunas++) {
            matrizUpper[1][colunas] = (matrizA[1][colunas] - (matrizLower[1][0] * matrizUpper[0][colunas])) / matrizLower[1][1];
        }

        for (int linhas = 2; linhas < NUMEROLINHAS; linhas++) {
            matrizLower[linhas][2] = matrizA[linhas][2] - ((matrizA[linhas][0] * matrizUpper[0][3]) + (matrizLower[linhas][1] * matrizUpper[1][2]));
        }

        matrizUpper[2][3] = (matrizA[2][3] - (matrizA[2][0] * matrizUpper[0][3]) - (matrizLower[2][1] * matrizUpper[1][3])) / matrizLower[2][2];
        matrizLower[3][3] = matrizA[3][3] - (matrizLower[3][0] * matrizUpper[0][3]) - (matrizLower[3][1] * matrizUpper[1][3]) - (matrizLower[3][2] * matrizUpper[2][3]);

    }


//*********************************************************************************************************************
// [2.5] CÁLCULO DA MATRIZ INVERSA DE L E U
//*********************************************************************************************************************

    //Inversa da Matriz U *********************************************************************************************
    private static float[][] calcularInversaMatrizU(float[][] matrizUpper) {

        float[][] matrizUpperInversa = new float[NUMEROLINHAS][NUMEROCOLUNAS];

        for (int linhas = 0; linhas < NUMEROLINHAS; linhas++) {
            for (int colunas = 0; colunas < NUMEROCOLUNAS; colunas++) {
                if (linhas == colunas) {
                    matrizUpperInversa[linhas][colunas] = 1;
                }
            }
        }

        matrizUpperInversa[0][1] = -matrizUpper[0][1];
        matrizUpperInversa[1][2] = -matrizUpper[1][2];
        matrizUpperInversa[2][3] = -matrizUpper[2][3];
        matrizUpperInversa[0][2] = -((matrizUpper[0][1] * matrizUpperInversa[1][2]) + matrizUpper[0][2]);
        matrizUpperInversa[1][3] = -((matrizUpper[1][2] * matrizUpperInversa[2][3]) + matrizUpper[1][3]);
        matrizUpperInversa[0][3] = -((matrizUpper[0][1] * matrizUpperInversa[1][3]) + (matrizUpper[0][2] * matrizUpperInversa[2][3]) + matrizUpper[0][3]);

        for (int linhas = 1; linhas < NUMEROLINHAS; linhas++) {
            matrizUpperInversa[linhas][0] = 0;
        }

        matrizUpperInversa[2][1] = 0;
        matrizUpperInversa[3][1] = 0;
        matrizUpperInversa[3][2] = 0;

        return matrizUpperInversa;
    }

    //Inversa da Matriz L *********************************************************************************************
    private static float[][] calcularInversaMatrizL(float[][] matrizLower) {

        float[][] matrizLowerInversa = new float[NUMEROLINHAS][NUMEROCOLUNAS];

        for (int linhas = 0; linhas < NUMEROLINHAS; linhas++) {
            for (int colunas = 0; colunas < NUMEROCOLUNAS; colunas++) {
                if (linhas == colunas) {
                    matrizLowerInversa[linhas][colunas] = 1 / matrizLower[linhas][colunas];
                }
            }
        }

        matrizLowerInversa[1][0] = -(matrizLower[1][0] * matrizLowerInversa[0][0]) / matrizLower[1][1];
        matrizLowerInversa[2][1] = -(matrizLower[2][1] * matrizLowerInversa[1][1]) / matrizLower[2][2];
        matrizLowerInversa[3][2] = -(matrizLower[3][2] * matrizLowerInversa[2][2]) / matrizLower[3][3];
        matrizLowerInversa[2][0] = -((matrizLower[2][0] * matrizLowerInversa[0][0]) + (matrizLower[2][1] * matrizLowerInversa[1][0])) / matrizLower[2][2];
        matrizLowerInversa[3][1] = -((matrizLower[3][1] * matrizLowerInversa[1][1]) + (matrizLower[3][2] * matrizLowerInversa[2][1])) / matrizLower[3][3];
        matrizLowerInversa[3][0] = -((matrizLower[3][0] * matrizLowerInversa[0][0]) + (matrizLower[3][1] * matrizLowerInversa[1][0]) + (matrizLower[3][2] * matrizLowerInversa[2][0])) / matrizLower[3][3];

        for (int colunas = 1; colunas < NUMEROCOLUNAS; colunas++) {
            matrizLowerInversa[0][colunas] = 0;
        }

        matrizLowerInversa[1][2] = 0;
        matrizLowerInversa[1][3] = 0;
        matrizLowerInversa[2][3] = 0;

        return matrizLowerInversa;
    }


//*********************************************************************************************************************
// [2.6] CÁLCULO DA MATRIZ N
//*********************************************************************************************************************

    public static float[][] calculoMatrizN(float[][] matrizLowerInversa, float[][] matrizUpperInversa) {

        float[][] matrizN = new float[NUMEROLINHAS][NUMEROCOLUNAS];

        float soma = 0;
        float multiplicador;

        for (int linhas = 0; linhas < NUMEROLINHAS; linhas++) {
            for (int colunas = 0; colunas < NUMEROCOLUNAS; colunas++) {
                for (int indiceComum = 0; indiceComum < NUMEROLINHAS; indiceComum++) {
                    multiplicador = matrizUpperInversa[linhas][indiceComum] * matrizLowerInversa[indiceComum][colunas];
                    soma = multiplicador + soma;
                }
                matrizN[linhas][colunas] = soma;
                soma = 0;
            }
        }

        return matrizN;
    }

//*********************************************************************************************************************
// [2.7] CÁLCULO DA MATRIZ M (VETOR1 x N)
//*********************************************************************************************************************

    private static float[] calcularMatrizM(float[][] matrizN) {
        float[] matrizM = new float[NUMEROCOLUNAS];
        for (int colunas = 0; colunas < NUMEROCOLUNAS; colunas++) {
            float soma = 0;
            for (int linhas = 0; linhas < NUMEROLINHAS; linhas++) {
                soma = soma + matrizN[linhas][colunas];
            }
            matrizM[colunas] = soma;
        }

        return matrizM;
    }
}
