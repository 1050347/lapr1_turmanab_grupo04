import java.io.File;
import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.util.Scanner;

public class LerFicheiroDados {


    //********************************************************************************************
    //*****Metodo para verificar se o ficheiro indicado pelo user existe *************************
    //********************************************************************************************

    public static boolean verificarExistenciaFicheiro() {

        File ficheiro = new File(AbrirMenus.nomeficheiro);

        if (ficheiro.exists()) {
            return true;
        } else {
            return false;
        }

    }


    //********************************************************************************************
    //*****Metodo para verificar a tipologia de ficheiro DIARIO***********************************
    //********************************************************************************************

    public static boolean verificarTipodeFicheiroDia() throws FileNotFoundException {


        File ficheiro = new File(AbrirMenus.nomeficheiro);

        Scanner ler = new Scanner(ficheiro);

        ler.nextLine();
        String data = ler.nextLine();
        data = data.split(",")[0];
        ler.close();

        if ((data.substring(2, 3)).compareTo("-") == 0) {
            return true;
        } else {
            return false;
        }

    }

    //********************************************************************************************
    //*****Metodo para verificar a tipologia de ficheiro ACUMULADO********************************
    //********************************************************************************************

    public static boolean verificarTipodeFicheiroAcumulado() throws FileNotFoundException {


        File ficheiro = new File(AbrirMenus.nomeficheiro);

        Scanner ler = new Scanner(ficheiro);

        ler.nextLine();
        String data = ler.nextLine();
        data = data.split(",")[0];
        ler.close();

        if ((data.substring(4, 5)).compareTo("-") == 0) {
            return true;
        } else {
            return false;
        }

    }


    //********************************************************************************************
    //*****Metodo para verificar a tipologia de ficheiro TRANSICOES*******************************
    //********************************************************************************************

    public static boolean verificarTipodeFicheiroTransicoes() throws FileNotFoundException {


        File ficheiro = new File(AbrirMenus.nomeficheiro);
        Scanner ler = new Scanner(ficheiro);

        String data = ler.nextLine();
        ler.close();

        if ((data.substring(3, 4)).compareTo("=") == 0) {
            return true;
        } else {
            return false;
        }

    }


    //********************************************************************************************
    //*****Metodo para ler e extrair os dados dos totais Dia do CSV [Matriz Temporaria]***********
    //********************************************************************************************

    public static void lerFicheiroDeDadosDiaTemp() throws FileNotFoundException {

        //determinar o numero de linhas da matriz

        Scanner lerContador = new Scanner(new File(AbrirMenus.nomeficheiro));

        int contadorLinhas = 0;
        while (lerContador.hasNextLine()) {
            String line = lerContador.nextLine();
            contadorLinhas++;
        }
        int numeroColunas = 6;
        int numeroLinhas = contadorLinhas - 1;
        lerContador.close();

        //extrair os dados para uma matriz temporaria no metodo (dados ainda em tratamento)

        Scanner ler = new Scanner(new File(AbrirMenus.nomeficheiro));

        String descritivo = ler.nextLine();
        String[][] matrizString = new String[numeroLinhas][numeroColunas];

        for (int linha = 0; linha < matrizString.length; linha++) {
            String conteudoDaLinha = ler.nextLine();
            String[] itensDaLinha = conteudoDaLinha.split("[,./_ +*?!|]");

            for (int coluna = 0; coluna < matrizString[0].length; coluna++) {
                String valoresTemporariosString = itensDaLinha[coluna].trim();
                matrizString[linha][coluna] = valoresTemporariosString;
            }
        }

        String data;
        for (int linha = 0; linha < matrizString.length; linha++) {
            matrizString[linha][0] = matrizString[linha][0].replace("-", "");
            data = matrizString[linha][0];
            matrizString[linha][0] = data.substring(4, 8) + data.substring(2, 4) + data.substring(0, 2);
        }

        //inserir os dados na matriz temporaria da App

        for (int linha = 0; linha < matrizString.length; linha++) {
            for (int coluna = 0; coluna < matrizString[0].length; coluna++) {
                String conteudoDaCelula = matrizString[linha][coluna];
                AppCovanalytics.dadosCovidDiaTemp[linha][coluna] = Integer.parseInt(conteudoDaCelula);
            }
            AppCovanalytics.contaLinhasDadosDiaTemp++;
        }

        ler.close();

    }


    //********************************************************************************************
    //*****Metodo para ler e extrair os dados dos totais Acumulados do CSV [Matriz Temporaria]****
    //********************************************************************************************

    public static void lerFicheiroDeDadosAcumuladosTemp() throws FileNotFoundException {

        //determinar o numero de linhas da matriz

        Scanner lerContador = new Scanner(new File(AbrirMenus.nomeficheiro));

        int contadorLinhas = 0;
        while (lerContador.hasNextLine()) {
            String line = lerContador.nextLine();
            contadorLinhas++;
        }
        int numeroColunas = 6;
        int numeroLinhas = contadorLinhas - 1;
        lerContador.close();

        //extrair os dados para uma matriz temporaria no metodo (dados ainda em tratamento)

        Scanner ler = new Scanner(new File(AbrirMenus.nomeficheiro));

        String descritivo = ler.nextLine();
        String[][] matrizString = new String[numeroLinhas][numeroColunas];

        for (int linha = 0; linha < matrizString.length; linha++) {
            String conteudoDaLinha = ler.nextLine();
            String[] itensDaLinha = conteudoDaLinha.split("[,./_ +*?!|]");

            for (int coluna = 0; coluna < matrizString[0].length; coluna++) {
                String valoresTemporariosString = itensDaLinha[coluna].trim();
                matrizString[linha][coluna] = valoresTemporariosString;
            }
        }

        //inserir os dados na matriz temporaria da App

        int[][] matrizInt = new int[numeroLinhas][numeroColunas];

        for (int linha = 0; linha < matrizString.length; linha++) {
            for (int coluna = 0; coluna < matrizString[0].length; coluna++) {
                String conteudoDaCelula = matrizString[linha][coluna];
                conteudoDaCelula = conteudoDaCelula.replace("-", "");
                AppCovanalytics.dadosCovidAcumuladosTemp[linha][coluna] = Integer.parseInt(conteudoDaCelula);
            }
            AppCovanalytics.contaLinhasDadosAcumuladosTemp++;
        }
        ler.close();

    }


    //********************************************************************************************
    //*****Metodo para calcular o 1º indice na matriz temporaria a ser lido para a final**********
    //********************************************************************************************

    public static int calcularMenorIndiceMatrizTemporaria(int[][] matrizDadosTemp, int[][] matrizDadosFinal, int contaLinhasDadosDiaFinal) {

        int menorIndiceMatrizTemp = 0;
        int indiceAuxiliar = 0;

        if (contaLinhasDadosDiaFinal == 0) {
            menorIndiceMatrizTemp = 0;

        } else {
            while (matrizDadosFinal[indiceAuxiliar][0] != matrizDadosTemp[0][0] && matrizDadosFinal[indiceAuxiliar][0] != 0) {
                indiceAuxiliar++;
            }
            menorIndiceMatrizTemp = indiceAuxiliar;
        }

        return menorIndiceMatrizTemp;
    }


    //********************************************************************************************
    //*****Metodo para eliminar os dados + contador linhas Matriz TEMPORARIA de dados diarios*****
    //********************************************************************************************

    public static void apagarDadosDiaTemp() {

        for (int linha = 0; linha < AppCovanalytics.contaLinhasDadosDiaTemp; linha++) {
            for (int coluna = 0; coluna < AppCovanalytics.dadosCovidDiaTemp[0].length; coluna++) {
                AppCovanalytics.dadosCovidDiaTemp[linha][coluna] = 0;
            }
        }
        AppCovanalytics.contaLinhasDadosDiaTemp = 0;
    }


    //********************************************************************************************
    //*****Metodo para eliminar os dados + contador linhas Matriz TEMPORARIA de dados acumulados**
    //********************************************************************************************

    public static void apagarDadosAcumuladosTemp() {

        for (int linha = 0; linha < AppCovanalytics.contaLinhasDadosAcumuladosTemp; linha++) {
            for (int coluna = 0; coluna < AppCovanalytics.dadosCovidAcumuladosTemp[0].length; coluna++) {
                AppCovanalytics.dadosCovidAcumuladosTemp[linha][coluna] = 0;
            }
        }
        AppCovanalytics.contaLinhasDadosAcumuladosTemp = 0;
    }



    //********************************************************************************************
    //*****Metodo para calcular a quantidade de datas sobrepostas entre matrizes******************
    //********************************************************************************************

    public static int quantidadeDeSobrepostas(int[][] matrizDados1, int[][] matrizDados2, int contaLinhasDados2, int contaLinhasDados1) {

        int sobrepostas = 0;

        for (int linhaAux = 0; linhaAux < contaLinhasDados2; linhaAux++) {
            for (int linha = 0; linha < contaLinhasDados1; linha++) {

                if (matrizDados1[linha][0] == matrizDados2[linhaAux][0]) {
                    sobrepostas++;
                }
            }
        }
        return sobrepostas;
    }


    //********************************************************************************************
    //*****Metodo para eliminar os dados da Matriz FINAL******************************************
    //********************************************************************************************

    public static void apagarDadosMatrizFinal(int[][] matrizDadosFinal, int contaLinhasDadosFinal) {

        for (int linha = 0; linha < contaLinhasDadosFinal; linha++) {
            for (int coluna = 0; coluna < AppCovanalytics.NUMEROCOLUNAS; coluna++) {
                matrizDadosFinal[linha][coluna] = 0;
            }
        }
    }
    //********************************************************************************************
    //*****Metodo para passar os dados da matriz Final para a Auxiliar ***************************
    //********************************************************************************************


    public static void transferirDadosMatrizAuxiliar(int[][] matrizDadosFinal, int[][] matrizDadosAux, int contaLinhasDadosFinal) {

        for (int linha = 0; linha < contaLinhasDadosFinal; linha++) {
            for (int coluna = 0; coluna < AppCovanalytics.NUMEROCOLUNAS; coluna++) {
                matrizDadosAux[linha][coluna] = matrizDadosFinal[linha][coluna];
            }
        }
    }

    //********************************************************************************************
    //*****Metodo para atualizar os dados da matriz final [dados Diarios]*************************
    //********************************************************************************************

    public static void transferirDadosMatrizDiaFinal(int[][] matrizDadosTemp, int[][] matrizDadosFinal) {

        if (matrizDadosFinal[0][0] > matrizDadosTemp[0][0]) {

            transferirDadosMatrizAuxiliar(matrizDadosFinal, AppCovanalytics.dadosCovidDiaAux, AppCovanalytics.contaLinhasDadosDiaFinal);

            apagarDadosMatrizFinal(matrizDadosFinal, AppCovanalytics.contaLinhasDadosDiaFinal);

            AppCovanalytics.contaLinhasDadosDiaAux = AppCovanalytics.contaLinhasDadosDiaFinal;

            AppCovanalytics.contaLinhasDadosDiaFinal = 0;

            int sobrepostas = quantidadeDeSobrepostas(matrizDadosTemp, AppCovanalytics.dadosCovidDiaAux, AppCovanalytics.contaLinhasDadosDiaAux, AppCovanalytics.contaLinhasDadosDiaTemp);

            for (int linha = 0; linha < AppCovanalytics.contaLinhasDadosDiaTemp; linha++) {
                for (int coluna = 0; coluna < AppCovanalytics.NUMEROCOLUNAS; coluna++) {
                    matrizDadosFinal[linha][coluna] = matrizDadosTemp[linha][coluna];
                }
                AppCovanalytics.contaLinhasDadosDiaFinal++;
            }

            for (int linha = 0; linha < AppCovanalytics.contaLinhasDadosDiaAux; linha++) {
                for (int coluna = 0; coluna < AppCovanalytics.NUMEROCOLUNAS; coluna++) {
                    matrizDadosFinal[AppCovanalytics.contaLinhasDadosDiaTemp + linha][coluna] = AppCovanalytics.dadosCovidDiaAux[linha + sobrepostas][coluna];
                }
                AppCovanalytics.contaLinhasDadosDiaFinal++;
            }
            AppCovanalytics.contaLinhasDadosDiaFinal = AppCovanalytics.contaLinhasDadosDiaFinal - sobrepostas;


        } else {

            int indice0 = calcularMenorIndiceMatrizTemporaria(matrizDadosTemp, matrizDadosFinal, AppCovanalytics.contaLinhasDadosDiaFinal);

            int sobrepostas = quantidadeDeSobrepostas(matrizDadosTemp, matrizDadosFinal, AppCovanalytics.contaLinhasDadosDiaFinal, AppCovanalytics.contaLinhasDadosDiaTemp);

            for (int linha = 0; linha < AppCovanalytics.contaLinhasDadosDiaTemp; linha++) {
                for (int coluna = 0; coluna < AppCovanalytics.NUMEROCOLUNAS; coluna++) {
                    matrizDadosFinal[linha + indice0][coluna] = matrizDadosTemp[linha][coluna];
                }
                AppCovanalytics.contaLinhasDadosDiaFinal++;
            }
            AppCovanalytics.contaLinhasDadosDiaFinal = AppCovanalytics.contaLinhasDadosDiaFinal - sobrepostas;

        }

    }

    //********************************************************************************************
    //*****Metodo para atualizar os dados da matriz final [dados Acumulados]**********************
    //********************************************************************************************

    public static void transferirDadosMatrizAcumFinal(int[][] matrizDadosTemp, int[][] matrizDadosFinal) {


        if (matrizDadosFinal[0][0] > matrizDadosTemp[0][0]) {

            transferirDadosMatrizAuxiliar(matrizDadosFinal, AppCovanalytics.dadosCovidAcumuladosAux, AppCovanalytics.contaLinhasDadosAcumuladosFinal);

            apagarDadosMatrizFinal(matrizDadosFinal, AppCovanalytics.contaLinhasDadosAcumuladosFinal);

            AppCovanalytics.contaLinhasDadosAcumuladosAux = AppCovanalytics.contaLinhasDadosAcumuladosFinal;

            AppCovanalytics.contaLinhasDadosAcumuladosFinal = 0;

            int sobrepostas = quantidadeDeSobrepostas(matrizDadosTemp, AppCovanalytics.dadosCovidAcumuladosAux, AppCovanalytics.contaLinhasDadosAcumuladosAux, AppCovanalytics.contaLinhasDadosAcumuladosTemp);

            for (int linha = 0; linha < AppCovanalytics.contaLinhasDadosAcumuladosTemp; linha++) {
                for (int coluna = 0; coluna < AppCovanalytics.NUMEROCOLUNAS; coluna++) {
                    matrizDadosFinal[linha][coluna] = matrizDadosTemp[linha][coluna];
                }
                AppCovanalytics.contaLinhasDadosAcumuladosFinal++;
            }

            for (int linha = 0; linha < AppCovanalytics.contaLinhasDadosAcumuladosAux; linha++) {
                for (int coluna = 0; coluna < AppCovanalytics.NUMEROCOLUNAS; coluna++) {
                    matrizDadosFinal[AppCovanalytics.contaLinhasDadosAcumuladosTemp + linha][coluna] = AppCovanalytics.dadosCovidAcumuladosAux[linha + sobrepostas][coluna];
                }
                AppCovanalytics.contaLinhasDadosAcumuladosFinal++;
            }
            AppCovanalytics.contaLinhasDadosAcumuladosFinal = AppCovanalytics.contaLinhasDadosAcumuladosFinal - sobrepostas;


        } else {

            int indice0 = calcularMenorIndiceMatrizTemporaria(matrizDadosTemp, matrizDadosFinal, AppCovanalytics.contaLinhasDadosAcumuladosFinal);

            int sobrepostas = quantidadeDeSobrepostas(matrizDadosTemp, matrizDadosFinal, AppCovanalytics.contaLinhasDadosAcumuladosFinal, AppCovanalytics.contaLinhasDadosAcumuladosTemp);

            for (int linha = 0; linha < AppCovanalytics.contaLinhasDadosAcumuladosTemp; linha++) {
                for (int coluna = 0; coluna < AppCovanalytics.NUMEROCOLUNAS; coluna++) {
                    matrizDadosFinal[linha + indice0][coluna] = matrizDadosTemp[linha][coluna];
                }
                AppCovanalytics.contaLinhasDadosAcumuladosFinal++;
            }
            AppCovanalytics.contaLinhasDadosAcumuladosFinal = AppCovanalytics.contaLinhasDadosAcumuladosFinal - sobrepostas;

        }

    }


    //*******************************************************************************************************
    //*****Metodo para extrair os limites das datas(+ antiga e + recente) da Matriz [diarios ou acumulados]**
    //*******************************************************************************************************

    public static int[] calcularLimitesDatasMatrizDados(int[][] matrizDadosFinal, int contaLinhasDadosFinal) {

        int[] limiteDatas = new int[2];

        limiteDatas[0] = matrizDadosFinal[0][0];
        limiteDatas[1] = matrizDadosFinal[contaLinhasDadosFinal - 1][0];

        return limiteDatas;

    }

    //********************************************************************************************
    //*****Metodo para converter a data mais recente da Matriz [dados diarios ou acumulados]******
    //********************************************************************************************

    public static LocalDate converterData(int dataDadoMaisAtual) {


        String dataDado = Integer.toString(dataDadoMaisAtual);
        String dataConvertida = dataDado.substring(0, 4) + "-" + dataDado.substring(4, 6) + "-" + dataDado.substring(6, 8);
        LocalDate data;
        data = LocalDate.parse(dataConvertida);

        return data;

    }

    //****************************************************************************************************************
    //*****Metodo para Converter as Datas ****************************************************************************
    //****************************************************************************************************************


    public static int conversorData(String dataString) {

        String[] temporario;
        String[] temporarioTratado = new String[1];
        int data;

        temporario = dataString.split("[-,./_ +*?!|]");
        temporarioTratado[0] = "";
        for (int linha = 0; linha < temporario.length; linha++) {
            if (!temporario[linha].equals("")) {
                temporarioTratado[0] += temporario[linha];
            }
        }

        temporarioTratado[0] = temporarioTratado[0].replace(" ", "");

        data = Integer.parseInt(temporarioTratado[0]);


        return data;
    }

    //****************************************************************************************************************
    //*****Metodo para Verificar as Datas ****************************************************************************
    //****************************************************************************************************************

    public static int verificadorDatas(int data, int[][] matrizDia, int[][] matrizAcumulados, int[][] matrizDadosMenu) {
        int contador = 0;

        switch (matrizDadosMenu[1][0]) {

            case 1:
                for (int linha = 0; linha < AppCovanalytics.contaLinhasDadosDiaFinal; linha++) {
                    if (matrizDia[linha][0] == data) {
                        contador++;
                    }
                }
                break;

            case 2:
                for (int linha = 0; linha < AppCovanalytics.contaLinhasDadosAcumuladosFinal; linha++) {
                    if (matrizAcumulados[linha][0] == data) {
                        contador++;
                    }
                }
                break;

        }
        return contador;
    }

    //****************************************************************************************************************
    //*****Metodo para Verificar Maximos e Mínimos de anos e meses ***************************************************
    //****************************************************************************************************************

    public static int[] verificadorMaxMin(int[][] matrizDia, int[][] matrizAcumulados, int[][] matrizDadosMenu) {

        int[] matrizMaxMin = new int[4];
        int maxAno = 0;
        int minAno = 9999;
        int maxMes = 0;
        int minMes = 99;

        switch (matrizDadosMenu[1][0]) {

            case 1:

                for (int linha = 0; linha < AppCovanalytics.contaLinhasDadosDiaFinal; linha++) {
                    String temporarioString = Integer.toString(matrizDia[linha][0]);
                    temporarioString = temporarioString.substring(0, 4);
                    int temporario = Integer.parseInt(temporarioString);
                    if (temporario > maxAno) {
                        maxAno = temporario;
                    }
                }

                for (int linha = 0; linha < AppCovanalytics.contaLinhasDadosDiaFinal; linha++) {
                    String temporarioString = Integer.toString(matrizDia[linha][0]);
                    temporarioString = temporarioString.substring(0, 4);
                    int temporario = Integer.parseInt(temporarioString);
                    if (temporario < minAno) {
                        minAno = temporario;
                    }
                }

                for (int linha = 0; linha < AppCovanalytics.contaLinhasDadosDiaFinal; linha++) {
                    String temporarioString = Integer.toString(matrizDia[linha][0]);
                    temporarioString = temporarioString.substring(4, 6);
                    int temporario = Integer.parseInt(temporarioString);
                    if (temporario > maxMes) {
                        maxMes = temporario;
                    }
                }

                for (int linha = 0; linha < AppCovanalytics.contaLinhasDadosDiaFinal; linha++) {
                    String temporarioString = Integer.toString(matrizDia[linha][0]);
                    temporarioString = temporarioString.substring(4, 6);
                    int temporario = Integer.parseInt(temporarioString);
                    if (temporario < minMes) {
                        minMes = temporario;
                    }
                }
                break;

            case 2:

                for (int linha = 0; linha < AppCovanalytics.contaLinhasDadosAcumuladosFinal; linha++) {
                    String temporarioString = Integer.toString(matrizAcumulados[linha][0]);
                    temporarioString = temporarioString.substring(0, 4);
                    int temporario = Integer.parseInt(temporarioString);
                    if (temporario > maxAno) {
                        maxAno = temporario;
                    }
                }

                for (int linha = 0; linha < AppCovanalytics.contaLinhasDadosAcumuladosFinal; linha++) {
                    String temporarioString = Integer.toString(matrizAcumulados[linha][0]);
                    temporarioString = temporarioString.substring(0, 4);
                    int temporario = Integer.parseInt(temporarioString);
                    if (temporario < minAno) {
                        minAno = temporario;
                    }
                }

                for (int linha = 0; linha < AppCovanalytics.contaLinhasDadosAcumuladosFinal; linha++) {
                    String temporarioString = Integer.toString(matrizAcumulados[linha][0]);
                    temporarioString = temporarioString.substring(4, 6);
                    int temporario = Integer.parseInt(temporarioString);
                    if (temporario > maxMes) {
                        maxMes = temporario;
                    }
                }

                for (int linha = 0; linha < AppCovanalytics.contaLinhasDadosAcumuladosFinal; linha++) {
                    String temporarioString = Integer.toString(matrizAcumulados[linha][0]);
                    temporarioString = temporarioString.substring(4, 6);
                    int temporario = Integer.parseInt(temporarioString);
                    if (temporario < minMes) {
                        minMes = temporario;
                    }
                }
                break;
        }

        matrizMaxMin[0] = maxAno;
        matrizMaxMin[1] = minAno;
        matrizMaxMin[2] = maxMes;
        matrizMaxMin[3] = minMes;

        return matrizMaxMin;
    }

    //****************************************************************************************************************
    //*****Metodo para Ler a Matriz de Transição *********************************************************************
    //****************************************************************************************************************

    public static void lerMatrizTransicao() throws FileNotFoundException {
        int NUMEROCOLUNAS = 5;
        int NUNEROLINHAS = 5;
        Scanner ler = new Scanner(new File(AbrirMenus.nomeficheiro));
        for (int linha = 0; linha < NUNEROLINHAS; linha++) {
            for (int coluna = 0; coluna < NUMEROCOLUNAS; ) {
                String variavelTemp = ler.nextLine();
                if (variavelTemp.trim().length() != 0) {
                    AppCovanalytics.dadosMatrizTransicoes[linha][coluna] = Float.parseFloat(variavelTemp.substring(4));
                    coluna++;
                }
            }
        }
        ler.close();
    }
}
