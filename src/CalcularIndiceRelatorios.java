import java.time.DayOfWeek;
import java.time.LocalDate;

public class CalcularIndiceRelatorios {

    //********************************************************************************************
    //*****Metodo para calcular o indice na matriz para os relatorios numero casos no dia/semana**
    //********************************************************************************************

    public static int calcularIndiceRelatorioNumeroCasos_DiaSemana(int[][] matrizDados, int dataDia) {


        int indiceLinha = 0;
        while (dataDia != matrizDados[indiceLinha][0]) {
            indiceLinha++;
        }

        return indiceLinha;

    }

    //********************************************************************************************
    //*****Metodo para calcular o indice na matriz para os relatorios numero casos num mês *******
    //********************************************************************************************

    public static int[] calcularIndiceRelatorioNumeroCasos_Mes(int[][] matrizDados, int dataMes) {

        int[] indiceIntervaloMes = new int[2];
        int indiceLinha = 0;

        while (dataMes != matrizDados[indiceLinha][0] / 100) {
            indiceLinha++;
        }
        indiceIntervaloMes[0] = indiceLinha;
        indiceLinha++;

        do {
            indiceLinha++;
        } while (dataMes == matrizDados[indiceLinha][0] / 100 && indiceLinha < matrizDados.length - 1);

        if (indiceLinha == matrizDados.length - 1) {
            indiceIntervaloMes[1] = indiceLinha;
        } else {
            indiceIntervaloMes[1] = indiceLinha - 1;
        }

        return indiceIntervaloMes;
    }


    //********************************************************************************************
    //*****Metodo para calcular o indice na matriz para os relatorios numero casos num intervalo**
    //********************************************************************************************

    public static int[] calcularIndiceRelatorioNumeroCasos_IntervaloDias(int[][] matrizDados, int contaLinhasDados, int datasIntervaloInicial, int datasIntervaloFinal) {

        int[] datasIntervalo = {datasIntervaloInicial, datasIntervaloFinal};
        int[] indiceLinha = new int[2];


        for (int linha = 0; linha < contaLinhasDados; linha++) {
            if (matrizDados[linha][0] == datasIntervalo[0]) {
                indiceLinha[0] = linha;
            }
            if (matrizDados[linha][0] == datasIntervalo[1]) {
                indiceLinha[1] = linha;
            }
        }

        return indiceLinha;
    }

    //********************************************************************************************
    //*****Metodo para calcular o indice na matriz para os relatorios de periodo comparativo******
    //********************************************************************************************

    public static int[] calcularIndiceRelatorioComparativo_IntervaloDias(int[][] matrizDados, int contaLinhasMatriz, int datasIntervalo1, int datasIntervalo2, int datasIntervalo3, int datasIntervalo4) {

        int[] datasIntervalo = {datasIntervalo1, datasIntervalo2, datasIntervalo3, datasIntervalo4};
        int[] indiceLinha = new int[4];

        for (int linha = 0; linha < contaLinhasMatriz; linha++) {
            if (matrizDados[linha][0] == datasIntervalo[0]) {
                indiceLinha[0] = linha;
            }
            if (matrizDados[linha][0] == datasIntervalo[1]) {
                indiceLinha[1] = linha;
            }
            if (matrizDados[linha][0] == datasIntervalo[2]) {
                indiceLinha[2] = linha;
            }
            if (matrizDados[linha][0] == datasIntervalo[3]) {
                indiceLinha[3] = linha;
            }
        }

        return indiceLinha;

    }


    //********************************************************************************************
    //*****Metodo para calcular o dia da semana para uma data inserida pelo utilizador************
    //********************************************************************************************

    public static DayOfWeek calcularDiadaSemana (int data) {

        String dataInt=Integer.toString(data);
        String dataConvertida=dataInt.substring(0,4)+"-"+dataInt.substring(4,6)+"-"+dataInt.substring(6,8);

        LocalDate dataSemana;
        dataSemana=LocalDate.parse(dataConvertida);

        return dataSemana.getDayOfWeek();

    }
}
