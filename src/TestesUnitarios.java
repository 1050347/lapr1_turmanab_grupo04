
import java.io.FileNotFoundException;
import java.util.Arrays;

public class TestesUnitarios {

    public static void main(String[] args) throws FileNotFoundException {


        //********************************************************************
        //*****executar os processos dos Testes unitários**********
        //********************************************************************

        executarImprimirResultadoTestesUnitarios();


    }

    public static void executarImprimirResultadoTestesUnitarios() throws FileNotFoundException {

        //********************************************************************
        //*****carregar as matrizes com dados exemplos para os testes*********

        String ficheiroDadosDia = "registoTotalTestesUnitarios.csv";
        String ficheiroDadosAcumulados = "registoAcumuladoTestesUnitarios.csv";
        String ficheiroMatrizTransicoes = "matrizTransicoesTestesUnitarios.txt";

        AbrirMenus.nomeficheiro = ficheiroDadosDia;
        LerFicheiroDados.lerFicheiroDeDadosDiaTemp();
        LerFicheiroDados.transferirDadosMatrizDiaFinal(AppCovanalytics.dadosCovidDiaTemp, AppCovanalytics.dadosCovidDiaFinal);
        LerFicheiroDados.apagarDadosDiaTemp();

        AbrirMenus.nomeficheiro = ficheiroDadosAcumulados;
        LerFicheiroDados.lerFicheiroDeDadosAcumuladosTemp();
        LerFicheiroDados.transferirDadosMatrizAcumFinal(AppCovanalytics.dadosCovidAcumuladosTemp, AppCovanalytics.dadosCovidAcumuladosFinal);
        LerFicheiroDados.apagarDadosAcumuladosTemp();


        System.out.println();

        System.out.println("calculo de indice para posicao dia (ficheiro:relatorio dados diarios covid) " + (testarCalcularIndiceRelatorioNumeroCasos_DiaSemana(AppCovanalytics.dadosCovidDiaFinal, 20201110, 8) ? "- OK" : "- NOT OK" + "\n"));
        System.out.println("calculo de indice para agregado mes (ficheiro:relatorio dados diarios covid) " + (testarCalcularIndiceRelatorioNumeroCasos_Mes(AppCovanalytics.dadosCovidDiaFinal, 202012, 29, 59) ? "- OK" : "-NOT OK" + "\n"));
        System.out.println("calculo de indice para um intervalo datas (ficheiro:relatorio dados diarios covid) " + (testarCalcularIndiceRelatorioNumeroCasos_IntervaloDias(AppCovanalytics.dadosCovidDiaFinal, 432, 20211230, 20220107, 423, 431) ? "- OK" : "- NOT OK" + "\n"));
        System.out.println("calculo de indice para um intervalo comparativo datas (ficheiro:relatorio dados diarios covid) " + (testarCalcularIndiceRelatorioComparativo_IntervaloDias(AppCovanalytics.dadosCovidDiaFinal, 432, 20210601, 20210610, 20210701, 20210712, 211, 220, 241, 252) ? "- OK" : "- NOT OK" + "\n"));
        System.out.println("calculo de media para conjunto dados num intervalo de datas (ficheiro:relatorio dados diarios covid) " + (testarCalcularMediaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, 30, 49, 3, 3210.4) ? "- OK" : "- NOT OK" + "\n"));
        System.out.println("calculo de desvio padrao para conjunto dados num intervalo de datas (ficheiro:relatorio dados diarios covid) " + (testarCalcularDesvioPadraoIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, 30, 49, 3, 107.65105665993251) ? "- OK" : "- NOT OK" + "\n"));
        System.out.println("calculo do numero de linhas da matriz, testar processo de importação (ficheiro:relatorio dados diarios covid) " + (testarContarNumeroLinhasDados(AppCovanalytics.contaLinhasDadosDiaFinal, 432) ? "- OK" : "- NOT OK" + "\n"));
        System.out.println("calculo do numero de linhas da matriz, testar processo de limpeza dados temporarios (ficheiro:relatorio dados diarios covid) " + (testarContarNumeroLinhasDados(AppCovanalytics.contaLinhasDadosDiaTemp, 0) ? "- OK" : "- NOT OK" + "\n"));
        System.out.println("calculo do data mais atual da matriz, testar processo de controlo do indice linhas (ficheiro:relatorio dados diarios covid) " + (testarCalcularDataMaisAtualMatrizDados(AppCovanalytics.dadosCovidDiaFinal, AppCovanalytics.contaLinhasDadosDiaFinal, 20220107) ? "- OK" : "- NOT OK" + "\n"));


        System.out.println();

        System.out.println("calculo de indice para posicao dia (ficheiro:relatorio dados acumulado covid) " + (testarCalcularIndiceRelatorioNumeroCasos_DiaSemana(AppCovanalytics.dadosCovidAcumuladosFinal, 20211201, 62) ? "- OK" : "- NOT OK" + "\n"));
        System.out.println("calculo de indice para agregado mes (ficheiro:relatorio dados acumulados covid) " + (testarCalcularIndiceRelatorioNumeroCasos_Mes(AppCovanalytics.dadosCovidAcumuladosFinal, 202112, 62, 92) ? "- OK" : "-NOT OK" + "\n"));
        System.out.println("calculo de indice para um intervalo datas (ficheiro:relatorio dados acumulados covid) " + (testarCalcularIndiceRelatorioNumeroCasos_IntervaloDias(AppCovanalytics.dadosCovidAcumuladosFinal, 500, 20211012, 20211231, 12, 92) ? "- OK" : "- NOT OK" + "\n"));
        System.out.println("calculo de indice para um intervalo comparativo datas (ficheiro:relatorio dados acumulados covid) " + (testarCalcularIndiceRelatorioComparativo_IntervaloDias(AppCovanalytics.dadosCovidAcumuladosFinal, 500, 20211210, 20211215, 20211219, 20211221, 71, 76, 80, 82) ? "- OK" : "- NOT OK" + "\n"));
        System.out.println("calculo de desvio padrao para conjunto dados num intervalo de datas (ficheiro:relatorio dados acumulados covid) " + (testarCalcularDesvioPadraoIntervaloDatas(AppCovanalytics.dadosCovidAcumuladosFinal, 10, 13, 5, 8.94427190999916) ? "- OK" : "- NOT OK" + "\n"));
        System.out.println("calculo de somatorio para conjunto dados num intervalo de datas (ficheiro:relatorio dados acumulados covid) " + (testarcalcularSomaIntervaloDatas(AppCovanalytics.dadosCovidAcumuladosFinal, 30, 49, 4, 2467571) ? "- OK" : "- NOT OK" + "\n"));
        System.out.println("calculo de numero de linhas da matriz, testar processo de importação (ficheiro:relatorio dados acumulados covid) " + (testarContarNumeroLinhasDados(AppCovanalytics.contaLinhasDadosAcumuladosFinal, 93) ? "- OK" : "- NOT OK" + "\n"));
        System.out.println("calculo de numero de linhas da matriz, testar processo de limpeza dados temporários (ficheiro:relatorio dados acumulados covid) " + (testarContarNumeroLinhasDados(AppCovanalytics.contaLinhasDadosAcumuladosTemp, 0) ? "- OK" : "- NOT OK" + "\n"));
        System.out.println("calculo do data mais atual da matriz, testar processo de controlo do indice linhas (ficheiro:relatorio dados acumulados covid) " + (testarCalcularDataMaisAtualMatrizDados(AppCovanalytics.dadosCovidAcumuladosFinal, AppCovanalytics.contaLinhasDadosAcumuladosFinal, 20211231) ? "- OK" : "- NOT OK" + "\n"));


    }


    //********************************************************************************************
    //*****Metodo p/ TESTAR calculo indice na matriz p/ relatorios numero casos no dia/semana*****
    //********************************************************************************************


    private static boolean testarCalcularIndiceRelatorioNumeroCasos_DiaSemana(int[][] matrizDados, int dataDia, int indiceTeste) {

        if (CalcularIndiceRelatorios.calcularIndiceRelatorioNumeroCasos_DiaSemana(matrizDados, dataDia) == indiceTeste) {
            return true;
        } else {
            return false;
        }

    }

    //********************************************************************************************
    //*****Metodo p/ TESTAR calculo indice na matriz para os relatorios numero casos num mês******
    //********************************************************************************************

    private static boolean testarCalcularIndiceRelatorioNumeroCasos_Mes(int[][] matrizDados, int dataMes, int indiceTeste1, int indiceTeste2) {

        if ((CalcularIndiceRelatorios.calcularIndiceRelatorioNumeroCasos_Mes(matrizDados, dataMes))[0] == indiceTeste1 && (CalcularIndiceRelatorios.calcularIndiceRelatorioNumeroCasos_Mes(matrizDados, dataMes))[1] == indiceTeste2) {
            return true;
        } else {
            return false;
        }

    }


    //********************************************************************************************
    //*****Metodo p/ TESTAR calculo indice matriz para os relatorios numero casos num intervalo***
    //********************************************************************************************

    private static boolean testarCalcularIndiceRelatorioNumeroCasos_IntervaloDias(int[][] matrizDados, int contaLinhasDados, int datasIntervalo1, int datasIntervalo2, int indiceTeste1, int indiceTeste2) {

        int[] indiceTeste = {indiceTeste1, indiceTeste2};

        if (Arrays.equals(CalcularIndiceRelatorios.calcularIndiceRelatorioNumeroCasos_IntervaloDias(matrizDados, contaLinhasDados, datasIntervalo1, datasIntervalo2), indiceTeste)) {
            return true;
        } else {
            return false;
        }

    }


    //********************************************************************************************
    //*****Metodo p/ TESTAR calculo indice matriz para os relatorios comparativos num intervalo***
    //********************************************************************************************


    private static boolean testarCalcularIndiceRelatorioComparativo_IntervaloDias(int[][] matrizDados, int contaLinhasMatriz, int datasIntervalo1, int datasIntervalo2, int datasIntervalo3, int datasIntervalo4, int indiceTeste1, int indiceTeste2, int indiceTeste3, int indiceTeste4) {


        int[] indiceTeste = {indiceTeste1, indiceTeste2, indiceTeste3, indiceTeste4};

        if (Arrays.equals(CalcularIndiceRelatorios.calcularIndiceRelatorioComparativo_IntervaloDias(matrizDados, contaLinhasMatriz, datasIntervalo1, datasIntervalo2, datasIntervalo3, datasIntervalo4), indiceTeste)) {
            return true;
        } else {
            return false;
        }
    }

    //********************************************************************************************
    //*****Metodo p/ TESTAR calculo valores medios dos dados num intervalo de datas***************
    //********************************************************************************************

    private static boolean testarCalcularMediaIntervaloDatas(int[][] matrizDados, int indicedatasIntervalo1, int indicedatasIntervalo2, int colunaDados, double mediaTeste) {

        int[] indicedatasIntervalo = {indicedatasIntervalo1, indicedatasIntervalo2};

        if (ImprimirRelatorios.calcularMediaIntervaloDatas(matrizDados, indicedatasIntervalo, colunaDados) == mediaTeste) {
            return true;
        } else {
            return false;
        }
    }


    //********************************************************************************************
    //*****Metodo p/ TESTAR calculo desvio padrao dos dados num intervalo de datas****************
    //********************************************************************************************


    private static boolean testarCalcularDesvioPadraoIntervaloDatas(int[][] matrizDados, int datasIntervalo1, int datasIntervalo2, int colunaDados, double desvioPadraoTeste) {

        int[] datasIntervalo = {datasIntervalo1, datasIntervalo2};

        if (ImprimirRelatorios.calcularDesvioPadraoIntervaloDatas(matrizDados, datasIntervalo, colunaDados) == desvioPadraoTeste) {
            return true;
        } else {
            return false;
        }

    }


    //********************************************************************************************
    //*****Metodo p/ TESTAR calculo somatorio valoresdos dados num intervalo de datas*************
    //********************************************************************************************

    private static boolean testarcalcularSomaIntervaloDatas(int[][] matrizDados, int datasIntervalo1, int datasIntervalo2, int colunaDados, int somatorioTeste) {

        int[] datasIntervalo = {datasIntervalo1, datasIntervalo2};

        if (ImprimirRelatorios.calcularSomaIntervaloDatas(matrizDados, datasIntervalo, colunaDados) == somatorioTeste) {
            return true;
        } else {
            return false;
        }

    }

    //********************************************************************************************
    //*****Metodo p/ TESTAR o metodo de contagem do numero de linhas das matriz diaria************
    //********************************************************************************************

    private static boolean testarContarNumeroLinhasDados(int contaLinhasDados, int contaLinhasDadosTeste) {

        if (contaLinhasDados == contaLinhasDadosTeste) {
            return true;
        } else {
            return false;
        }

    }


    //********************************************************************************************
    //*****Metodo para testar extrair a data mais recente da Matriz [dados diarios ou acumulados]*
    //********************************************************************************************

    public static boolean testarCalcularDataMaisAtualMatrizDados(int[][] matrizDadosFinal, int contaLinhasDadosFinal, int dataDadosMaisAtualTeste) {

        int dataDadoMaisAtual = matrizDadosFinal[contaLinhasDadosFinal - 1][0];

        if (dataDadoMaisAtual == dataDadosMaisAtualTeste) {
            return true;
        } else {
            return false;
        }

    }

}