import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;


public class ModoNaoInterativo {

    public static void main(String[] args) throws FileNotFoundException {


        gravarFicheiroSaida();

    }


    /*
     ********************************************************************************************
     ***** Método para gerar o ficheiro com os dados introduzidos pelo utilizador no terminal
     ********************************************************************************************
     */

    public static void gravarFicheiroSaida() throws FileNotFoundException {


        if (AppCovanalytics.numeroVariaveis == 20) {

            //carregarMatrizesDados();
            verificarResolucaoTemporal();
            String dadosParaImprimir = "";
            exportarRelatoriosParaFicheiro(AppCovanalytics.parametrosUtilizador, dadosParaImprimir);

        } else if (AppCovanalytics.numeroVariaveis == 16) {

            // carregarMatrizesDados();
            verificarResolucaoTemporal();
            String dadosParaImprimir = "";
            exportarRelatoriosParaFicheiro(AppCovanalytics.parametrosUtilizador, dadosParaImprimir);
        } else if (AppCovanalytics.numeroVariaveis == 5) {

            //   carregarMatrizesDados();
            String dadosParaImprimir = "";
            exportarRelatoriosParaFicheiro(AppCovanalytics.parametrosUtilizador, dadosParaImprimir);

        }


    }

    /*
     ********************************************************************************************
     ***** Método para exportar os dados inseridos dos relatorios, acumulados na String
     ********************************************************************************************
     */


    public static void exportarRelatoriosParaFicheiro(String[] parametrosUtilizador, String dadosParaImprimir) throws FileNotFoundException {


        if (AppCovanalytics.numeroVariaveis == 20) {

            File ficheiro = new File(parametrosUtilizador[19]);
            PrintWriter exportar = new PrintWriter(ficheiro);


            String dadosAux = gerarRelatorioDiaSemanaMes_Data(parametrosUtilizador, dadosParaImprimir) + gerarRelatorioIntervaloDias(parametrosUtilizador, AbrirMenus.matrizDadosMenu, dadosParaImprimir) + gerarRelatorioComparacaoPeriodos(parametrosUtilizador, AbrirMenus.matrizDadosMenu, dadosParaImprimir) + gerarRelatorioPrevisional(dadosParaImprimir);

            String[] ficheiroFinal = dadosAux.split(" ");

            for (int linha = 0; linha < ficheiroFinal.length; linha++) {
                exportar.println(ficheiroFinal[linha]);
            }
            exportar.close();

        } else if (AppCovanalytics.numeroVariaveis == 16) {

            File ficheiro = new File(parametrosUtilizador[15]);
            PrintWriter exportar = new PrintWriter(ficheiro);

            String dadosAux = gerarRelatorioDiaSemanaMes_Data(parametrosUtilizador, dadosParaImprimir) + gerarRelatorioIntervaloDias(parametrosUtilizador, AbrirMenus.matrizDadosMenu, dadosParaImprimir) + gerarRelatorioComparacaoPeriodos(parametrosUtilizador, AbrirMenus.matrizDadosMenu, dadosParaImprimir);

            String[] ficheiroFinal = dadosAux.split(" ");

            for (int linha = 0; linha < ficheiroFinal.length; linha++) {
                exportar.println(ficheiroFinal[linha]);
            }
            exportar.close();
        } else if (AppCovanalytics.numeroVariaveis == 5) {

            File ficheiro = new File(parametrosUtilizador[4]);
            PrintWriter exportar = new PrintWriter(ficheiro);

            String dadosAux = gerarRelatorioPrevisional(dadosParaImprimir);

            String[] ficheiroFinal = dadosAux.split(" ");

            for (int linha = 0; linha < ficheiroFinal.length; linha++) {
                exportar.println(ficheiroFinal[linha]);
            }
            exportar.close();

        }


    }


    /*
     ********************************************************************************************
     ***** Método para carregar as matrizes de dados introduzidas como parametros no terminal
     ********************************************************************************************
     */

    public static void carregarMatrizesDados() throws FileNotFoundException {

        if (AppCovanalytics.numeroVariaveis == 20) {

            AbrirMenus.nomeficheiro = AppCovanalytics.parametrosUtilizador[16];
            LerFicheiroDados.lerFicheiroDeDadosDiaTemp();
            LerFicheiroDados.transferirDadosMatrizDiaFinal(AppCovanalytics.dadosCovidDiaTemp, AppCovanalytics.dadosCovidDiaFinal);
            LerFicheiroDados.apagarDadosDiaTemp();
            AbrirMenus.nomeficheiro = AppCovanalytics.parametrosUtilizador[17];
            LerFicheiroDados.lerFicheiroDeDadosAcumuladosTemp();
            LerFicheiroDados.transferirDadosMatrizAcumFinal(AppCovanalytics.dadosCovidAcumuladosTemp, AppCovanalytics.dadosCovidAcumuladosFinal);
            LerFicheiroDados.apagarDadosAcumuladosTemp();
            AbrirMenus.nomeficheiro = AppCovanalytics.parametrosUtilizador[18];
            LerFicheiroDados.lerMatrizTransicao();

        } else if (AppCovanalytics.numeroVariaveis == 16) {


            AbrirMenus.nomeficheiro = AppCovanalytics.parametrosUtilizador[14];
            LerFicheiroDados.lerFicheiroDeDadosAcumuladosTemp();
            LerFicheiroDados.transferirDadosMatrizAcumFinal(AppCovanalytics.dadosCovidAcumuladosTemp, AppCovanalytics.dadosCovidAcumuladosFinal);
            LerFicheiroDados.apagarDadosAcumuladosTemp();

        } else if (AppCovanalytics.numeroVariaveis == 5) {

            AbrirMenus.nomeficheiro = AppCovanalytics.parametrosUtilizador[2];
            LerFicheiroDados.lerFicheiroDeDadosDiaTemp();
            LerFicheiroDados.transferirDadosMatrizDiaFinal(AppCovanalytics.dadosCovidDiaTemp, AppCovanalytics.dadosCovidDiaFinal);
            LerFicheiroDados.apagarDadosDiaTemp();
            AbrirMenus.nomeficheiro = AppCovanalytics.parametrosUtilizador[3];
            LerFicheiroDados.lerMatrizTransicao();


        }


    }


    /*
     ********************************************************************************************
     ***** Método para verificar a resolução temporaral introduzida no parametros no terminal
     ********************************************************************************************
     */

    public static void verificarResolucaoTemporal() {

        //verificar se o utilizador pediu agrupar os dados diarios, semanal e mensal e converter para os indices da matriz menu
        if (Integer.parseInt(AppCovanalytics.parametrosUtilizador[1]) == 0) {
            AbrirMenus.matrizDadosMenu[2][0] = 2;
        } else if (Integer.parseInt(AppCovanalytics.parametrosUtilizador[1]) == 1) {
            AbrirMenus.matrizDadosMenu[2][0] = 3;
        } else {
            AbrirMenus.matrizDadosMenu[2][0] = 4;
        }

    }


    /*
     ********************************************************************************************
     ***** Método para extrair os dados das matrizes diarias e acumuladas
     ***** o parametro [1] dia, semana ou mes define o agrupamento
     ***** considerada a data inserida na posição [3]
     ********************************************************************************************
     */


    public static String gerarRelatorioDiaSemanaMes_Data(String[] parametrosUtilizador, String dadosParaImprimir) {

        //atribuir a data (parametro 1) na matriz de dados de menus

        AbrirMenus.matrizDadosMenu[3][0] = converterFormatoDataAAAAMMDD(parametrosUtilizador[3]);

        if (AppCovanalytics.numeroVariaveis == 20) {

            //definir condição de geração de reporte diario / semanal ou mensal

            switch (AbrirMenus.matrizDadosMenu[2][0]) {


                case 2:

                    //calculo da posição da matriz para escrever os valores para a String (matriz total dia)
                    int indiceLinha = CalcularIndiceRelatorios.calcularIndiceRelatorioNumeroCasos_DiaSemana(AppCovanalytics.dadosCovidDiaFinal, AbrirMenus.matrizDadosMenu[3][0]);


                    //preencher a string com os dados de cabecalho e dados da linha
                    dadosParaImprimir = dadosParaImprimir + "RelatorioNumerosTotaisDiario_Dia" + " ";
                    dadosParaImprimir = dadosParaImprimir + ExportarRelatorios.CABECALHOFICHEIRODIARIO + " ";
                    dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidDiaFinal[indiceLinha][0]);
                    dadosParaImprimir = dadosParaImprimir + ",";
                    dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidDiaFinal[indiceLinha][2]);
                    dadosParaImprimir = dadosParaImprimir + ",";
                    dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidDiaFinal[indiceLinha][3]);
                    dadosParaImprimir = dadosParaImprimir + ",";
                    dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidDiaFinal[indiceLinha][4]);
                    dadosParaImprimir = dadosParaImprimir + ",";
                    dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidDiaFinal[indiceLinha][5]);
                    dadosParaImprimir = dadosParaImprimir + " ";
                    dadosParaImprimir = dadosParaImprimir + " ";

                    //calculo da posição da matriz para escrever os valores para a String (matriz total acumulado)
                    indiceLinha = CalcularIndiceRelatorios.calcularIndiceRelatorioNumeroCasos_DiaSemana(AppCovanalytics.dadosCovidAcumuladosFinal, AbrirMenus.matrizDadosMenu[3][0]);

                    //preencher a string com os dados de cabeclho e dados da linha


                    dadosParaImprimir = dadosParaImprimir + "RelatorioNumerosTotaisAcumulado_Dia" + " ";
                    dadosParaImprimir = dadosParaImprimir + ExportarRelatorios.CABECALHOFICHEIROACUMULADO + " ";
                    dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidAcumuladosFinal[indiceLinha][0]);
                    dadosParaImprimir = dadosParaImprimir + ",";
                    dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidAcumuladosFinal[indiceLinha][2]);
                    dadosParaImprimir = dadosParaImprimir + ",";
                    dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidAcumuladosFinal[indiceLinha][3]);
                    dadosParaImprimir = dadosParaImprimir + ",";
                    dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidAcumuladosFinal[indiceLinha][4]);
                    dadosParaImprimir = dadosParaImprimir + ",";
                    dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidAcumuladosFinal[indiceLinha][5]);
                    dadosParaImprimir = dadosParaImprimir + " ";
                    dadosParaImprimir = dadosParaImprimir + " ";
                    break;

                case 3:

                    //calculo da posição da matriz para escrever os valores para a String (matriz total dia)
                    int duracaoSemana = 7;
                    int contadorLinhas = 0;
                    indiceLinha = CalcularIndiceRelatorios.calcularIndiceRelatorioNumeroCasos_DiaSemana(AppCovanalytics.dadosCovidDiaFinal, AbrirMenus.matrizDadosMenu[3][0]);
                    duracaoSemana = duracaoSemana - ImprimirRelatorios.calcularNumeroDiasDescontarSemana(AbrirMenus.matrizDadosMenu[3][0]);

                    //preencher a string com os dados de cabecalho e dados da linha
                    dadosParaImprimir = dadosParaImprimir + "RelatorioNumerosTotaisDiario_Semana" + " ";
                    dadosParaImprimir = dadosParaImprimir + ExportarRelatorios.CABECALHOFICHEIRODIARIO + " ";

                    while (contadorLinhas < duracaoSemana && indiceLinha < AppCovanalytics.contaLinhasDadosDiaFinal) {
                        dadosParaImprimir = dadosParaImprimir + AppCovanalytics.dadosCovidDiaFinal[indiceLinha][0];
                        dadosParaImprimir = dadosParaImprimir + ",";
                        dadosParaImprimir = dadosParaImprimir + AppCovanalytics.dadosCovidDiaFinal[indiceLinha][2];
                        dadosParaImprimir = dadosParaImprimir + ",";
                        dadosParaImprimir = dadosParaImprimir + AppCovanalytics.dadosCovidDiaFinal[indiceLinha][3];
                        dadosParaImprimir = dadosParaImprimir + ",";
                        dadosParaImprimir = dadosParaImprimir + AppCovanalytics.dadosCovidDiaFinal[indiceLinha][4];
                        dadosParaImprimir = dadosParaImprimir + ",";
                        dadosParaImprimir = dadosParaImprimir + AppCovanalytics.dadosCovidDiaFinal[indiceLinha][5];
                        dadosParaImprimir = dadosParaImprimir + " ";
                        indiceLinha++;
                        contadorLinhas++;
                    }
                    dadosParaImprimir = dadosParaImprimir + " ";

                    //calculo da posição da matriz para escrever os valores para a String (matriz total acumulado)

                    duracaoSemana = 7;
                    contadorLinhas = 0;
                    indiceLinha = CalcularIndiceRelatorios.calcularIndiceRelatorioNumeroCasos_DiaSemana(AppCovanalytics.dadosCovidAcumuladosFinal, AbrirMenus.matrizDadosMenu[3][0]);
                    duracaoSemana = duracaoSemana - ImprimirRelatorios.calcularNumeroDiasDescontarSemana(AbrirMenus.matrizDadosMenu[3][0]);

                    //preencher a string com os dados de cabecalho e dados da linha
                    dadosParaImprimir = dadosParaImprimir + "RelatorioNumerosTotaisAcumulado_Semana" + " ";
                    dadosParaImprimir = dadosParaImprimir + ExportarRelatorios.CABECALHOFICHEIROACUMULADO + " ";

                    while (contadorLinhas < duracaoSemana && indiceLinha < AppCovanalytics.contaLinhasDadosDiaFinal) {
                        dadosParaImprimir = dadosParaImprimir + AppCovanalytics.dadosCovidAcumuladosFinal[indiceLinha][0];
                        dadosParaImprimir = dadosParaImprimir + ",";
                        dadosParaImprimir = dadosParaImprimir + AppCovanalytics.dadosCovidAcumuladosFinal[indiceLinha][2];
                        dadosParaImprimir = dadosParaImprimir + ",";
                        dadosParaImprimir = dadosParaImprimir + AppCovanalytics.dadosCovidAcumuladosFinal[indiceLinha][3];
                        dadosParaImprimir = dadosParaImprimir + ",";
                        dadosParaImprimir = dadosParaImprimir + AppCovanalytics.dadosCovidAcumuladosFinal[indiceLinha][4];
                        dadosParaImprimir = dadosParaImprimir + ",";
                        dadosParaImprimir = dadosParaImprimir + AppCovanalytics.dadosCovidAcumuladosFinal[indiceLinha][5];
                        dadosParaImprimir = dadosParaImprimir + " ";
                        indiceLinha++;
                        contadorLinhas++;
                    }
                    dadosParaImprimir = dadosParaImprimir + " ";
                    break;

                case 4:

                    AbrirMenus.matrizDadosMenu[3][0] = converterFormatoDataAAAMM(parametrosUtilizador[3]);
                    //calculo da posição da matriz para escrever os valores para a String (matriz total dia)
                    int[] indiceIntervaloMes = CalcularIndiceRelatorios.calcularIndiceRelatorioNumeroCasos_Mes(AppCovanalytics.dadosCovidDiaFinal, AbrirMenus.matrizDadosMenu[3][0]);

                    //preencher a string com os dados de cabecalho e dados da linha
                    dadosParaImprimir = dadosParaImprimir + "RelatorioNumerosTotaisDiario_Mensal" + " ";
                    dadosParaImprimir = dadosParaImprimir + ExportarRelatorios.CABECALHOFICHEIRODIARIO + " ";

                    dadosParaImprimir = dadosParaImprimir + ((AppCovanalytics.dadosCovidDiaFinal[indiceIntervaloMes[0]][0]) / 100);
                    dadosParaImprimir = dadosParaImprimir + ",";
                    dadosParaImprimir = dadosParaImprimir + (ImprimirRelatorios.calcularSomaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceIntervaloMes, 2));
                    dadosParaImprimir = dadosParaImprimir + ",";
                    dadosParaImprimir = dadosParaImprimir + (ImprimirRelatorios.calcularSomaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceIntervaloMes, 3));
                    dadosParaImprimir = dadosParaImprimir + ",";
                    dadosParaImprimir = dadosParaImprimir + (ImprimirRelatorios.calcularSomaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceIntervaloMes, 4));
                    dadosParaImprimir = dadosParaImprimir + ",";
                    dadosParaImprimir = dadosParaImprimir + (ImprimirRelatorios.calcularSomaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceIntervaloMes, 5));
                    dadosParaImprimir = dadosParaImprimir + " ";
                    dadosParaImprimir = dadosParaImprimir + " ";

                    //calculo da posição da matriz para escrever os valores para a String (matriz total acumulado)
                    indiceIntervaloMes = CalcularIndiceRelatorios.calcularIndiceRelatorioNumeroCasos_Mes(AppCovanalytics.dadosCovidAcumuladosFinal, AbrirMenus.matrizDadosMenu[3][0]);

                    //preencher a string com os dados de cabecalho e dados da linha
                    dadosParaImprimir = dadosParaImprimir + "RelatorioNumerosTotaisAcumulado_Mensal" + " ";
                    dadosParaImprimir = dadosParaImprimir + ExportarRelatorios.CABECALHOFICHEIROACUMULADO + " ";

                    dadosParaImprimir = dadosParaImprimir + ((AppCovanalytics.dadosCovidAcumuladosFinal[indiceIntervaloMes[0]][0]) / 100);
                    dadosParaImprimir = dadosParaImprimir + ",";
                    dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidAcumuladosFinal[indiceIntervaloMes[1]][2]);
                    dadosParaImprimir = dadosParaImprimir + ",";
                    dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidAcumuladosFinal[indiceIntervaloMes[1]][3]);
                    dadosParaImprimir = dadosParaImprimir + ",";
                    dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidAcumuladosFinal[indiceIntervaloMes[1]][4]);
                    dadosParaImprimir = dadosParaImprimir + ",";
                    dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidAcumuladosFinal[indiceIntervaloMes[1]][5]);
                    dadosParaImprimir = dadosParaImprimir + " ";
                    dadosParaImprimir = dadosParaImprimir + " ";

            }
        } else if (AppCovanalytics.numeroVariaveis == 16) {

            switch (AbrirMenus.matrizDadosMenu[2][0]) {


                case 2:

                    //calculo da posição da matriz para escrever os valores para a String (matriz total acumulado)
                    int indiceLinha = CalcularIndiceRelatorios.calcularIndiceRelatorioNumeroCasos_DiaSemana(AppCovanalytics.dadosCovidAcumuladosFinal, AbrirMenus.matrizDadosMenu[3][0]);


                    //preencher a string com os dados de cabecalho e dados da linha
                    dadosParaImprimir = dadosParaImprimir + "RelatorioNumerosTotaisAcumulado_Dia" + " ";
                    dadosParaImprimir = dadosParaImprimir + ExportarRelatorios.CABECALHOFICHEIROACUMULADO + " ";
                    dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidAcumuladosFinal[indiceLinha][0]);
                    dadosParaImprimir = dadosParaImprimir + ",";
                    dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidAcumuladosFinal[indiceLinha][2]);
                    dadosParaImprimir = dadosParaImprimir + ",";
                    dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidAcumuladosFinal[indiceLinha][3]);
                    dadosParaImprimir = dadosParaImprimir + ",";
                    dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidAcumuladosFinal[indiceLinha][4]);
                    dadosParaImprimir = dadosParaImprimir + ",";
                    dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidAcumuladosFinal[indiceLinha][5]);
                    dadosParaImprimir = dadosParaImprimir + " ";
                    dadosParaImprimir = dadosParaImprimir + " ";
                    break;

                case 3:

                    //calculo da posição da matriz para escrever os valores para a String (matriz total acumulado)
                    int duracaoSemana = 7;
                    int contadorLinhas = 0;
                    indiceLinha = CalcularIndiceRelatorios.calcularIndiceRelatorioNumeroCasos_DiaSemana(AppCovanalytics.dadosCovidAcumuladosFinal, AbrirMenus.matrizDadosMenu[3][0]);
                    duracaoSemana = duracaoSemana - ImprimirRelatorios.calcularNumeroDiasDescontarSemana(AbrirMenus.matrizDadosMenu[3][0]);

                    //preencher a string com os dados de cabecalho e dados da linha
                    dadosParaImprimir = dadosParaImprimir + "RelatorioNumerosTotaisAcumulado_Semana" + " ";
                    dadosParaImprimir = dadosParaImprimir + ExportarRelatorios.CABECALHOFICHEIROACUMULADO + " ";

                    while (contadorLinhas < duracaoSemana && indiceLinha < AppCovanalytics.contaLinhasDadosAcumuladosFinal) {
                        dadosParaImprimir = dadosParaImprimir + AppCovanalytics.dadosCovidAcumuladosFinal[indiceLinha][0];
                        dadosParaImprimir = dadosParaImprimir + ",";
                        dadosParaImprimir = dadosParaImprimir + AppCovanalytics.dadosCovidAcumuladosFinal[indiceLinha][2];
                        dadosParaImprimir = dadosParaImprimir + ",";
                        dadosParaImprimir = dadosParaImprimir + AppCovanalytics.dadosCovidAcumuladosFinal[indiceLinha][3];
                        dadosParaImprimir = dadosParaImprimir + ",";
                        dadosParaImprimir = dadosParaImprimir + AppCovanalytics.dadosCovidAcumuladosFinal[indiceLinha][4];
                        dadosParaImprimir = dadosParaImprimir + ",";
                        dadosParaImprimir = dadosParaImprimir + AppCovanalytics.dadosCovidAcumuladosFinal[indiceLinha][5];
                        dadosParaImprimir = dadosParaImprimir + " ";
                        indiceLinha++;
                        contadorLinhas++;
                    }
                    dadosParaImprimir = dadosParaImprimir + " ";
                    break;

                case 4:

                    AbrirMenus.matrizDadosMenu[3][0] = converterFormatoDataAAAMM(parametrosUtilizador[3]);
                    //calculo da posição da matriz para escrever os valores para a String (matriz total acumulado)
                    int[] indiceIntervaloMes = CalcularIndiceRelatorios.calcularIndiceRelatorioNumeroCasos_Mes(AppCovanalytics.dadosCovidAcumuladosFinal, AbrirMenus.matrizDadosMenu[3][0]);

                    //preencher a string com os dados de cabecalho e dados da linha
                    dadosParaImprimir = dadosParaImprimir + "RelatorioNumerosTotaisAcumulado_Mensal" + " ";
                    dadosParaImprimir = dadosParaImprimir + ExportarRelatorios.CABECALHOFICHEIROACUMULADO + " ";

                    dadosParaImprimir = dadosParaImprimir + ((AppCovanalytics.dadosCovidAcumuladosFinal[indiceIntervaloMes[0]][0]) / 100);
                    dadosParaImprimir = dadosParaImprimir + ",";
                    dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidAcumuladosFinal[indiceIntervaloMes[1]][2]);
                    dadosParaImprimir = dadosParaImprimir + ",";
                    dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidAcumuladosFinal[indiceIntervaloMes[1]][3]);
                    dadosParaImprimir = dadosParaImprimir + ",";
                    dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidAcumuladosFinal[indiceIntervaloMes[1]][4]);
                    dadosParaImprimir = dadosParaImprimir + ",";
                    dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidAcumuladosFinal[indiceIntervaloMes[1]][5]);
                    dadosParaImprimir = dadosParaImprimir + " ";
                    dadosParaImprimir = dadosParaImprimir + " ";
                    break;


            }

        }
        return dadosParaImprimir;

    }



    /*
     ********************************************************************************************
     ***** Método para extrair os dados das matrizes diarias e acumuladas
     ***** o parametro [3] e [5] definem o intervalo de datas
     ***** relatorio de intervalo de datas
     ********************************************************************************************
     */

    public static String gerarRelatorioIntervaloDias(String[] parametrosUtilizador, int[][] matrizDadosMenu, String dadosParaImprimir) {


        AbrirMenus.matrizDadosMenu[3][0] = converterFormatoDataAAAAMMDD(parametrosUtilizador[3]);
        AbrirMenus.matrizDadosMenu[3][1] = converterFormatoDataAAAAMMDD(parametrosUtilizador[5]);


        if (AppCovanalytics.numeroVariaveis == 20) {


            //calculo da posição da matriz para escrever os valores para a String (matriz total dia)
            int[] indiceLinha = CalcularIndiceRelatorios.calcularIndiceRelatorioNumeroCasos_IntervaloDias(AppCovanalytics.dadosCovidDiaFinal, AppCovanalytics.contaLinhasDadosDiaFinal, AbrirMenus.matrizDadosMenu[3][0], AbrirMenus.matrizDadosMenu[3][1]);

            //preencher a string com os dados de cabecalho e dados da linha
            dadosParaImprimir = dadosParaImprimir + "RelatorioNumerosTotaisDiario_IntervaloDatas" + " ";
            dadosParaImprimir = dadosParaImprimir + ExportarRelatorios.CABECALHOFICHEIRODIARIO + " ";

            for (int linha = indiceLinha[0]; linha <= indiceLinha[1]; linha++) {
                dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidDiaFinal[linha][0]);
                dadosParaImprimir = dadosParaImprimir + ",";
                dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidDiaFinal[linha][2]);
                dadosParaImprimir = dadosParaImprimir + ",";
                dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidDiaFinal[linha][3]);
                dadosParaImprimir = dadosParaImprimir + ",";
                dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidDiaFinal[linha][4]);
                dadosParaImprimir = dadosParaImprimir + ",";
                dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidDiaFinal[linha][5]);
                dadosParaImprimir = dadosParaImprimir + " ";

            }
            dadosParaImprimir = dadosParaImprimir + " ";

        }

        if (AppCovanalytics.numeroVariaveis == 20 || AppCovanalytics.numeroVariaveis == 16) {

            //calculo da posição da matriz para escrever os valores para a String (matriz total acumulado)
            int[] indiceLinha = CalcularIndiceRelatorios.calcularIndiceRelatorioNumeroCasos_IntervaloDias(AppCovanalytics.dadosCovidAcumuladosFinal, AppCovanalytics.contaLinhasDadosAcumuladosFinal, AbrirMenus.matrizDadosMenu[3][0], AbrirMenus.matrizDadosMenu[3][1]);

            //preencher a string com os dados de cabecalho e dados da linha
            dadosParaImprimir = dadosParaImprimir + "RelatorioNumerosTotaisAcumulado_IntervaloDatas" + " ";
            dadosParaImprimir = dadosParaImprimir + ExportarRelatorios.CABECALHOFICHEIROACUMULADO + " ";

            for (int linha = indiceLinha[0]; linha <= indiceLinha[1]; linha++) {
                dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidAcumuladosFinal[linha][0]);
                dadosParaImprimir = dadosParaImprimir + ",";
                dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidAcumuladosFinal[linha][2]);
                dadosParaImprimir = dadosParaImprimir + ",";
                dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidAcumuladosFinal[linha][3]);
                dadosParaImprimir = dadosParaImprimir + ",";
                dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidAcumuladosFinal[linha][4]);
                dadosParaImprimir = dadosParaImprimir + ",";
                dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidAcumuladosFinal[linha][5]);
                dadosParaImprimir = dadosParaImprimir + " ";

            }
            dadosParaImprimir = dadosParaImprimir + " ";

        }

        return dadosParaImprimir;

    }

    /*
     ********************************************************************************************
     ***** Método para extrair os dados das matrizes diarias e acumuladas
     ***** o parametro [7], [9], [11] e [13] definem o intervalo de datas (inicial e final)
     ***** relatorio de comparação de datas
     ********************************************************************************************
     */


    public static String gerarRelatorioComparacaoPeriodos(String[] parametrosUtilizador, int[][] matrizDadosMenu, String dadosParaImprimir) {


        AbrirMenus.matrizDadosMenu[3][0] = converterFormatoDataAAAAMMDD(parametrosUtilizador[7]);
        AbrirMenus.matrizDadosMenu[3][1] = converterFormatoDataAAAAMMDD(parametrosUtilizador[9]);
        AbrirMenus.matrizDadosMenu[3][2] = converterFormatoDataAAAAMMDD(parametrosUtilizador[11]);
        AbrirMenus.matrizDadosMenu[3][3] = converterFormatoDataAAAAMMDD(parametrosUtilizador[13]);


        if (AppCovanalytics.numeroVariaveis == 20) {


            //calculo da posição da matriz para escrever os valores para a String (matriz total dia)
            int[] indiceIntervaloMes = CalcularIndiceRelatorios.calcularIndiceRelatorioComparativo_IntervaloDias(AppCovanalytics.dadosCovidDiaFinal, AppCovanalytics.contaLinhasDadosDiaFinal, matrizDadosMenu[3][0], matrizDadosMenu[3][1], matrizDadosMenu[3][2], matrizDadosMenu[3][3]);

            int contadorP1 = indiceIntervaloMes[0];
            int contadorP2 = indiceIntervaloMes[2];

            //preencher a string com os dados de cabecalho e dados da linha
            dadosParaImprimir = dadosParaImprimir + "RelatorioNumerosTotaisDiario_ComparacaoPeriodos" + " ";
            dadosParaImprimir = dadosParaImprimir + ExportarRelatorios.CABECALHOFICHEIRODIARIO + " ";


            while (contadorP1 <= indiceIntervaloMes[1] || contadorP2 <= indiceIntervaloMes[3]) {

                if (contadorP1 > indiceIntervaloMes[1]) {
                    dadosParaImprimir = dadosParaImprimir + "-";
                    dadosParaImprimir = dadosParaImprimir + ",";
                    dadosParaImprimir = dadosParaImprimir + "-";
                    dadosParaImprimir = dadosParaImprimir + ",";
                    dadosParaImprimir = dadosParaImprimir + "-";
                    dadosParaImprimir = dadosParaImprimir + ",";
                    dadosParaImprimir = dadosParaImprimir + "-";
                    dadosParaImprimir = dadosParaImprimir + ",";
                    dadosParaImprimir = dadosParaImprimir + "-";
                    dadosParaImprimir = dadosParaImprimir + ",";
                } else {
                    dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidDiaFinal[contadorP1][0]);
                    dadosParaImprimir = dadosParaImprimir + ",";
                    dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidDiaFinal[contadorP1][2]);
                    dadosParaImprimir = dadosParaImprimir + ",";
                    dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidDiaFinal[contadorP1][3]);
                    dadosParaImprimir = dadosParaImprimir + ",";
                    dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidDiaFinal[contadorP1][4]);
                    dadosParaImprimir = dadosParaImprimir + ",";
                    dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidDiaFinal[contadorP1][5]);
                    dadosParaImprimir = dadosParaImprimir + ",";
                }


                if (contadorP2 > indiceIntervaloMes[3]) {
                    dadosParaImprimir = dadosParaImprimir + "-";
                    dadosParaImprimir = dadosParaImprimir + ",";
                    dadosParaImprimir = dadosParaImprimir + "-";
                    dadosParaImprimir = dadosParaImprimir + ",";
                    dadosParaImprimir = dadosParaImprimir + "-";
                    dadosParaImprimir = dadosParaImprimir + ",";
                    dadosParaImprimir = dadosParaImprimir + "-";
                    dadosParaImprimir = dadosParaImprimir + ",";
                    dadosParaImprimir = dadosParaImprimir + "-";

                } else {

                    dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidDiaFinal[contadorP2][0]);
                    dadosParaImprimir = dadosParaImprimir + ",";
                    dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidDiaFinal[contadorP2][2]);
                    dadosParaImprimir = dadosParaImprimir + ",";
                    dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidDiaFinal[contadorP2][3]);
                    dadosParaImprimir = dadosParaImprimir + ",";
                    dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidDiaFinal[contadorP2][4]);
                    dadosParaImprimir = dadosParaImprimir + ",";
                    dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidDiaFinal[contadorP2][5]);

                }
                dadosParaImprimir = dadosParaImprimir + " ";
                contadorP1++;
                contadorP2++;

            }

            int[] indiceIntervaloMedia1 = new int[2];
            indiceIntervaloMedia1[0] = indiceIntervaloMes[0];
            indiceIntervaloMedia1[1] = indiceIntervaloMes[1];
            int[] indiceIntervaloMedia2 = new int[2];
            indiceIntervaloMedia2[0] = indiceIntervaloMes[2];
            indiceIntervaloMedia2[1] = indiceIntervaloMes[3];


            dadosParaImprimir = dadosParaImprimir + "valoresMedios" + ",";
            dadosParaImprimir = dadosParaImprimir + (ImprimirRelatorios.calcularMediaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceIntervaloMedia1, 2));
            dadosParaImprimir = dadosParaImprimir + ",";
            dadosParaImprimir = dadosParaImprimir + (ImprimirRelatorios.calcularMediaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceIntervaloMedia1, 3));
            dadosParaImprimir = dadosParaImprimir + ",";
            dadosParaImprimir = dadosParaImprimir + (ImprimirRelatorios.calcularMediaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceIntervaloMedia1, 4));
            dadosParaImprimir = dadosParaImprimir + ",";
            dadosParaImprimir = dadosParaImprimir + (ImprimirRelatorios.calcularMediaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceIntervaloMedia1, 5));
            dadosParaImprimir = dadosParaImprimir + ",";

            dadosParaImprimir = dadosParaImprimir + "valoresMedios" + ",";
            dadosParaImprimir = dadosParaImprimir + (ImprimirRelatorios.calcularMediaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceIntervaloMedia2, 2));
            dadosParaImprimir = dadosParaImprimir + ",";
            dadosParaImprimir = dadosParaImprimir + (ImprimirRelatorios.calcularMediaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceIntervaloMedia2, 3));
            dadosParaImprimir = dadosParaImprimir + ",";
            dadosParaImprimir = dadosParaImprimir + (ImprimirRelatorios.calcularMediaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceIntervaloMedia2, 4));
            dadosParaImprimir = dadosParaImprimir + ",";
            dadosParaImprimir = dadosParaImprimir + (ImprimirRelatorios.calcularMediaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceIntervaloMedia2, 5));
            dadosParaImprimir = dadosParaImprimir + " ";


            dadosParaImprimir = dadosParaImprimir + "desvioPadrao" + ",";
            dadosParaImprimir = dadosParaImprimir + (ImprimirRelatorios.calcularDesvioPadraoIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceIntervaloMedia1, 2));
            dadosParaImprimir = dadosParaImprimir + ",";
            dadosParaImprimir = dadosParaImprimir + (ImprimirRelatorios.calcularDesvioPadraoIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceIntervaloMedia1, 3));
            dadosParaImprimir = dadosParaImprimir + ",";
            dadosParaImprimir = dadosParaImprimir + (ImprimirRelatorios.calcularDesvioPadraoIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceIntervaloMedia1, 4));
            dadosParaImprimir = dadosParaImprimir + ",";
            dadosParaImprimir = dadosParaImprimir + (ImprimirRelatorios.calcularDesvioPadraoIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceIntervaloMedia1, 5));
            dadosParaImprimir = dadosParaImprimir + ",";

            dadosParaImprimir = dadosParaImprimir + "desvioPadrao" + ",";
            dadosParaImprimir = dadosParaImprimir + (ImprimirRelatorios.calcularDesvioPadraoIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceIntervaloMedia2, 2));
            dadosParaImprimir = dadosParaImprimir + ",";
            dadosParaImprimir = dadosParaImprimir + (ImprimirRelatorios.calcularDesvioPadraoIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceIntervaloMedia2, 3));
            dadosParaImprimir = dadosParaImprimir + ",";
            dadosParaImprimir = dadosParaImprimir + (ImprimirRelatorios.calcularDesvioPadraoIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceIntervaloMedia2, 4));
            dadosParaImprimir = dadosParaImprimir + ",";
            dadosParaImprimir = dadosParaImprimir + (ImprimirRelatorios.calcularDesvioPadraoIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceIntervaloMedia2, 5));
            dadosParaImprimir = dadosParaImprimir + " ";
            dadosParaImprimir = dadosParaImprimir + " ";


        }

        //calculo da posição da matriz para escrever os valores para a String (matriz total acumulado)

        int[] indiceIntervaloMes = CalcularIndiceRelatorios.calcularIndiceRelatorioComparativo_IntervaloDias(AppCovanalytics.dadosCovidAcumuladosFinal, AppCovanalytics.contaLinhasDadosAcumuladosFinal, matrizDadosMenu[3][0], matrizDadosMenu[3][1], matrizDadosMenu[3][2], matrizDadosMenu[3][3]);

        int contadorP1 = indiceIntervaloMes[0];
        int contadorP2 = indiceIntervaloMes[2];

        //preencher a string com os dados de cabecalho e dados da linha
        dadosParaImprimir = dadosParaImprimir + "RelatorioNumerosTotaisAcumulado_ComparacaoPeriodos" + " ";
        dadosParaImprimir = dadosParaImprimir + ExportarRelatorios.CABECALHOFICHEIROACUMULADO + " ";


        while (contadorP1 <= indiceIntervaloMes[1] || contadorP2 <= indiceIntervaloMes[3]) {

            if (contadorP1 > indiceIntervaloMes[1]) {
                dadosParaImprimir = dadosParaImprimir + "-";
                dadosParaImprimir = dadosParaImprimir + ",";
                dadosParaImprimir = dadosParaImprimir + "-";
                dadosParaImprimir = dadosParaImprimir + ",";
                dadosParaImprimir = dadosParaImprimir + "-";
                dadosParaImprimir = dadosParaImprimir + ",";
                dadosParaImprimir = dadosParaImprimir + "-";
                dadosParaImprimir = dadosParaImprimir + ",";
                dadosParaImprimir = dadosParaImprimir + "-";
                dadosParaImprimir = dadosParaImprimir + ",";
            } else {
                dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidAcumuladosFinal[contadorP1][0]);
                dadosParaImprimir = dadosParaImprimir + ",";
                dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidAcumuladosFinal[contadorP1][2]);
                dadosParaImprimir = dadosParaImprimir + ",";
                dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidAcumuladosFinal[contadorP1][3]);
                dadosParaImprimir = dadosParaImprimir + ",";
                dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidAcumuladosFinal[contadorP1][4]);
                dadosParaImprimir = dadosParaImprimir + ",";
                dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidAcumuladosFinal[contadorP1][5]);
                dadosParaImprimir = dadosParaImprimir + ",";
            }


            if (contadorP2 > indiceIntervaloMes[3]) {
                dadosParaImprimir = dadosParaImprimir + "-";
                dadosParaImprimir = dadosParaImprimir + ",";
                dadosParaImprimir = dadosParaImprimir + "-";
                dadosParaImprimir = dadosParaImprimir + ",";
                dadosParaImprimir = dadosParaImprimir + "-";
                dadosParaImprimir = dadosParaImprimir + ",";
                dadosParaImprimir = dadosParaImprimir + "-";
                dadosParaImprimir = dadosParaImprimir + ",";
                dadosParaImprimir = dadosParaImprimir + "-";

            } else {

                dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidAcumuladosFinal[contadorP2][0]);
                dadosParaImprimir = dadosParaImprimir + ",";
                dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidAcumuladosFinal[contadorP2][2]);
                dadosParaImprimir = dadosParaImprimir + ",";
                dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidAcumuladosFinal[contadorP2][3]);
                dadosParaImprimir = dadosParaImprimir + ",";
                dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidAcumuladosFinal[contadorP2][4]);
                dadosParaImprimir = dadosParaImprimir + ",";
                dadosParaImprimir = dadosParaImprimir + (AppCovanalytics.dadosCovidAcumuladosFinal[contadorP2][5]);

            }
            dadosParaImprimir = dadosParaImprimir + " ";
            contadorP1++;
            contadorP2++;

        }

        int[] indiceIntervaloMedia1 = new int[2];
        indiceIntervaloMedia1[0] = indiceIntervaloMes[0];
        indiceIntervaloMedia1[1] = indiceIntervaloMes[1];
        int[] indiceIntervaloMedia2 = new int[2];
        indiceIntervaloMedia2[0] = indiceIntervaloMes[2];
        indiceIntervaloMedia2[1] = indiceIntervaloMes[3];


        dadosParaImprimir = dadosParaImprimir + "valoresMedios" + ",";
        dadosParaImprimir = dadosParaImprimir + (ImprimirRelatorios.calcularMediaIntervaloDatas(AppCovanalytics.dadosCovidAcumuladosFinal, indiceIntervaloMedia1, 2));
        dadosParaImprimir = dadosParaImprimir + ",";
        dadosParaImprimir = dadosParaImprimir + (ImprimirRelatorios.calcularMediaIntervaloDatas(AppCovanalytics.dadosCovidAcumuladosFinal, indiceIntervaloMedia1, 3));
        dadosParaImprimir = dadosParaImprimir + ",";
        dadosParaImprimir = dadosParaImprimir + (ImprimirRelatorios.calcularMediaIntervaloDatas(AppCovanalytics.dadosCovidAcumuladosFinal, indiceIntervaloMedia1, 4));
        dadosParaImprimir = dadosParaImprimir + ",";
        dadosParaImprimir = dadosParaImprimir + (ImprimirRelatorios.calcularMediaIntervaloDatas(AppCovanalytics.dadosCovidAcumuladosFinal, indiceIntervaloMedia1, 5));
        dadosParaImprimir = dadosParaImprimir + ",";

        dadosParaImprimir = dadosParaImprimir + "valoresMedios" + ",";
        dadosParaImprimir = dadosParaImprimir + (ImprimirRelatorios.calcularMediaIntervaloDatas(AppCovanalytics.dadosCovidAcumuladosFinal, indiceIntervaloMedia2, 2));
        dadosParaImprimir = dadosParaImprimir + ",";
        dadosParaImprimir = dadosParaImprimir + (ImprimirRelatorios.calcularMediaIntervaloDatas(AppCovanalytics.dadosCovidAcumuladosFinal, indiceIntervaloMedia2, 3));
        dadosParaImprimir = dadosParaImprimir + ",";
        dadosParaImprimir = dadosParaImprimir + (ImprimirRelatorios.calcularMediaIntervaloDatas(AppCovanalytics.dadosCovidAcumuladosFinal, indiceIntervaloMedia2, 4));
        dadosParaImprimir = dadosParaImprimir + ",";
        dadosParaImprimir = dadosParaImprimir + (ImprimirRelatorios.calcularMediaIntervaloDatas(AppCovanalytics.dadosCovidAcumuladosFinal, indiceIntervaloMedia2, 5));
        dadosParaImprimir = dadosParaImprimir + " ";


        dadosParaImprimir = dadosParaImprimir + "desvioPadrao" + ",";
        dadosParaImprimir = dadosParaImprimir + (ImprimirRelatorios.calcularDesvioPadraoIntervaloDatas(AppCovanalytics.dadosCovidAcumuladosFinal, indiceIntervaloMedia1, 2));
        dadosParaImprimir = dadosParaImprimir + ",";
        dadosParaImprimir = dadosParaImprimir + (ImprimirRelatorios.calcularDesvioPadraoIntervaloDatas(AppCovanalytics.dadosCovidAcumuladosFinal, indiceIntervaloMedia1, 3));
        dadosParaImprimir = dadosParaImprimir + ",";
        dadosParaImprimir = dadosParaImprimir + (ImprimirRelatorios.calcularDesvioPadraoIntervaloDatas(AppCovanalytics.dadosCovidAcumuladosFinal, indiceIntervaloMedia1, 4));
        dadosParaImprimir = dadosParaImprimir + ",";
        dadosParaImprimir = dadosParaImprimir + (ImprimirRelatorios.calcularDesvioPadraoIntervaloDatas(AppCovanalytics.dadosCovidAcumuladosFinal, indiceIntervaloMedia1, 5));
        dadosParaImprimir = dadosParaImprimir + ",";

        dadosParaImprimir = dadosParaImprimir + "desvioPadrao" + ",";
        dadosParaImprimir = dadosParaImprimir + (ImprimirRelatorios.calcularDesvioPadraoIntervaloDatas(AppCovanalytics.dadosCovidAcumuladosFinal, indiceIntervaloMedia2, 2));
        dadosParaImprimir = dadosParaImprimir + ",";
        dadosParaImprimir = dadosParaImprimir + (ImprimirRelatorios.calcularDesvioPadraoIntervaloDatas(AppCovanalytics.dadosCovidAcumuladosFinal, indiceIntervaloMedia2, 3));
        dadosParaImprimir = dadosParaImprimir + ",";
        dadosParaImprimir = dadosParaImprimir + (ImprimirRelatorios.calcularDesvioPadraoIntervaloDatas(AppCovanalytics.dadosCovidAcumuladosFinal, indiceIntervaloMedia2, 4));
        dadosParaImprimir = dadosParaImprimir + ",";
        dadosParaImprimir = dadosParaImprimir + (ImprimirRelatorios.calcularDesvioPadraoIntervaloDatas(AppCovanalytics.dadosCovidAcumuladosFinal, indiceIntervaloMedia2, 5));
        dadosParaImprimir = dadosParaImprimir + " ";
        dadosParaImprimir = dadosParaImprimir + " ";


        return dadosParaImprimir;

    }

    /*
     ********************************************************************************************
     ***** Método para gerar Relatório Previsional
     ********************************************************************************************
     */

    public static String gerarRelatorioPrevisional(String dadosParaImprimir) throws FileNotFoundException {

        if (AppCovanalytics.numeroVariaveis == 20) {
            String dataPrevisao = AppCovanalytics.parametrosUtilizador[15].substring(6, 10) + AppCovanalytics.parametrosUtilizador[15].substring(2, 6) + AppCovanalytics.parametrosUtilizador[15].substring(0, 2);
            int verificadorDataPrevisao, valorMenuMarkovDataPrevisao;
            LocalDate dataInicio;
            valorMenuMarkovDataPrevisao = LerFicheiroDados.conversorData(dataPrevisao);
            verificadorDataPrevisao = LerFicheiroDados.verificadorDatas(valorMenuMarkovDataPrevisao, AppCovanalytics.dadosCovidDiaFinal, AppCovanalytics.dadosCovidAcumuladosFinal, AbrirMenus.matrizDadosMenu);

            if (verificadorDataPrevisao == 0) {
                dataInicio = MetodoPrevisional.dataDisponivelMax;  //LocalDate.parse(dataPrevisao, DateTimeFormatter.ISO_LOCAL_DATE);
            } else {
                dataInicio = LocalDate.parse(dataPrevisao).minusDays(1);
            }

            dadosParaImprimir += "RelatorioPrevisional" + " ";
            dadosParaImprimir += "PrevisaoDeTransicaoEntreEstados" + " ";
            dadosParaImprimir += ExportarRelatorios.CABECALHOCADEIASDEMARKOV + " ";

            dadosParaImprimir += (MetodoPrevisional.calculoCadeiaDeMarkov(dataInicio, Integer.parseInt(dataPrevisao.replace("-", "")))[1][0]);
            dadosParaImprimir += ",";
            dadosParaImprimir += (MetodoPrevisional.calculoCadeiaDeMarkov(dataInicio, Integer.parseInt(dataPrevisao.replace("-", "")))[2][0]);
            dadosParaImprimir += ",";
            dadosParaImprimir += (MetodoPrevisional.calculoCadeiaDeMarkov(dataInicio, Integer.parseInt(dataPrevisao.replace("-", "")))[3][0]);
            dadosParaImprimir += ",";
            dadosParaImprimir += (MetodoPrevisional.calculoCadeiaDeMarkov(dataInicio, Integer.parseInt(dataPrevisao.replace("-", "")))[4][0]);
            dadosParaImprimir += " ";
            dadosParaImprimir += " ";

            dadosParaImprimir += "EvolucaoAteObito" + " ";
            dadosParaImprimir += ExportarRelatorios.CABECALHOFATORIZACAODECROUT + " ";

            dadosParaImprimir += (MetodoPrevisional.calculoFatorizacaoDeCrout()[0]);
            dadosParaImprimir += ",";
            dadosParaImprimir += (MetodoPrevisional.calculoFatorizacaoDeCrout()[1]);
            dadosParaImprimir += ",";
            dadosParaImprimir += (MetodoPrevisional.calculoFatorizacaoDeCrout()[2]);
            dadosParaImprimir += ",";
            dadosParaImprimir += (MetodoPrevisional.calculoFatorizacaoDeCrout()[3]);
            dadosParaImprimir += " ";
            dadosParaImprimir += " ";
        } else {
            String dataPrevisao = AppCovanalytics.parametrosUtilizador[1].substring(6, 10) + AppCovanalytics.parametrosUtilizador[1].substring(2, 6) + AppCovanalytics.parametrosUtilizador[1].substring(0, 2);
            int verificadorDataPrevisao, valorMenuMarkovDataPrevisao;
            LocalDate dataInicio;
            valorMenuMarkovDataPrevisao = LerFicheiroDados.conversorData(dataPrevisao);
            verificadorDataPrevisao = LerFicheiroDados.verificadorDatas(valorMenuMarkovDataPrevisao, AppCovanalytics.dadosCovidDiaFinal, AppCovanalytics.dadosCovidAcumuladosFinal, AbrirMenus.matrizDadosMenu);

            if (verificadorDataPrevisao == 0) {
                dataInicio = MetodoPrevisional.dataDisponivelMax;  //LocalDate.parse(dataPrevisao, DateTimeFormatter.ISO_LOCAL_DATE);
            } else {
                dataInicio = LocalDate.parse(dataPrevisao).minusDays(1);
            }


            dadosParaImprimir += "RelatorioPrevisional" + " ";
            dadosParaImprimir += "PrevisaoDeTransicaoEntreEstados" + " ";
            dadosParaImprimir += ExportarRelatorios.CABECALHOCADEIASDEMARKOV + " ";

            dadosParaImprimir += (MetodoPrevisional.calculoCadeiaDeMarkov(dataInicio, Integer.parseInt(dataPrevisao.replace("-", "")))[1][0]);
            dadosParaImprimir += ",";
            dadosParaImprimir += (MetodoPrevisional.calculoCadeiaDeMarkov(dataInicio, Integer.parseInt(dataPrevisao.replace("-", "")))[2][0]);
            dadosParaImprimir += ",";
            dadosParaImprimir += (MetodoPrevisional.calculoCadeiaDeMarkov(dataInicio, Integer.parseInt(dataPrevisao.replace("-", "")))[3][0]);
            dadosParaImprimir += ",";
            dadosParaImprimir += (MetodoPrevisional.calculoCadeiaDeMarkov(dataInicio, Integer.parseInt(dataPrevisao.replace("-", "")))[4][0]);
            dadosParaImprimir += " ";
            dadosParaImprimir += " ";

            dadosParaImprimir += "EvolucaoAteObito" + " ";
            dadosParaImprimir += ExportarRelatorios.CABECALHOFATORIZACAODECROUT + " ";

            dadosParaImprimir += (MetodoPrevisional.calculoFatorizacaoDeCrout()[0]);
            dadosParaImprimir += ",";
            dadosParaImprimir += (MetodoPrevisional.calculoFatorizacaoDeCrout()[1]);
            dadosParaImprimir += ",";
            dadosParaImprimir += (MetodoPrevisional.calculoFatorizacaoDeCrout()[2]);
            dadosParaImprimir += ",";
            dadosParaImprimir += (MetodoPrevisional.calculoFatorizacaoDeCrout()[3]);
            dadosParaImprimir += " ";
            dadosParaImprimir += " ";
        }

        return dadosParaImprimir;
    }



    /*
     ********************************************************************************************
     ***** Método para converter o formato de data DD-MM-AAAA em AAAAMMDD (tipo int)
     ********************************************************************************************
     */

    public static int converterFormatoDataAAAAMMDD(String data) {

        int dataConvertida;

        data = data.replace("-", "");
        String dataAux = data.substring(4, 8) + data.substring(2, 4) + data.substring(0, 2);
        dataConvertida = Integer.parseInt(dataAux);


        return dataConvertida;


    }

    /*
     ********************************************************************************************
     ***** Método para converter o formato de data DD-MM-AAAA em AAAAMM (tipo int)
     ********************************************************************************************
     */

    public static int converterFormatoDataAAAMM(String data) {

        int dataConvertida;

        data = data.replace("-", "");
        String dataAux = data.substring(4, 8) + data.substring(2, 4);
        dataConvertida = Integer.parseInt(dataAux);


        return dataConvertida;


    }


    /*
     ********************************************************************************************
     ***** Processo para validar a qualidade dos dados inseridos pelo utilizador no termina
     ***** Método valida se todos as posições esperadas retornam os valores expectaveis
     ***** Metodo quando o utilizar insere os 20 parametros
     ********************************************************************************************
     */

    public static boolean validarParametrosEntrada20() throws FileNotFoundException {


        int resolucaoTemporal = Integer.parseInt(AppCovanalytics.parametrosUtilizador[1]);

        if (AppCovanalytics.parametrosUtilizador[0].compareTo("-r") != 0 || resolucaoTemporal != 0 && resolucaoTemporal != 1 && resolucaoTemporal != 2 || AppCovanalytics.parametrosUtilizador[2].compareTo("-di") != 0 || !testarIntervaloDatas(converterFormatoDataAAAA_MM_DD(AppCovanalytics.parametrosUtilizador[3])) || AppCovanalytics.parametrosUtilizador[4].compareTo("-df") != 0 || !testarIntervaloDatas(converterFormatoDataAAAA_MM_DD(AppCovanalytics.parametrosUtilizador[5])) || AppCovanalytics.parametrosUtilizador[6].compareTo("-di1") != 0 || !testarIntervaloDatas(converterFormatoDataAAAA_MM_DD(AppCovanalytics.parametrosUtilizador[7])) || AppCovanalytics.parametrosUtilizador[8].compareTo("-df1") != 0 || !testarIntervaloDatas(converterFormatoDataAAAA_MM_DD(AppCovanalytics.parametrosUtilizador[9])) || AppCovanalytics.parametrosUtilizador[10].compareTo("-di2") != 0 || !testarIntervaloDatas(converterFormatoDataAAAA_MM_DD(AppCovanalytics.parametrosUtilizador[11])) || AppCovanalytics.parametrosUtilizador[12].compareTo("-df2") != 0 || !testarIntervaloDatas(converterFormatoDataAAAA_MM_DD(AppCovanalytics.parametrosUtilizador[13])) || AppCovanalytics.parametrosUtilizador[14].compareTo("-T") != 0 || !testarIntervaloDatas(converterFormatoDataAAAA_MM_DD(AppCovanalytics.parametrosUtilizador[15])) || !verificarExistenciaFicheiro(AppCovanalytics.parametrosUtilizador[16]) || !verificarExistenciaFicheiro(AppCovanalytics.parametrosUtilizador[17]) || !verificarExistenciaFicheiro(AppCovanalytics.parametrosUtilizador[18])) {
            return false;
        } else {
            return true;
        }

    }

    /*
     ********************************************************************************************
     ***** Processo para validar a qualidade dos dados inseridos pelo utilizador no termina
     ***** Método valida se todos as posições esperadas retornam os valores expectaveis
     ***** Metodo quando o utilizar insere os 16 parametros
     ********************************************************************************************
     */


    public static boolean validarParametrosEntrada16() throws FileNotFoundException {


        int resolucaoTemporal = Integer.parseInt(AppCovanalytics.parametrosUtilizador[1]);

        if (AppCovanalytics.parametrosUtilizador[0].compareTo("-r") != 0 || resolucaoTemporal != 0 && resolucaoTemporal != 1 && resolucaoTemporal != 2 || AppCovanalytics.parametrosUtilizador[2].compareTo("-di") != 0 || !testarIntervaloDatas(converterFormatoDataAAAA_MM_DD(AppCovanalytics.parametrosUtilizador[3])) || AppCovanalytics.parametrosUtilizador[4].compareTo("-df") != 0 || !testarIntervaloDatas(converterFormatoDataAAAA_MM_DD(AppCovanalytics.parametrosUtilizador[5])) || AppCovanalytics.parametrosUtilizador[6].compareTo("-di1") != 0 || !testarIntervaloDatas(converterFormatoDataAAAA_MM_DD(AppCovanalytics.parametrosUtilizador[7])) || AppCovanalytics.parametrosUtilizador[8].compareTo("-df1") != 0 || !testarIntervaloDatas(converterFormatoDataAAAA_MM_DD(AppCovanalytics.parametrosUtilizador[9])) || AppCovanalytics.parametrosUtilizador[10].compareTo("-di2") != 0 || !testarIntervaloDatas(converterFormatoDataAAAA_MM_DD(AppCovanalytics.parametrosUtilizador[11])) || AppCovanalytics.parametrosUtilizador[12].compareTo("-df2") != 0 || !testarIntervaloDatas(converterFormatoDataAAAA_MM_DD(AppCovanalytics.parametrosUtilizador[13])) || !verificarExistenciaFicheiro(AppCovanalytics.parametrosUtilizador[14])) {
            return false;
        } else {
            return true;
        }

    }


    /*
     ********************************************************************************************
     ***** Processo para validar a qualidade dos dados inseridos pelo utilizador no termina
     ***** Método valida se todos as posições esperadas retornam os valores expectaveis
     ***** Metodo quando o utilizar insere os 5 parametros
     ********************************************************************************************
     */


    public static boolean validarParametrosEntrada5() throws FileNotFoundException {


        if (AppCovanalytics.parametrosUtilizador[0].compareTo("-T") != 0 || !testarIntervaloDatas(converterFormatoDataAAAA_MM_DD(AppCovanalytics.parametrosUtilizador[1])) || !verificarExistenciaFicheiro(AppCovanalytics.parametrosUtilizador[2]) || !verificarExistenciaFicheiro(AppCovanalytics.parametrosUtilizador[3])) {
            return false;
        } else {
            return true;
        }

    }

    /*
     ********************************************************************************************
     ***** Método para validar se o ficheiro indicado pelo utilizador existe
     ********************************************************************************************
     */


    public static boolean verificarExistenciaFicheiro(String nomeFicheiro) {


        File ficheiro = new File(nomeFicheiro);

        if (ficheiro.exists()) {
            return true;
        } else {
            return false;
        }

    }


    /*
     ********************************************************************************************
     ***** Método para converter o formato de data inserido nos parametros pelo utilizador
     ********************************************************************************************
     */


    public static String converterFormatoDataAAAA_MM_DD(String data) {


        String dataConvertida = data.substring(6, 10) + "-" + data.substring(3, 5) + "-" + data.substring(0, 2);
        return dataConvertida;

    }


    /*
     ********************************************************************************************
     ***** Método para validar se as datas inseridas pelo utilizar estão dentro do intervalo admitido (Jan.2020 e Março 2022)
     ********************************************************************************************
     */


    public static boolean testarIntervaloDatas(String data) {

        LocalDate dataParaTestar = LocalDate.parse(data);
        LocalDate dataMin = LocalDate.parse("2020-01-01", DateTimeFormatter.ISO_LOCAL_DATE);
        LocalDate dataMax = LocalDate.parse("2022-03-31", DateTimeFormatter.ISO_LOCAL_DATE);

        long diffMin = Duration.between(dataMin.atStartOfDay(), dataParaTestar.atStartOfDay()).toDays();
        long diffMax = Duration.between(dataMax.atStartOfDay(), dataParaTestar.atStartOfDay()).toDays();


        if (diffMin >= 0 && diffMax <= 0) {
            return true;
        } else {
            return false;
        }

    }


    /*
     ********************************************************************************************
     ***** Método para validar se as datas inseridas pelo utilizar estão nas matrizes de dados (parametro 20)
     ********************************************************************************************
     */


    public static boolean validarDatasInseridasComDatasMatriz_parametros20() {


        //converter as data do 1 registo e do ultimo, das matrizes dia e acumulado para formato LocalDate

        String dataMinMatrizDia = Integer.toString(LerFicheiroDados.calcularLimitesDatasMatrizDados(AppCovanalytics.dadosCovidDiaFinal, AppCovanalytics.contaLinhasDadosDiaFinal)[0]);
        String dataMaxMatrizDia = Integer.toString(LerFicheiroDados.calcularLimitesDatasMatrizDados(AppCovanalytics.dadosCovidDiaFinal, AppCovanalytics.contaLinhasDadosDiaFinal)[1]);
        String dataMinMatrizAcum = Integer.toString(LerFicheiroDados.calcularLimitesDatasMatrizDados(AppCovanalytics.dadosCovidAcumuladosFinal, AppCovanalytics.contaLinhasDadosAcumuladosFinal)[0]);
        String dataMaxMatrizAcum = Integer.toString(LerFicheiroDados.calcularLimitesDatasMatrizDados(AppCovanalytics.dadosCovidAcumuladosFinal, AppCovanalytics.contaLinhasDadosAcumuladosFinal)[1]);
        String dataSeguinteDataMinMatrizDia = Integer.toString(AppCovanalytics.dadosCovidDiaFinal[1][0]);

        dataMinMatrizDia = dataMinMatrizDia.substring(0, 4) + "-" + dataMinMatrizDia.substring(4, 6) + "-" + dataMinMatrizDia.substring(6, 8);
        dataMaxMatrizDia = dataMaxMatrizDia.substring(0, 4) + "-" + dataMaxMatrizDia.substring(4, 6) + "-" + dataMaxMatrizDia.substring(6, 8);
        dataMinMatrizAcum = dataMinMatrizAcum.substring(0, 4) + "-" + dataMinMatrizAcum.substring(4, 6) + "-" + dataMinMatrizAcum.substring(6, 8);
        dataMaxMatrizAcum = dataMaxMatrizAcum.substring(0, 4) + "-" + dataMaxMatrizAcum.substring(4, 6) + "-" + dataMaxMatrizAcum.substring(6, 8);
        dataSeguinteDataMinMatrizDia = dataSeguinteDataMinMatrizDia.substring(0, 4) + "-" + dataSeguinteDataMinMatrizDia.substring(4, 6) + "-" + dataSeguinteDataMinMatrizDia.substring(6, 8);


        LocalDate dataMinMatrizDiaConv = LocalDate.parse(dataMinMatrizDia);
        LocalDate dataMaxMatrizDiaConv = LocalDate.parse(dataMaxMatrizDia);
        LocalDate dataMinMatrizAcumConv = LocalDate.parse(dataMinMatrizAcum);
        LocalDate dataMaxMatrizAcumConv = LocalDate.parse(dataMaxMatrizAcum);
        LocalDate dataSeguinteDataMinMatrizDiaConv = LocalDate.parse(dataSeguinteDataMinMatrizDia);


        LocalDate di = LocalDate.parse(converterFormatoDataAAAA_MM_DD(AppCovanalytics.parametrosUtilizador[3]));
        LocalDate df = LocalDate.parse(converterFormatoDataAAAA_MM_DD(AppCovanalytics.parametrosUtilizador[5]));
        LocalDate di1 = LocalDate.parse(converterFormatoDataAAAA_MM_DD(AppCovanalytics.parametrosUtilizador[7]));
        LocalDate df1 = LocalDate.parse(converterFormatoDataAAAA_MM_DD(AppCovanalytics.parametrosUtilizador[9]));
        LocalDate di2 = LocalDate.parse(converterFormatoDataAAAA_MM_DD(AppCovanalytics.parametrosUtilizador[11]));
        LocalDate df2 = LocalDate.parse(converterFormatoDataAAAA_MM_DD(AppCovanalytics.parametrosUtilizador[13]));
        LocalDate diaT = LocalDate.parse(converterFormatoDataAAAA_MM_DD(AppCovanalytics.parametrosUtilizador[15]));


        //validar os limites de datas aceitaveis com entrada de parametros=20


        long diadiffminDI = Duration.between(dataMinMatrizDiaConv.atStartOfDay(), di.atStartOfDay()).toDays();
        long diadiffMaxDI = Duration.between(dataMaxMatrizDiaConv.atStartOfDay(), di.atStartOfDay()).toDays();
        long diadiffminDF = Duration.between(dataMinMatrizDiaConv.atStartOfDay(), df.atStartOfDay()).toDays();
        long diadiffMaxDF = Duration.between(dataMaxMatrizDiaConv.atStartOfDay(), df.atStartOfDay()).toDays();
        long diadiffminDI1 = Duration.between(dataMinMatrizDiaConv.atStartOfDay(), di1.atStartOfDay()).toDays();
        long diadiffMaxDI1 = Duration.between(dataMaxMatrizDiaConv.atStartOfDay(), di1.atStartOfDay()).toDays();
        long diadiffminDF1 = Duration.between(dataMinMatrizDiaConv.atStartOfDay(), df1.atStartOfDay()).toDays();
        long diadiffMaxDF1 = Duration.between(dataMaxMatrizDiaConv.atStartOfDay(), df1.atStartOfDay()).toDays();
        long diadiffminDI2 = Duration.between(dataMinMatrizDiaConv.atStartOfDay(), di2.atStartOfDay()).toDays();
        long diadiffMaxDI2 = Duration.between(dataMaxMatrizDiaConv.atStartOfDay(), di2.atStartOfDay()).toDays();
        long diadiffminDF2 = Duration.between(dataMinMatrizDiaConv.atStartOfDay(), df2.atStartOfDay()).toDays();
        long diadiffMaxDF2 = Duration.between(dataMaxMatrizDiaConv.atStartOfDay(), df2.atStartOfDay()).toDays();
        long diaDiffMinT = Duration.between(dataSeguinteDataMinMatrizDiaConv.atStartOfDay(), diaT.atStartOfDay()).toDays();

        long AcumdiffminDI = Duration.between(dataMinMatrizAcumConv.atStartOfDay(), di.atStartOfDay()).toDays();
        long AcumdiffMaxDI = Duration.between(dataMaxMatrizAcumConv.atStartOfDay(), di.atStartOfDay()).toDays();
        long AcumdiffminDF = Duration.between(dataMinMatrizAcumConv.atStartOfDay(), df.atStartOfDay()).toDays();
        long AcumdiffMaxDF = Duration.between(dataMaxMatrizAcumConv.atStartOfDay(), df.atStartOfDay()).toDays();
        long AcumiffminDI1 = Duration.between(dataMinMatrizAcumConv.atStartOfDay(), di1.atStartOfDay()).toDays();
        long AcumdiffMaxDI1 = Duration.between(dataMaxMatrizAcumConv.atStartOfDay(), di1.atStartOfDay()).toDays();
        long AcumiffminDF1 = Duration.between(dataMinMatrizAcumConv.atStartOfDay(), df1.atStartOfDay()).toDays();
        long AcumiffMaxDF1 = Duration.between(dataMaxMatrizAcumConv.atStartOfDay(), df1.atStartOfDay()).toDays();
        long AcumdiffminDI2 = Duration.between(dataMinMatrizAcumConv.atStartOfDay(), di2.atStartOfDay()).toDays();
        long AcumdiffMaxDI2 = Duration.between(dataMaxMatrizAcumConv.atStartOfDay(), di2.atStartOfDay()).toDays();
        long AcumdiffminDF2 = Duration.between(dataMinMatrizAcumConv.atStartOfDay(), df2.atStartOfDay()).toDays();
        long AcumdiffMaxDF2 = Duration.between(dataMaxMatrizAcumConv.atStartOfDay(), df2.atStartOfDay()).toDays();


        if (diadiffminDI >= 0 && diadiffMaxDI <= 0 && diadiffminDF >= 0 && diadiffMaxDF <= 0 && diadiffminDI1 >= 0 && diadiffMaxDI1 <= 0 && diadiffminDF1 >= 0 && diadiffMaxDF1 <= 0 && diadiffminDI2 >= 0 && diadiffMaxDI2 <= 0 && diadiffminDF2 >= 0 && diadiffMaxDF2 <= 0 && diaDiffMinT >= 0 && AcumdiffminDI >= 0 && AcumdiffMaxDI <= 0 && AcumdiffminDF >= 0 && AcumdiffMaxDF <= 0 && AcumiffminDI1 >= 0 && AcumdiffMaxDI1 <= 0 && AcumiffminDF1 >= 0 && AcumiffMaxDF1 <= 0 && AcumdiffminDI2 >= 0 && AcumdiffMaxDI2 <= 0 && AcumdiffminDF2 >= 0 && AcumdiffMaxDF2 <= 0) {
            return true;
        } else {
            return false;
        }

    }


    /*
     ********************************************************************************************
     ***** Método para validar se as datas inseridas pelo utilizar estão nas matrizes de dados (parametro 16)
     ********************************************************************************************
     */

    public static boolean validarDatasInseridasComDatasMatriz_parametros16() {


        String dataMinMatrizAcum = Integer.toString(LerFicheiroDados.calcularLimitesDatasMatrizDados(AppCovanalytics.dadosCovidAcumuladosFinal, AppCovanalytics.contaLinhasDadosAcumuladosFinal)[0]);
        String dataMaxMatrizAcum = Integer.toString(LerFicheiroDados.calcularLimitesDatasMatrizDados(AppCovanalytics.dadosCovidAcumuladosFinal, AppCovanalytics.contaLinhasDadosAcumuladosFinal)[1]);
        dataMinMatrizAcum = dataMinMatrizAcum.substring(0, 4) + "-" + dataMinMatrizAcum.substring(4, 6) + "-" + dataMinMatrizAcum.substring(6, 8);
        dataMaxMatrizAcum = dataMaxMatrizAcum.substring(0, 4) + "-" + dataMaxMatrizAcum.substring(4, 6) + "-" + dataMaxMatrizAcum.substring(6, 8);
        LocalDate dataMinMatrizAcumConv = LocalDate.parse(dataMinMatrizAcum);
        LocalDate dataMaxMatrizAcumConv = LocalDate.parse(dataMaxMatrizAcum);

        LocalDate di = LocalDate.parse(converterFormatoDataAAAA_MM_DD(AppCovanalytics.parametrosUtilizador[3]));
        LocalDate df = LocalDate.parse(converterFormatoDataAAAA_MM_DD(AppCovanalytics.parametrosUtilizador[5]));
        LocalDate di1 = LocalDate.parse(converterFormatoDataAAAA_MM_DD(AppCovanalytics.parametrosUtilizador[7]));
        LocalDate df1 = LocalDate.parse(converterFormatoDataAAAA_MM_DD(AppCovanalytics.parametrosUtilizador[9]));
        LocalDate di2 = LocalDate.parse(converterFormatoDataAAAA_MM_DD(AppCovanalytics.parametrosUtilizador[11]));
        LocalDate df2 = LocalDate.parse(converterFormatoDataAAAA_MM_DD(AppCovanalytics.parametrosUtilizador[13]));


        long AcumdiffminDI = Duration.between(dataMinMatrizAcumConv.atStartOfDay(), di.atStartOfDay()).toDays();
        long AcumdiffMaxDI = Duration.between(dataMaxMatrizAcumConv.atStartOfDay(), di.atStartOfDay()).toDays();
        long AcumdiffminDF = Duration.between(dataMinMatrizAcumConv.atStartOfDay(), df.atStartOfDay()).toDays();
        long AcumdiffMaxDF = Duration.between(dataMaxMatrizAcumConv.atStartOfDay(), df.atStartOfDay()).toDays();
        long AcumiffminDI1 = Duration.between(dataMinMatrizAcumConv.atStartOfDay(), di1.atStartOfDay()).toDays();
        long AcumdiffMaxDI1 = Duration.between(dataMaxMatrizAcumConv.atStartOfDay(), di1.atStartOfDay()).toDays();
        long AcumiffminDF1 = Duration.between(dataMinMatrizAcumConv.atStartOfDay(), df1.atStartOfDay()).toDays();
        long AcumiffMaxDF1 = Duration.between(dataMaxMatrizAcumConv.atStartOfDay(), df1.atStartOfDay()).toDays();
        long AcumdiffminDI2 = Duration.between(dataMinMatrizAcumConv.atStartOfDay(), di2.atStartOfDay()).toDays();
        long AcumdiffMaxDI2 = Duration.between(dataMaxMatrizAcumConv.atStartOfDay(), di2.atStartOfDay()).toDays();
        long AcumdiffminDF2 = Duration.between(dataMinMatrizAcumConv.atStartOfDay(), df2.atStartOfDay()).toDays();
        long AcumdiffMaxDF2 = Duration.between(dataMaxMatrizAcumConv.atStartOfDay(), df2.atStartOfDay()).toDays();


        if (AcumdiffminDI >= 0 && AcumdiffMaxDI <= 0 && AcumdiffminDF >= 0 && AcumdiffMaxDF <= 0 && AcumiffminDI1 >= 0 && AcumdiffMaxDI1 <= 0 && AcumiffminDF1 >= 0 && AcumiffMaxDF1 <= 0 && AcumdiffminDI2 >= 0 && AcumdiffMaxDI2 <= 0 && AcumdiffminDF2 >= 0 && AcumdiffMaxDF2 <= 0) {
            return true;
        } else {
            return false;
        }
    }

    /*
     ********************************************************************************************
     ***** Método para validar se as datas inseridas pelo utilizar estão nas matrizes de dados (parametro 5)
     ********************************************************************************************
     */

    public static boolean validarDatasInseridasComDatasMatriz_parametros5() {

        String dataMinMatrizDia = Integer.toString(LerFicheiroDados.calcularLimitesDatasMatrizDados(AppCovanalytics.dadosCovidDiaFinal, AppCovanalytics.contaLinhasDadosDiaFinal)[0]);
        String dataMaxMatrizDia = Integer.toString(LerFicheiroDados.calcularLimitesDatasMatrizDados(AppCovanalytics.dadosCovidDiaFinal, AppCovanalytics.contaLinhasDadosDiaFinal)[1]);
        String dataSeguinteDataMinMatrizDia = Integer.toString(AppCovanalytics.dadosCovidDiaFinal[1][0]);

        dataMinMatrizDia = dataMinMatrizDia.substring(0, 4) + "-" + dataMinMatrizDia.substring(4, 6) + "-" + dataMinMatrizDia.substring(6, 8);
        dataMaxMatrizDia = dataMaxMatrizDia.substring(0, 4) + "-" + dataMaxMatrizDia.substring(4, 6) + "-" + dataMaxMatrizDia.substring(6, 8);
        dataSeguinteDataMinMatrizDia = dataSeguinteDataMinMatrizDia.substring(0, 4) + "-" + dataSeguinteDataMinMatrizDia.substring(4, 6) + "-" + dataSeguinteDataMinMatrizDia.substring(6, 8);


        LocalDate dataMinMatrizDiaConv = LocalDate.parse(dataMinMatrizDia);
        LocalDate dataMaxMatrizDiaConv = LocalDate.parse(dataMaxMatrizDia);
        LocalDate dataSeguinteDataMinMatrizDiaConv = LocalDate.parse(dataSeguinteDataMinMatrizDia);


        LocalDate diaT = LocalDate.parse(converterFormatoDataAAAA_MM_DD(AppCovanalytics.parametrosUtilizador[1]));


        //validar os limites de datas aceitaveis com entrada de parametros=20


        long diaDiffMinT = Duration.between(dataSeguinteDataMinMatrizDiaConv.atStartOfDay(), diaT.atStartOfDay()).toDays();

        if (diaDiffMinT >= 0) {
            return true;
        } else {
            return false;
        }

    }


    /*
     ********************************************************************************************
     ***** Método para imprimir a mensagem de erro quando as validações falham
     ********************************************************************************************
     */

    public static void imprimirMensagemErroMenuNaoInterativo() {

        System.out.println("\nOs parametros inseridos não estao de acordo com as instruçoes. Possiveis causas:");
        System.out.println("-o numero total de parametros não foi respeitado;");
        System.out.println("-os atributos (-r, di, df,...) nao se encontram nas respetivas posiçoes;");
        System.out.println("-as datas nao estao no formato desejado e/ou nao se encontram no intervalo de dados das matrizes;");
        System.out.println("-o nome dos ficheiros nao coincidem e/ou nao estao disponiveis na respetiva pasta.");
        System.out.println("Por favor tente de novo!\n");


    }
}