
public class ImprimirRelatorios {

    //********************************************************************************************
    //*****Metodo para imprimir o relatorio do numero casos dia***********************************
    //********************************************************************************************

    public static void gerarRelatorioNumeroCasos_Dia(int[][] matrizDadosMenu) {

        int tipoRelatorio = matrizDadosMenu[1][0];


        switch (tipoRelatorio) {

            case 1:
                System.out.println("**** RELATÓRIO SOBRE OS NÚMEROS TOTAIS DIÁRIOS DE COVID19 *****");
                System.out.print("**************************************************************\n\n");
                System.out.print("||  Data  || Número de infetados || Número de hospitalizados || Número de internados UCI || Número de mortes ||\n");
                System.out.println("_______________________________________________________________________________________________________________");

                int indiceLinha = CalcularIndiceRelatorios.calcularIndiceRelatorioNumeroCasos_DiaSemana(AppCovanalytics.dadosCovidDiaFinal, AbrirMenus.matrizDadosMenu[3][0]);

                System.out.printf("%10s", AppCovanalytics.dadosCovidDiaFinal[indiceLinha][0]);
                System.out.printf("%22s", AppCovanalytics.dadosCovidDiaFinal[indiceLinha][2]);
                System.out.printf("%28s", AppCovanalytics.dadosCovidDiaFinal[indiceLinha][3]);
                System.out.printf("%28s", AppCovanalytics.dadosCovidDiaFinal[indiceLinha][4]);
                System.out.printf("%20s", AppCovanalytics.dadosCovidDiaFinal[indiceLinha][5]);
                System.out.println();
                break;

            case 2:
                System.out.println("**** RELATÓRIO SOBRE OS NÚMEROS TOTAIS ACUMULADOS DE COVID19 *****");
                System.out.print("*****************************************************************\n\n");
                System.out.print("||  Data  || Número de infetados || Número de hospitalizados || Número de internados UCI || Número de mortes ||\n");
                System.out.println("_______________________________________________________________________________________________________________");

                indiceLinha = CalcularIndiceRelatorios.calcularIndiceRelatorioNumeroCasos_DiaSemana(AppCovanalytics.dadosCovidAcumuladosFinal, AbrirMenus.matrizDadosMenu[3][0]);

                System.out.printf("%10s", AppCovanalytics.dadosCovidAcumuladosFinal[indiceLinha][0]);
                System.out.printf("%22s", AppCovanalytics.dadosCovidAcumuladosFinal[indiceLinha][2]);
                System.out.printf("%28s", AppCovanalytics.dadosCovidAcumuladosFinal[indiceLinha][3]);
                System.out.printf("%28s", AppCovanalytics.dadosCovidAcumuladosFinal[indiceLinha][4]);
                System.out.printf("%20s", AppCovanalytics.dadosCovidAcumuladosFinal[indiceLinha][5]);
                System.out.println();
                break;

        }

    }

    //********************************************************************************************
    //*****Metodo para imprimir o relatorio do numero casos semana********************************
    //********************************************************************************************

    public static void gerarRelatorioNumeroCasos_Semana(int[][] matrizDadosMenu) {

        int tipoRelatorio = matrizDadosMenu[1][0];

        int duracaoSemana = 7;
        int contadorLinhas = 0;


        switch (tipoRelatorio) {
            case 1:
                System.out.println("**** RELATÓRIO SOBRE OS NÚMEROS TOTAIS DIÁRIOS DE COVID19 *****");
                System.out.print("**************************************************************\n\n");
                System.out.print("(Período semanal: de 2ªa feira a domingo)*********************\n\n");
                System.out.print("||  Data  || Número de infetados || Número de hospitalizados || Número de internados UCI || Número de mortes ||\n");
                System.out.println("_______________________________________________________________________________________________________________");

                int indiceLinha = CalcularIndiceRelatorios.calcularIndiceRelatorioNumeroCasos_DiaSemana(AppCovanalytics.dadosCovidDiaFinal, AbrirMenus.matrizDadosMenu[3][0]);
                duracaoSemana=duracaoSemana-calcularNumeroDiasDescontarSemana(AbrirMenus.matrizDadosMenu[3][0]);

                while (contadorLinhas < duracaoSemana && indiceLinha < AppCovanalytics.contaLinhasDadosDiaFinal) {
                    System.out.printf("%10s", AppCovanalytics.dadosCovidDiaFinal[indiceLinha][0]);
                    System.out.printf("%22s", AppCovanalytics.dadosCovidDiaFinal[indiceLinha][2]);
                    System.out.printf("%28s", AppCovanalytics.dadosCovidDiaFinal[indiceLinha][3]);
                    System.out.printf("%28s", AppCovanalytics.dadosCovidDiaFinal[indiceLinha][4]);
                    System.out.printf("%20s", AppCovanalytics.dadosCovidDiaFinal[indiceLinha][5]);
                    System.out.println();
                    indiceLinha++;
                    contadorLinhas++;
                }
                break;

            case 2:
                System.out.println("**** RELATÓRIO SOBRE OS NÚMEROS TOTAIS ACUMULADOS DE COVID19 *****");
                System.out.print("*****************************************************************\n\n");
                System.out.print("(Período semanal: de 2ªa feira a domingo)*************************\n\n");
                System.out.print("||  Data  || Número de infetados || Número de hospitalizados || Número de internados UCI || Número de mortes ||\n");
                System.out.println("_______________________________________________________________________________________________________________");

                indiceLinha = CalcularIndiceRelatorios.calcularIndiceRelatorioNumeroCasos_DiaSemana(AppCovanalytics.dadosCovidAcumuladosFinal, AbrirMenus.matrizDadosMenu[3][0]);
                duracaoSemana=duracaoSemana-calcularNumeroDiasDescontarSemana(AbrirMenus.matrizDadosMenu[3][0]);

                while (contadorLinhas < duracaoSemana && indiceLinha < AppCovanalytics.contaLinhasDadosAcumuladosFinal) {
                    System.out.printf("%10s", AppCovanalytics.dadosCovidAcumuladosFinal[indiceLinha][0]);
                    System.out.printf("%22s", AppCovanalytics.dadosCovidAcumuladosFinal[indiceLinha][2]);
                    System.out.printf("%28s", AppCovanalytics.dadosCovidAcumuladosFinal[indiceLinha][3]);
                    System.out.printf("%28s", AppCovanalytics.dadosCovidAcumuladosFinal[indiceLinha][4]);
                    System.out.printf("%20s", AppCovanalytics.dadosCovidAcumuladosFinal[indiceLinha][5]);
                    System.out.println();
                    indiceLinha++;
                    contadorLinhas++;
                }
                break;
        }

    }

    //********************************************************************************************
    //*****Metodo para imprimir o relatorio do numero casos num intervalo de dias*****************
    //********************************************************************************************

    public static void gerarRelatorioNumeroCasos_IntervaloDias(int[][] matrizDadosMenu) {

        int tipoRelatorio = matrizDadosMenu[1][0];


        switch (tipoRelatorio) {
            case 1:
                System.out.println("**** RELATÓRIO SOBRE OS NÚMEROS TOTAIS DIÁRIOS DE COVID19 *****");
                System.out.print("**************************************************************\n\n");
                System.out.print("||  Data  || Número de infetados || Número de hospitalizados || Número de internados UCI || Número de mortes ||\n");
                System.out.println("_______________________________________________________________________________________________________________");

                int[] indiceLinha = CalcularIndiceRelatorios.calcularIndiceRelatorioNumeroCasos_IntervaloDias(AppCovanalytics.dadosCovidDiaFinal, AppCovanalytics.contaLinhasDadosDiaFinal, AbrirMenus.matrizDadosMenu[3][0], AbrirMenus.matrizDadosMenu[3][1]);
                for (int linha = indiceLinha[0]; linha <= indiceLinha[1]; linha++) {
                    System.out.printf("%10s", AppCovanalytics.dadosCovidDiaFinal[linha][0]);
                    System.out.printf("%22s", AppCovanalytics.dadosCovidDiaFinal[linha][2]);
                    System.out.printf("%28s", AppCovanalytics.dadosCovidDiaFinal[linha][3]);
                    System.out.printf("%28s", AppCovanalytics.dadosCovidDiaFinal[linha][4]);
                    System.out.printf("%20s", AppCovanalytics.dadosCovidDiaFinal[linha][5]);
                    System.out.println();
                }
                System.out.printf("%10s", "\nTotais período");
                System.out.printf("%18s", calcularSomaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceLinha, 2));
                System.out.printf("%28s", calcularSomaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceLinha, 3));
                System.out.printf("%28s", calcularSomaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceLinha, 4));
                System.out.printf("%20s", calcularSomaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceLinha, 5));
                System.out.println();
                System.out.printf("%10s", "Média período");
                System.out.printf("%19.4f", calcularMediaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceLinha, 2));
                System.out.printf("%28.4f", calcularMediaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceLinha, 3));
                System.out.printf("%28.4f", calcularMediaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceLinha, 4));
                System.out.printf("%20.4f", calcularMediaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceLinha, 5));
                System.out.println();

                break;
            case 2:
                System.out.println("**** RELATÓRIO SOBRE OS NÚMEROS TOTAIS ACUMULADOS DE COVID19 *****");
                System.out.print("*****************************************************************\n\n");
                System.out.print("||  Data  || Número de infetados || Número de hospitalizados || Número de internados UCI || Número de mortes ||\n");
                System.out.println("_______________________________________________________________________________________________________________");

                indiceLinha = CalcularIndiceRelatorios.calcularIndiceRelatorioNumeroCasos_IntervaloDias(AppCovanalytics.dadosCovidAcumuladosFinal, AppCovanalytics.contaLinhasDadosAcumuladosFinal, AbrirMenus.matrizDadosMenu[3][0], AbrirMenus.matrizDadosMenu[3][1]);
                for (int linha = indiceLinha[0]; linha <= indiceLinha[1]; linha++) {
                    System.out.printf("%10s", AppCovanalytics.dadosCovidAcumuladosFinal[linha][0]);
                    System.out.printf("%22s", AppCovanalytics.dadosCovidAcumuladosFinal[linha][2]);
                    System.out.printf("%28s", AppCovanalytics.dadosCovidAcumuladosFinal[linha][3]);
                    System.out.printf("%28s", AppCovanalytics.dadosCovidAcumuladosFinal[linha][4]);
                    System.out.printf("%20s", AppCovanalytics.dadosCovidAcumuladosFinal[linha][5]);
                    System.out.println();
                }
                break;
        }

    }

    //********************************************************************************************
    //*****Metodo para imprimir o relatorio do numero casos agrupado mensalmente******************
    //********************************************************************************************

    public static void gerarRelatorioNumeroCasos_Mes(int[][] matrizDadosMenu) {

        int tipoRelatorio = matrizDadosMenu[1][0];

        switch (tipoRelatorio) {
            case 1:
                System.out.println("**** RELATÓRIO SOBRE OS NÚMEROS TOTAIS DIÁRIOS DE COVID19 *****");
                System.out.print("**************************************************************\n\n");
                System.out.print("||  Data  || Número de infetados || Número de hospitalizados || Número de internados UCI || Número de mortes ||\n");
                System.out.println("_______________________________________________________________________________________________________________");

                int[] indiceIntervaloMes = CalcularIndiceRelatorios.calcularIndiceRelatorioNumeroCasos_Mes(AppCovanalytics.dadosCovidDiaFinal, AbrirMenus.matrizDadosMenu[3][0]);

                System.out.printf("%10s", (AppCovanalytics.dadosCovidDiaFinal[indiceIntervaloMes[0]][0]) / 100);
                System.out.printf("%22s", calcularSomaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceIntervaloMes, 2));
                System.out.printf("%28s", calcularSomaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceIntervaloMes, 3));
                System.out.printf("%28s", calcularSomaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceIntervaloMes, 4));
                System.out.printf("%20s", calcularSomaIntervaloDatas(AppCovanalytics.dadosCovidDiaFinal, indiceIntervaloMes, 5));
                System.out.println();

                break;
            case 2:
                System.out.println("**** RELATÓRIO SOBRE OS NÚMEROS TOTAIS ACUMULADOS DE COVID19 *****");
                System.out.print("*****************************************************************\n\n");
                System.out.print("||  Data  || Número de infetados || Número de hospitalizados || Número de internados UCI || Número de mortes ||\n");
                System.out.println("_______________________________________________________________________________________________________________");

                indiceIntervaloMes = CalcularIndiceRelatorios.calcularIndiceRelatorioNumeroCasos_Mes(AppCovanalytics.dadosCovidAcumuladosFinal, AbrirMenus.matrizDadosMenu[3][0]);

                System.out.printf("%10s", (AppCovanalytics.dadosCovidAcumuladosFinal[indiceIntervaloMes[0]][0]) / 100);
                System.out.printf("%22s", AppCovanalytics.dadosCovidAcumuladosFinal[indiceIntervaloMes[1]][2]);
                System.out.printf("%28s", AppCovanalytics.dadosCovidAcumuladosFinal[indiceIntervaloMes[1]][3]);
                System.out.printf("%28s", AppCovanalytics.dadosCovidAcumuladosFinal[indiceIntervaloMes[1]][4]);
                System.out.printf("%20s", AppCovanalytics.dadosCovidAcumuladosFinal[indiceIntervaloMes[1]][5]);
                System.out.println();
                break;
        }

    }


    //********************************************************************************************
    //*****Metodo para imprimir o relatorio com comparação de períodos****************************
    //********************************************************************************************


    public static void gerarRelatorioComparativo_Dias(int[][] matrizDadosMenu) {

        int tipoRelatorio = matrizDadosMenu[1][0];

        switch (tipoRelatorio) {
            case 1:

                //Reporte sobre a comparação de numero de infetados diário

                System.out.println("******** RELATÓRIO COMPARATIVO SOBRE OS NÚMEROS TOTAIS DIÁRIOS DE COVID19 **********");
                System.out.print("***********************************************************************************\n");
                System.out.println("***1ºPeríodo definido pelo utilizador=P1 ; 2ºPeríodo definido pelo utilizador=P2***\n");
                System.out.print("||  Data.P1  ||   Número de infetados.P1   ||  Data.P2  ||   Número de infetados.P2   ||   [P2-P1]   ||\n");
                calcularDetalheReporteComparativo(matrizDadosMenu, AppCovanalytics.dadosCovidDiaFinal, AppCovanalytics.contaLinhasDadosDiaFinal);

                break;

            case 2:
                //Reporte sobre a comparação de numero de infetados acumulado

                System.out.println("******** RELATÓRIO COMPARATIVO SOBRE NÚMEROS TOTAIS ACUMULADOS DE COVID19 **********");
                System.out.print("***********************************************************************************\n");
                System.out.println("***1ºPeríodo definido pelo utilizador=P1 ; 2ºPeríodo definido pelo utilizador=P2***\n");
                System.out.print("||  Data.P1  ||   Número de infetados.P1   ||  Data.P2  ||   Número de infetados.P2   ||   [P2-P1]   ||\n");
                calcularDetalheReporteComparativo(matrizDadosMenu, AppCovanalytics.dadosCovidAcumuladosFinal, AppCovanalytics.contaLinhasDadosAcumuladosFinal);
                break;

        }

    }


    //********************************************************************************************
    //*****Metodo para calcular os valores medios de um intervalo de valores**********************
    //********************************************************************************************


    public static double calcularMediaIntervaloDatas(int[][] matrizDados, int[] indiceIntervaloDatas, int colunaDados) {

        int somatorioIntervaloDatas = 0;
        double mediaIntervaloDatas;

        for (int linha = indiceIntervaloDatas[0]; linha <= indiceIntervaloDatas[1]; linha++) {
            somatorioIntervaloDatas = somatorioIntervaloDatas + matrizDados[linha][colunaDados];
        }

        mediaIntervaloDatas = ((double)somatorioIntervaloDatas) / (indiceIntervaloDatas[1] - indiceIntervaloDatas[0] + 1);
        return mediaIntervaloDatas;
    }

    //********************************************************************************************
    //*****Metodo para calcular o desvio padrão de um intervalo de valores************************
    //********************************************************************************************

    public static double calcularDesvioPadraoIntervaloDatas(int[][] matrizDados, int[] indiceIntervaloDatas, int colunaDados) {

        int somaDifMedia;
        int somatorioDifMedia = 0;
        double desvioPadrao;

        for (int linha = indiceIntervaloDatas[0]; linha <= indiceIntervaloDatas[1]; linha++) {
            somaDifMedia = (int) Math.pow((matrizDados[linha][colunaDados] - calcularMediaIntervaloDatas(matrizDados, indiceIntervaloDatas, colunaDados)), 2);
            somatorioDifMedia = somatorioDifMedia + somaDifMedia;
        }

        desvioPadrao = Math.sqrt(((double)somatorioDifMedia) / (indiceIntervaloDatas[1] - indiceIntervaloDatas[0] + 1));
        return desvioPadrao;
    }


    //********************************************************************************************
    //*****Metodo para calcular a soma de dados  de um intervalo de datas*************************
    //********************************************************************************************

    public static int calcularSomaIntervaloDatas(int[][] matrizDados, int[] indiceIntervaloDatas, int colunaDados) {

        int somatorioIntervaloDatas = 0;

        for (int linha = indiceIntervaloDatas[0]; linha <= indiceIntervaloDatas[1]; linha++) {
            somatorioIntervaloDatas = somatorioIntervaloDatas + matrizDados[linha][colunaDados];
        }

        return somatorioIntervaloDatas;


    }

    //********************************************************************************************
    //*****Metodo para retornar o nome da matriz a ser testada (reporte periodos comparativo)*****
    //********************************************************************************************

    public static void calcularDetalheReporteComparativo(int[][] matrizDadosMenu, int[][] matrizDados, int contaLinhasMatriz) {


        int[] indiceIntervaloMes = CalcularIndiceRelatorios.calcularIndiceRelatorioComparativo_IntervaloDias(matrizDados, contaLinhasMatriz, matrizDadosMenu[3][0], matrizDadosMenu[3][1], matrizDadosMenu[3][2], matrizDadosMenu[3][3]);

        int contadorP1 = indiceIntervaloMes[0];
        int contadorP2 = indiceIntervaloMes[2];

        while (contadorP1 <= indiceIntervaloMes[1] || contadorP2 <= indiceIntervaloMes[3]) {
            if (contadorP1 > indiceIntervaloMes[1]) {
                System.out.printf("%12s", "-");
                System.out.printf("%28s", "-");
            } else {
                System.out.printf("%12s", matrizDados[contadorP1][0]);
                System.out.printf("%28s", matrizDados[contadorP1][2]);
            }
            System.out.print("   ||");
            if (contadorP2 > indiceIntervaloMes[3]) {
                System.out.printf("%10s", "-");
                System.out.printf("%28s", "-");
            } else {
                System.out.printf("%10s", matrizDados[contadorP2][0]);
                System.out.printf("%28s", matrizDados[contadorP2][2]);
            }
            System.out.print("   ||");
            if (contadorP1 <= indiceIntervaloMes[1] && contadorP2 <= indiceIntervaloMes[3]) {
                System.out.printf("%10s", matrizDados[contadorP2][2] - matrizDados[contadorP1][2]);
            } else {
                System.out.printf("%10s", "-");
            }
            System.out.print("   ||\n");
            contadorP1++;
            contadorP2++;
        }
        System.out.println();

        contadorP1 = indiceIntervaloMes[0];
        contadorP2 = indiceIntervaloMes[2];

        //metodo para calcular e imprimir as médias dos valores

        int[] indiceIntervaloMedia1 = new int[2];
        indiceIntervaloMedia1[0] = indiceIntervaloMes[0];
        indiceIntervaloMedia1[1] = indiceIntervaloMes[1];
        System.out.printf("%16s", "Valores médios");
        System.out.printf("%24.4f", calcularMediaIntervaloDatas(matrizDados, indiceIntervaloMedia1, 2));
        System.out.print("   ||");

        int[] indiceIntervaloMedia2 = new int[2];
        indiceIntervaloMedia2[0] = indiceIntervaloMes[2];
        indiceIntervaloMedia2[1] = indiceIntervaloMes[3];
        System.out.printf("%38.4f", calcularMediaIntervaloDatas(matrizDados, indiceIntervaloMedia2, 2));
        System.out.print("   ||\n");

        System.out.printf("%16s", "Desvio Padrão");
        System.out.printf("%24.4f", calcularDesvioPadraoIntervaloDatas(matrizDados, indiceIntervaloMedia1, 2));
        System.out.print("   ||");

        System.out.printf("%38.4f", calcularDesvioPadraoIntervaloDatas(matrizDados, indiceIntervaloMedia2, 2));
        System.out.print("   ||\n");

        //Reporte sobre a comparação de numero de hospitalizados diário

        System.out.print("\n\n***********************************************************************************\n");
        System.out.println("***1ºPeríodo definido pelo utilizador=P1 ; 2ºPeríodo definido pelo utilizador=P2***\n");
        System.out.print("||  Data.P1  ||   Número de hospitalizados.P1   ||  Data.P2  ||   Número de hospitalizados.P2   ||   [P2-P1]   ||\n");


        while (contadorP1 <= indiceIntervaloMes[1] || contadorP2 <= indiceIntervaloMes[3]) {
            if (contadorP1 > indiceIntervaloMes[1]) {
                System.out.printf("%12s", "-");
                System.out.printf("%33s", "-");
            } else {
                System.out.printf("%12s", matrizDados[contadorP1][0]);
                System.out.printf("%33s", matrizDados[contadorP1][3]);
            }
            System.out.print("   ||");
            if (contadorP2 > indiceIntervaloMes[3]) {
                System.out.printf("%10s", "-");
                System.out.printf("%33s", "-");
            } else {
                System.out.printf("%10s", matrizDados[contadorP2][0]);
                System.out.printf("%33s", matrizDados[contadorP2][3]);
            }
            System.out.print("   ||");
            if (contadorP1 <= indiceIntervaloMes[1] && contadorP2 <= indiceIntervaloMes[3]) {
                System.out.printf("%10s", matrizDados[contadorP2][3] - matrizDados[contadorP1][3]);
            } else {
                System.out.printf("%10s", "-");
            }
            System.out.print("   ||\n");
            contadorP1++;
            contadorP2++;
        }
        System.out.println();

        contadorP1 = indiceIntervaloMes[0];
        contadorP2 = indiceIntervaloMes[2];

        //metodo para calcular e imprimir as médias dos valores

        System.out.printf("%16s", "Valores médios");
        System.out.printf("%29.4f", calcularMediaIntervaloDatas(matrizDados, indiceIntervaloMedia1, 3));
        System.out.print("   ||");

        System.out.printf("%43.4f", calcularMediaIntervaloDatas(matrizDados, indiceIntervaloMedia2, 3));
        System.out.print("   ||\n");

        System.out.printf("%16s", "Desvio Padrão");
        System.out.printf("%29.4f", calcularDesvioPadraoIntervaloDatas(matrizDados, indiceIntervaloMedia1, 3));
        System.out.print("   ||");

        System.out.printf("%43.4f", calcularDesvioPadraoIntervaloDatas(matrizDados, indiceIntervaloMedia2, 3));
        System.out.print("   ||\n");


        //Reporte sobre a comparação de numero de internados UCI

        System.out.print("\n\n***********************************************************************************\n");
        System.out.println("***1ºPeríodo definido pelo utilizador=P1 ; 2ºPeríodo definido pelo utilizador=P2***\n");
        System.out.print("||  Data.P1  ||   Número internados UCI.P1   ||  Data.P2  ||   Número internados UCI.P2   ||   [P2-P1]   ||\n");


        while (contadorP1 <= indiceIntervaloMes[1] || contadorP2 <= indiceIntervaloMes[3]) {
            if (contadorP1 > indiceIntervaloMes[1]) {
                System.out.printf("%12s", "-");
                System.out.printf("%30s", "-");
            } else {
                System.out.printf("%12s", matrizDados[contadorP1][0]);
                System.out.printf("%30s", matrizDados[contadorP1][4]);
            }
            System.out.print("   ||");
            if (contadorP2 > indiceIntervaloMes[3]) {
                System.out.printf("%10s", "-");
                System.out.printf("%30s", "-");
            } else {
                System.out.printf("%10s", matrizDados[contadorP2][0]);
                System.out.printf("%30s", matrizDados[contadorP2][4]);
            }
            System.out.print("   ||");
            if (contadorP1 <= indiceIntervaloMes[1] && contadorP2 <= indiceIntervaloMes[3]) {
                System.out.printf("%10s", matrizDados[contadorP2][4] - matrizDados[contadorP1][4]);
            } else {
                System.out.printf("%10s", "-");
            }
            System.out.print("   ||\n");
            contadorP1++;
            contadorP2++;
        }
        System.out.println();

        contadorP1 = indiceIntervaloMes[0];
        contadorP2 = indiceIntervaloMes[2];

        //metodo para calcular e imprimir as médias dos valores

        System.out.printf("%16s", "Valores médios");
        System.out.printf("%26.4f", calcularMediaIntervaloDatas(matrizDados, indiceIntervaloMedia1, 4));
        System.out.print("   ||");

        System.out.printf("%40.4f", calcularMediaIntervaloDatas(matrizDados, indiceIntervaloMedia2, 4));
        System.out.print("   ||\n");

        System.out.printf("%16s", "Desvio Padrão");
        System.out.printf("%26.4f", calcularDesvioPadraoIntervaloDatas(matrizDados, indiceIntervaloMedia1, 4));
        System.out.print("   ||");

        System.out.printf("%40.4f", calcularDesvioPadraoIntervaloDatas(matrizDados, indiceIntervaloMedia2, 4));
        System.out.print("   ||\n");

        //Reporte sobre a comparação de numero de mortes

        System.out.print("\n\n***********************************************************************************\n");
        System.out.println("***1ºPeríodo definido pelo utilizador=P1 ; 2ºPeríodo definido pelo utilizador=P2***\n");
        System.out.print("||  Data.P1  ||   Número de mortes.P1   ||  Data.P2  ||   Número de mortes.P2   ||   [P2-P1]   ||\n");


        while (contadorP1 <= indiceIntervaloMes[1] || contadorP2 <= indiceIntervaloMes[3]) {
            if (contadorP1 > indiceIntervaloMes[1]) {
                System.out.printf("%12s", "-");
                System.out.printf("%25s", "-");
            } else {
                System.out.printf("%12s", matrizDados[contadorP1][0]);
                System.out.printf("%25s", matrizDados[contadorP1][5]);
            }
            System.out.print("   ||");
            if (contadorP2 > indiceIntervaloMes[3]) {
                System.out.printf("%10s", "-");
                System.out.printf("%25s", "-");
            } else {
                System.out.printf("%10s", matrizDados[contadorP2][0]);
                System.out.printf("%25s", matrizDados[contadorP2][5]);
            }
            System.out.print("   ||");
            if (contadorP1 <= indiceIntervaloMes[1] && contadorP2 <= indiceIntervaloMes[3]) {
                System.out.printf("%10s", matrizDados[contadorP2][5] - matrizDados[contadorP1][5]);
            } else {
                System.out.printf("%10s", "-");
            }
            System.out.print("   ||\n");
            contadorP1++;
            contadorP2++;
        }
        System.out.println();

        //metodo para calcular e imprimir as médias dos valores

        System.out.printf("%16s", "Valores médios");
        System.out.printf("%21.4f", calcularMediaIntervaloDatas(matrizDados, indiceIntervaloMedia1, 5));
        System.out.print("   ||");

        System.out.printf("%35.4f", calcularMediaIntervaloDatas(matrizDados, indiceIntervaloMedia2, 5));
        System.out.print("   ||\n");

        System.out.printf("%16s", "Desvio Padrão");
        System.out.printf("%21.4f", calcularDesvioPadraoIntervaloDatas(matrizDados, indiceIntervaloMedia1, 5));
        System.out.print("   ||");

        System.out.printf("%35.4f", calcularDesvioPadraoIntervaloDatas(matrizDados, indiceIntervaloMedia2, 5));
        System.out.print("   ||\n");

    }

    //********************************************************************************************
    //*****Metodo para retornar o numero de dias a considerar para os relatorios semanais*********
    //********************************************************************************************

    public static int calcularNumeroDiasDescontarSemana(int dataMatrizDados) {

        int numeroDiasDescontar = 0;

        switch (CalcularIndiceRelatorios.calcularDiadaSemana(dataMatrizDados)) {
            case MONDAY:
                break;
            case TUESDAY:
                numeroDiasDescontar = 1;
                break;
            case WEDNESDAY:
                numeroDiasDescontar = 2;
                break;
            case THURSDAY:
                numeroDiasDescontar = 3;
                break;
            case FRIDAY:
                numeroDiasDescontar = 4;
                break;
            case SATURDAY:
                numeroDiasDescontar = 5;
                break;
            case SUNDAY:
                numeroDiasDescontar = 6;
                break;

        }

        return numeroDiasDescontar;

    }

}