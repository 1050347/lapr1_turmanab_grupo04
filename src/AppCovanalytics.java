import java.io.FileNotFoundException;

public class AppCovanalytics {

    /*
     ********************************************************************************************
     ***** Variaveis para definir limite maximo e minimo das matrizes de dados
     ***** Variaveis com os contadores de controlo das matrizes
     ********************************************************************************************
     */
    //
    static final int NUMEROLINHAS = 1500;
    static final int NUMEROCOLUNAS = 6;

    static int contaLinhasDadosDiaAux = 0;
    static int contaLinhasDadosAcumuladosAux = 0;
    static int contaLinhasDadosDiaTemp = 0;
    static int contaLinhasDadosAcumuladosTemp = 0;
    static int contaLinhasDadosDiaFinal = 0;
    static int contaLinhasDadosAcumuladosFinal = 0;

    /*
     ********************************************************************************************
     ***** Variaveis para as matrizes
     ***** Temporarias (para carregar inicialmente os dados) e finais (apos validação)
     ***** Vetor para guardar os argumentos introduzidos pelo utilizador no terminal
     ********************************************************************************************
     */

    static int[][] dadosCovidDiaAux = new int[NUMEROLINHAS][NUMEROCOLUNAS];
    static int[][] dadosCovidAcumuladosAux = new int[NUMEROLINHAS][NUMEROCOLUNAS];
    static int[][] dadosCovidDiaTemp = new int[NUMEROLINHAS][NUMEROCOLUNAS];
    static int[][] dadosCovidAcumuladosTemp = new int[NUMEROLINHAS][NUMEROCOLUNAS];
    static int[][] dadosCovidDiaFinal = new int[NUMEROLINHAS][NUMEROCOLUNAS];
    static int[][] dadosCovidAcumuladosFinal = new int[NUMEROLINHAS][NUMEROCOLUNAS];
    static float[][] dadosMatrizTransicoes = new float[5][5];

    static String[] parametrosUtilizador = new String[50];
    static int numeroVariaveis = 0;


    public static void main(String[] args) throws FileNotFoundException {


        /*
         ********************************************************************************************
         ***** Classe para gerar os testes unitários
         ********************************************************************************************
         */

        //TestesUnitarios.executarImprimirResultadoTestesUnitarios();


        /*
         ********************************************************************************************
         ***** Processo para capturar e guardar os parametros introduzidos pelo utilizador
         ********************************************************************************************
         */

        for (int linha = 0; linha < args.length; linha++) {
            parametrosUtilizador[linha] = args[linha];
            numeroVariaveis++;
        }

        /*
         ********************************************************************************************
         ***** Processo para validar os dados inseridos pelo utilizador no terminal
         ***** Processo para definir de deve correr a instrução dos parametros ou abrir modo interativo
         ********************************************************************************************
         */

        if (numeroVariaveis == 0) {
            AbrirMenus.abrirMenuInicial();


        } else if (numeroVariaveis == 20 && ModoNaoInterativo.validarParametrosEntrada20()) {
            ModoNaoInterativo.carregarMatrizesDados();
            if (ModoNaoInterativo.validarDatasInseridasComDatasMatriz_parametros20()) {
                ModoNaoInterativo.gravarFicheiroSaida();
                System.out.println("\nParametros inseridos estão de acordo com as regras. Ficheiro exportado com sucesso!\n");
            } else {
                ModoNaoInterativo.imprimirMensagemErroMenuNaoInterativo();
            }


        } else if (numeroVariaveis == 16 && ModoNaoInterativo.validarParametrosEntrada16()) {
            ModoNaoInterativo.carregarMatrizesDados();
            if (ModoNaoInterativo.validarDatasInseridasComDatasMatriz_parametros16()) {
                ModoNaoInterativo.gravarFicheiroSaida();
                System.out.println("\nParametros inseridos estão de acordo com as regras. Ficheiro exportado com sucesso!\n");
            } else {
                ModoNaoInterativo.imprimirMensagemErroMenuNaoInterativo();
            }


        } else if (numeroVariaveis == 5 && ModoNaoInterativo.validarParametrosEntrada5()) {
            ModoNaoInterativo.carregarMatrizesDados();
            if (ModoNaoInterativo.validarDatasInseridasComDatasMatriz_parametros5()) {
                ModoNaoInterativo.gravarFicheiroSaida();
                System.out.println("\nParametros inseridos estão de acordo com as regras. Ficheiro exportado com sucesso!\n");
            } else {
                ModoNaoInterativo.imprimirMensagemErroMenuNaoInterativo();
            }

        } else {
            ModoNaoInterativo.imprimirMensagemErroMenuNaoInterativo();

        }

    }

}


