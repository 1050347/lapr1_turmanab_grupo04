
import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.util.Scanner;

public class AbrirMenus {

    static int[][] matrizDadosMenu = new int[4][4];
    static Scanner ler = new Scanner(System.in);
    static String nomeficheiro;

    //********************************************************************************************
    //*****Metodo para carregar o MENU INICIAL ***************************************************
    //********************************************************************************************

    public static void abrirMenuInicial() throws FileNotFoundException {

        limparConsola();

        System.out.println();
        System.out.println("==========================================");
        System.out.println("|             Dados Pandemia             |");
        System.out.println("|                                        |");
        System.out.println("| Que operação pretende realizar?        |");
        System.out.println("|                                        |");
        System.out.println("|                                        |");
        System.out.println("|    1 - Carregar novos dados            |");
        System.out.println("|                                        |");
        System.out.println("|                                        |");
        System.out.println("|    0 - Sair da aplicação               |");
        System.out.println("==========================================\n");
        System.out.println("Introduza a opção desejada\n");


        int valorMenuInical = ler.nextInt();
        while (valorMenuInical != 1 && valorMenuInical != 0) {
            System.out.println("Opção inválida, introduza novamente a opção pretendida");
            valorMenuInical = ler.nextInt();
        }

        matrizDadosMenu[0][0] = valorMenuInical;

        switch (valorMenuInical) {
            case 0:
                System.out.println();
                System.out.println("Obrigado pela sua visita!");
                System.exit(0);
            case 1:
                abrirMenu0_3();
                break;
        }
    }


    //********************************************************************************************
    //*****Metodo para carregar o MENU 0 ********************************************************
    //********************************************************************************************

    public static void abrirMenu0() throws FileNotFoundException {

        limparConsola();

        System.out.println();
        System.out.println("==========================================");
        System.out.println("|             Dados Pandemia             |");
        System.out.println("|                                        |");
        System.out.println("| Que operação pretende realizar?        |");
        System.out.println("|                                        |");
        System.out.println("|                                        |");
        System.out.println("|    1 - Consulta de relatorios          |");
        System.out.println("|    2 - Modelo previsional              |");
        System.out.println("|    3 - Carregar novos dados            |");
        System.out.println("|                                        |");
        System.out.println("|    0 - Sair da aplicação               |");
        System.out.println("==========================================\n");
        System.out.println("Introduza a opção desejada\n");


        int valorMenu0 = ler.nextInt();
        while (valorMenu0 != 1 && valorMenu0 != 2 && valorMenu0 != 3 && valorMenu0 != 0) {
            System.out.println("Opção inválida, introduza novamente a opção pretendida");
            valorMenu0 = ler.nextInt();
        }

        matrizDadosMenu[0][0] = valorMenu0;

        switch (valorMenu0) {
            case 0:
                System.out.println();
                System.out.println("Obrigado pela sua visita!");
                System.exit(0);
            case 1:
                abrirMenu0_1();
                break;
            case 2:
                AbrirMenus.menuPrevisional();
                break;
            case 3:
                abrirMenu0_3();
                break;
        }

    }

    //********************************************************************************************
    //*****Metodo para carregar o MENU 0.1 [Consulta de relatórios] ******************************
    //********************************************************************************************


    public static void abrirMenu0_1() throws FileNotFoundException {

        limparConsola();

        System.out.println();
        System.out.println("==========================================");
        System.out.println("|        Consulta de relatorios          |");
        System.out.println("|                                        |");
        System.out.println("| Que tipo de dados pretende visualizar? |");
        System.out.println("|                                        |");
        System.out.println("|                                        |");
        System.out.println("|    1 - Dados diários                   |");
        System.out.println("|    2 - Dados acumulados                |");
        System.out.println("|                                        |");
        System.out.println("|                                        |");
        System.out.println("|    0 - Regressar ao menu principal     |");
        System.out.println("==========================================\n");
        System.out.println("Introduza a opção desejada\n");


        int valorMenu0_1 = ler.nextInt();

        while (valorMenu0_1 != 1 && valorMenu0_1 != 2 && valorMenu0_1 != 0) {
            System.out.println("Opção inválida, introduza novamente a opção pretendida");
            valorMenu0_1 = ler.nextInt();
        }

        matrizDadosMenu[1][0] = valorMenu0_1;

        if (AppCovanalytics.contaLinhasDadosDiaFinal == 0 && matrizDadosMenu[1][0] == 1) {
            System.out.println();
            System.out.println("Não existem Dados Diários carregados para a memória.");
            System.out.println();
            abrirMenu0_1();
        }

        if (AppCovanalytics.contaLinhasDadosAcumuladosFinal == 0 && matrizDadosMenu[1][0] == 2) {
            System.out.println();
            System.out.println("Não existem Dados Acumulados carregados para a memória.");
            System.out.println();
            abrirMenu0_1();
        }


        switch (valorMenu0_1) {
            case 1:
            case 2:
                abrirMenu0_1_1();
                break;
            case 0:
                abrirMenu0();
                break;
        }

    }


    //********************************************************************************************
    //*****Metodo para carregar o MENU 0.1.1 [Consulta de relatórios - modo visualizacao] ********
    //********************************************************************************************


    public static void abrirMenu0_1_1() throws FileNotFoundException {

        limparConsola();

        System.out.println();
        System.out.println("===============================================");
        System.out.println("|     Modo de Visualização dos relatorios     |");
        System.out.println("|                                             |");
        System.out.println("| Como pretende visualizar o(s) relatório(s)? |");
        System.out.println("|                                             |");
        System.out.println("|     1 - Por Intervalo de Datas              |");
        System.out.println("|     2 - Análise Diária                      |");
        System.out.println("|     3 - Análise Semanal                     |");
        System.out.println("|     4 - Análise Mensal                      |");
        System.out.println("|     5 - Comparação de Periodos              |");
        System.out.println("|                                             |");
        System.out.println("|     0 - Regressar ao Menu Principal         |");
        System.out.println("===============================================\n");

        System.out.println("Introduza a opção desejada\n");

        int valorMenu0_1_1 = ler.nextInt();

        while (valorMenu0_1_1 != 1 && valorMenu0_1_1 != 2 && valorMenu0_1_1 != 3 && valorMenu0_1_1 != 4 && valorMenu0_1_1 != 5 && valorMenu0_1_1 != 0) {
            System.out.println("Opção inválida, introduza novamente a opção pretendida");
            valorMenu0_1_1 = ler.nextInt();
        }

        matrizDadosMenu[2][0] = valorMenu0_1_1;

        switch (valorMenu0_1_1) {
            case 1:
                abrirMenu0_1_1_1();
                break;
            case 2:
                abrirMenu0_1_1_2();
                break;
            case 3:
                abrirMenu0_1_1_3();
                break;
            case 4:
                abrirMenu0_1_1_4();
                break;
            case 5:
                abrirMenu0_1_1_5();
                break;
            case 0:
                abrirMenu0();
                break;
        }
    }


    //****************************************************************************************************************
    //*****Metodo para carregar o MENU 0.1.1.1 [Consulta de relatórios - modo visualizacao - intervalo datas] ********
    //****************************************************************************************************************

    public static void abrirMenu0_1_1_1() throws FileNotFoundException {


        limparConsola();

        System.out.println();
        System.out.println("======================================");
        System.out.println("|        Intervalo de Datas          |");
        System.out.println("|                                    |");
        System.out.println("|    1 - Definição de datas          |");
        System.out.println("|                                    |");
        System.out.println("|                                    |");
        System.out.println("|    0 - Regressar ao menu principal |");
        System.out.println("======================================\n");
        System.out.println("Introduza a opção desejada\n");


        int verificadorDataInicial, verificadorDataFinal, valorMenu0_1_1_1DataInicial, valorMenu0_1_1_1DataFinal;

        int valorMenu0_1_1_1 = ler.nextInt();

        while (valorMenu0_1_1_1 != 1 && valorMenu0_1_1_1 != 0) {
            System.out.println("Opção inválida, introduza novamente a opção pretendida");
            valorMenu0_1_1_1 = ler.nextInt();
        }

        switch (valorMenu0_1_1_1) {
            case 1:

                System.out.println("Periodo de dados carregados disponiveis para consulta:");
                if (matrizDadosMenu[1][0] == 1) {
                    LocalDate dataDisponivelMin = LerFicheiroDados.converterData(LerFicheiroDados.calcularLimitesDatasMatrizDados(AppCovanalytics.dadosCovidDiaFinal, AppCovanalytics.contaLinhasDadosDiaFinal)[0]);
                    LocalDate dataDisponivelMax = LerFicheiroDados.converterData(LerFicheiroDados.calcularLimitesDatasMatrizDados(AppCovanalytics.dadosCovidDiaFinal, AppCovanalytics.contaLinhasDadosDiaFinal)[1]);
                    System.out.println("Desde: " + dataDisponivelMin + " Até: " + dataDisponivelMax);
                    System.out.println();
                }

                if (matrizDadosMenu[1][0] == 2) {
                    LocalDate dataDisponivelMin = LerFicheiroDados.converterData(LerFicheiroDados.calcularLimitesDatasMatrizDados(AppCovanalytics.dadosCovidAcumuladosFinal, AppCovanalytics.contaLinhasDadosAcumuladosFinal)[0]);
                    LocalDate dataDisponivelMax = LerFicheiroDados.converterData(LerFicheiroDados.calcularLimitesDatasMatrizDados(AppCovanalytics.dadosCovidAcumuladosFinal, AppCovanalytics.contaLinhasDadosAcumuladosFinal)[1]);
                    System.out.println("Desde: " + dataDisponivelMin + " Até: " + dataDisponivelMax);
                    System.out.println();
                }

                System.out.println("Definição do periodo de análise:");

                do {
                    System.out.println("Introduza a data inicial (AAAA-MM-DD):\n");

                    Scanner ler = new Scanner(System.in);

                    String dataInicialString = ler.nextLine();

                    valorMenu0_1_1_1DataInicial = LerFicheiroDados.conversorData(dataInicialString);

                    verificadorDataInicial = LerFicheiroDados.verificadorDatas(valorMenu0_1_1_1DataInicial, AppCovanalytics.dadosCovidDiaFinal, AppCovanalytics.dadosCovidAcumuladosFinal, matrizDadosMenu);

                    while (verificadorDataInicial == 0) {
                        System.out.println();
                        System.out.println("Data inicial inválida ou sem dados.");
                        System.out.println();
                        System.out.println("Volte a inserir a data inicial:");

                        dataInicialString = ler.nextLine();

                        valorMenu0_1_1_1DataInicial = LerFicheiroDados.conversorData(dataInicialString);

                        verificadorDataInicial = LerFicheiroDados.verificadorDatas(valorMenu0_1_1_1DataInicial, AppCovanalytics.dadosCovidDiaFinal, AppCovanalytics.dadosCovidAcumuladosFinal, matrizDadosMenu);
                    }


                    System.out.println();
                    System.out.println("Introduza a data final (AAAA-MM-DD):");

                    String dataFinalString = ler.nextLine();

                    valorMenu0_1_1_1DataFinal = LerFicheiroDados.conversorData(dataFinalString);

                    verificadorDataFinal = LerFicheiroDados.verificadorDatas(valorMenu0_1_1_1DataFinal, AppCovanalytics.dadosCovidDiaFinal, AppCovanalytics.dadosCovidAcumuladosFinal, matrizDadosMenu);

                    while (verificadorDataFinal == 0) {
                        System.out.println();
                        System.out.println("Data final inválida ou sem dados.");
                        System.out.println();
                        System.out.println("Volte a inserir a data final:");

                        dataFinalString = ler.nextLine();

                        valorMenu0_1_1_1DataFinal = LerFicheiroDados.conversorData(dataFinalString);

                        verificadorDataFinal = LerFicheiroDados.verificadorDatas(valorMenu0_1_1_1DataFinal, AppCovanalytics.dadosCovidDiaFinal, AppCovanalytics.dadosCovidAcumuladosFinal, matrizDadosMenu);
                    }

                    if (valorMenu0_1_1_1DataInicial > valorMenu0_1_1_1DataFinal) {
                        System.out.println();
                        System.out.println("Data inicial introduzida é superior à data final!");
                        System.out.println();
                        System.out.println("Introduzir novas datas:");
                        System.out.println();
                    }

                } while (valorMenu0_1_1_1DataInicial > valorMenu0_1_1_1DataFinal);


                matrizDadosMenu[3][0] = valorMenu0_1_1_1DataInicial;
                matrizDadosMenu[3][1] = valorMenu0_1_1_1DataFinal;

                limparConsola();

                ImprimirRelatorios.gerarRelatorioNumeroCasos_IntervaloDias(matrizDadosMenu);

                break;
            case 0:
                abrirMenu0();
        }

        System.out.println();
        System.out.println("======================================");
        System.out.println("|    9 - Exportar o relatório        |");
        System.out.println("|                                    |");
        System.out.println("|    0 - Regressar ao menu principal |");
        System.out.println("======================================\n");

        valorMenu0_1_1_1 = ler.nextInt();
        while (valorMenu0_1_1_1 != 9 && valorMenu0_1_1_1 != 0) {
            System.out.println("Opção inválida, introduza novamente a opção pretendida");
            valorMenu0_1_1_1 = ler.nextInt();
        }

        switch (valorMenu0_1_1_1) {
            case 0:
                abrirMenu0();
                break;
            case 9:
                ExportarRelatorios.exportarRelatorioNumeroCasos_IntervaloDias(matrizDadosMenu);
                System.out.println("Ficheiro gerado com sucesso!");
                abrirMenu0();
                break;
        }

    }


    //****************************************************************************************************************
    //*****Metodo para carregar o Menu 0.1.1.2 [Consulta de relatórios - modo visualizacao - diaria] *****************
    //****************************************************************************************************************


    public static void abrirMenu0_1_1_2() throws FileNotFoundException {

        limparConsola();

        System.out.println();
        System.out.println("======================================");
        System.out.println("|          Análise Diária            |");
        System.out.println("|                                    |");
        System.out.println("|    1 - Definição de datas          |");
        System.out.println("|                                    |");
        System.out.println("|                                    |");
        System.out.println("|    0 - Regressar ao menu principal |");
        System.out.println("======================================\n");

        System.out.println("Introduza a opção desejada\n");

        int valorMenu0_1_1_2 = ler.nextInt();

        while (valorMenu0_1_1_2 != 1 && valorMenu0_1_1_2 != 0) {
            System.out.println("Opção inválida, introduza novamente a opção pretendida");
            valorMenu0_1_1_2 = ler.nextInt();
        }

        switch (valorMenu0_1_1_2) {

            case 1:
                Scanner ler = new Scanner(System.in);

                int valorMenu0_1_1_2DiaInicial, verificadorDiaInicial;


                System.out.println("Periodo de dados carregados disponiveis para consulta:");
                if (matrizDadosMenu[1][0] == 1) {
                    LocalDate dataDisponivelMin = LerFicheiroDados.converterData(LerFicheiroDados.calcularLimitesDatasMatrizDados(AppCovanalytics.dadosCovidDiaFinal, AppCovanalytics.contaLinhasDadosDiaFinal)[0]);
                    LocalDate dataDisponivelMax = LerFicheiroDados.converterData(LerFicheiroDados.calcularLimitesDatasMatrizDados(AppCovanalytics.dadosCovidDiaFinal, AppCovanalytics.contaLinhasDadosDiaFinal)[1]);
                    System.out.println("Desde: " + dataDisponivelMin + " Até: " + dataDisponivelMax);
                    System.out.println();
                }

                if (matrizDadosMenu[1][0] == 2) {
                    LocalDate dataDisponivelMin = LerFicheiroDados.converterData(LerFicheiroDados.calcularLimitesDatasMatrizDados(AppCovanalytics.dadosCovidAcumuladosFinal, AppCovanalytics.contaLinhasDadosAcumuladosFinal)[0]);
                    LocalDate dataDisponivelMax = LerFicheiroDados.converterData(LerFicheiroDados.calcularLimitesDatasMatrizDados(AppCovanalytics.dadosCovidAcumuladosFinal, AppCovanalytics.contaLinhasDadosAcumuladosFinal)[1]);
                    System.out.println("Desde: " + dataDisponivelMin + " Até: " + dataDisponivelMax);
                    System.out.println();
                }

                do {
                    System.out.println("Introduza o dia para análise: (AAAA-MM-DD):\n");

                    String dataInicialString = ler.nextLine();

                    valorMenu0_1_1_2DiaInicial = LerFicheiroDados.conversorData(dataInicialString);

                    verificadorDiaInicial = LerFicheiroDados.verificadorDatas(valorMenu0_1_1_2DiaInicial, AppCovanalytics.dadosCovidDiaFinal, AppCovanalytics.dadosCovidAcumuladosFinal, matrizDadosMenu);

                    if (verificadorDiaInicial == 0) {
                        System.out.println();
                        System.out.println("Data inválida ou sem dados.");
                        System.out.println();
                        System.out.println("Volte a inserir a data:");
                        System.out.println();
                    }

                } while (verificadorDiaInicial == 0);

                matrizDadosMenu[3][0] = valorMenu0_1_1_2DiaInicial;

                limparConsola();
                ImprimirRelatorios.gerarRelatorioNumeroCasos_Dia(matrizDadosMenu);

                break;

            case 0:
                abrirMenu0();
        }
        System.out.println();
        System.out.println("======================================");
        System.out.println("|    9 - Exportar o relatório        |");
        System.out.println("|                                    |");
        System.out.println("|    0 - Regressar ao menu principal |");
        System.out.println("======================================\n");

        valorMenu0_1_1_2 = ler.nextInt();
        while (valorMenu0_1_1_2 != 9 && valorMenu0_1_1_2 != 0) {
            System.out.println("Opção inválida, introduza novamente a opção pretendida");
            valorMenu0_1_1_2 = ler.nextInt();
        }

        switch (valorMenu0_1_1_2) {
            case 0:
                abrirMenu0();
                break;
            case 9:
                ExportarRelatorios.exportarRelatorioNumeroCasos_Dia(matrizDadosMenu);
                System.out.println("Ficheiro gerado com sucesso!");
                abrirMenu0();
                break;
        }
    }

    //************************************************************************************************************
    //*****Metodo para carregar o Menu 0.1.1.3 [Consulta de relatórios - modo visualizacao - semanal] ************
    //************************************************************************************************************


    public static void abrirMenu0_1_1_3() throws FileNotFoundException {

        limparConsola();

        System.out.println();
        System.out.println("======================================");
        System.out.println("|        Análise Semanal             |");
        System.out.println("|                                    |");
        System.out.println("|    1 - Definição de datas          |");
        System.out.println("|                                    |");
        System.out.println("|                                    |");
        System.out.println("|    0 - Regressar ao menu principal |");
        System.out.println("======================================\n");

        System.out.println("Introduza a opção desejada\n");

        int valorMenu0_1_1_3 = ler.nextInt();

        while (valorMenu0_1_1_3 != 1 && valorMenu0_1_1_3 != 0) {
            System.out.println("Opção inválida, introduza novamente a opção pretendida");
            valorMenu0_1_1_3 = ler.nextInt();
        }

        switch (valorMenu0_1_1_3) {
            case 1:
                Scanner ler = new Scanner(System.in);

                int valorMenu0_1_1_3DiaInicial, verificadorDiaInicial;


                System.out.println("Periodo de dados carregados disponiveis para consulta:");
                if (matrizDadosMenu[1][0] == 1) {
                    LocalDate dataDisponivelMin = LerFicheiroDados.converterData(LerFicheiroDados.calcularLimitesDatasMatrizDados(AppCovanalytics.dadosCovidDiaFinal, AppCovanalytics.contaLinhasDadosDiaFinal)[0]);
                    LocalDate dataDisponivelMax = LerFicheiroDados.converterData(LerFicheiroDados.calcularLimitesDatasMatrizDados(AppCovanalytics.dadosCovidDiaFinal, AppCovanalytics.contaLinhasDadosDiaFinal)[1]);
                    System.out.println("Desde: " + dataDisponivelMin + " Até: " + dataDisponivelMax);
                    System.out.println();
                }

                if (matrizDadosMenu[1][0] == 2) {
                    LocalDate dataDisponivelMin = LerFicheiroDados.converterData(LerFicheiroDados.calcularLimitesDatasMatrizDados(AppCovanalytics.dadosCovidAcumuladosFinal, AppCovanalytics.contaLinhasDadosAcumuladosFinal)[0]);
                    LocalDate dataDisponivelMax = LerFicheiroDados.converterData(LerFicheiroDados.calcularLimitesDatasMatrizDados(AppCovanalytics.dadosCovidAcumuladosFinal, AppCovanalytics.contaLinhasDadosAcumuladosFinal)[1]);
                    System.out.println("Desde: " + dataDisponivelMin + " Até: " + dataDisponivelMax);
                    System.out.println();
                }

                do {
                    System.out.println("Introduza o dia inicial (AAAA-MM-DD):\n");

                    String dataInicialString = ler.nextLine();

                    valorMenu0_1_1_3DiaInicial = LerFicheiroDados.conversorData(dataInicialString);

                    verificadorDiaInicial = LerFicheiroDados.verificadorDatas(valorMenu0_1_1_3DiaInicial, AppCovanalytics.dadosCovidDiaFinal, AppCovanalytics.dadosCovidAcumuladosFinal, matrizDadosMenu);

                    if (verificadorDiaInicial == 0) {
                        System.out.println();
                        System.out.println("Data inválida ou sem dados.");
                        System.out.println();
                        System.out.println("Volte a inserir a data:");
                        System.out.println();
                    }

                } while (verificadorDiaInicial == 0);

                matrizDadosMenu[3][0] = valorMenu0_1_1_3DiaInicial;

                limparConsola();
                ImprimirRelatorios.gerarRelatorioNumeroCasos_Semana(matrizDadosMenu);

                break;
            case 0:
                abrirMenu0();
                break;
        }
        System.out.println();
        System.out.println("======================================");
        System.out.println("|    9 - Exportar o relatório        |");
        System.out.println("|                                    |");
        System.out.println("|    0 - Regressar ao menu principal |");
        System.out.println("======================================\n");

        valorMenu0_1_1_3 = ler.nextInt();
        while (valorMenu0_1_1_3 != 9 && valorMenu0_1_1_3 != 0) {
            System.out.println("Opção inválida, introduza novamente a opção pretendida");
            valorMenu0_1_1_3 = ler.nextInt();
        }

        switch (valorMenu0_1_1_3) {
            case 0:
                abrirMenu0();
                break;
            case 9:
                ExportarRelatorios.exportarRelatorioNumeroCasos_Semana(matrizDadosMenu);
                System.out.println("Ficheiro gerado com sucesso!");
                abrirMenu0();
                break;
        }
    }


    //****************************************************************************************************************
    //*****Metodo para carregar o Menu 0.1.1.4 [Consulta de relatórios - modo visualizacao - mensal] *****************
    //****************************************************************************************************************

    public static void abrirMenu0_1_1_4() throws FileNotFoundException {

        limparConsola();

        System.out.println();
        System.out.println("======================================");
        System.out.println("|            Análise Mensal          |");
        System.out.println("|                                    |");
        System.out.println("|    1 - Definição de datas          |");
        System.out.println("|                                    |");
        System.out.println("|                                    |");
        System.out.println("|    0 - Regressar ao menu principal |");
        System.out.println("======================================\n");

        System.out.println("Introduza a opção desejada\n");

        int valorMenu0_1_1_4 = ler.nextInt();

        while (valorMenu0_1_1_4 != 1 && valorMenu0_1_1_4 != 0) {
            System.out.println("Opção inválida, introduza novamente a opção pretendida");
            valorMenu0_1_1_4 = ler.nextInt();
        }

        switch (valorMenu0_1_1_4) {
            case 1:

                int[] matrizMaxMin = LerFicheiroDados.verificadorMaxMin(AppCovanalytics.dadosCovidDiaFinal, AppCovanalytics.dadosCovidAcumuladosFinal, matrizDadosMenu);

                int maxAno = matrizMaxMin[0];
                int minAno = matrizMaxMin[1];
                int maxMes = matrizMaxMin[2];
                int minMes = matrizMaxMin[3];

                int contadorCaracteres = 0;
                int valorMenu0_1_1_4AnoMes;
                int verificador = 0;

                System.out.println("Periodo de dados carregados disponiveis para consulta:");
                if (matrizDadosMenu[1][0] == 1) {
                    LocalDate dataDisponivelMin = LerFicheiroDados.converterData(LerFicheiroDados.calcularLimitesDatasMatrizDados(AppCovanalytics.dadosCovidDiaFinal, AppCovanalytics.contaLinhasDadosDiaFinal)[0]);
                    LocalDate dataDisponivelMax = LerFicheiroDados.converterData(LerFicheiroDados.calcularLimitesDatasMatrizDados(AppCovanalytics.dadosCovidDiaFinal, AppCovanalytics.contaLinhasDadosDiaFinal)[1]);
                    System.out.println("Desde: " + dataDisponivelMin + " Até: " + dataDisponivelMax);
                    System.out.println();
                }

                if (matrizDadosMenu[1][0] == 2) {
                    LocalDate dataDisponivelMin = LerFicheiroDados.converterData(LerFicheiroDados.calcularLimitesDatasMatrizDados(AppCovanalytics.dadosCovidAcumuladosFinal, AppCovanalytics.contaLinhasDadosAcumuladosFinal)[0]);
                    LocalDate dataDisponivelMax = LerFicheiroDados.converterData(LerFicheiroDados.calcularLimitesDatasMatrizDados(AppCovanalytics.dadosCovidAcumuladosFinal, AppCovanalytics.contaLinhasDadosAcumuladosFinal)[1]);
                    System.out.println("Desde: " + dataDisponivelMin + " Até: " + dataDisponivelMax);
                    System.out.println();
                }


                System.out.println("Definição do ano e do mês pretendido:\n");
                System.out.println("Introduza o ano:");

                int ano = ler.nextInt();
                while (ano < minAno || ano > maxAno) {
                    System.out.println("Ano introduzido inválido ou sem dados.\nVolte a inserir o ano:");
                    ano = ler.nextInt();
                }

                System.out.println("Introduza o mês");

                do {
                    int mes = ler.nextInt();
                    while (mes < minMes || mes > maxMes) {
                        System.out.println("Mês introduzido inválido ou sem dados.\nVolte a inserir o mês");
                        mes = ler.nextInt();
                    }


                    String anoString = Integer.toString(ano);
                    String mesString = Integer.toString(mes);


                    for (int caracter = 0; caracter < mesString.length(); caracter++) {
                        if (mesString.charAt(caracter) != ' ') {
                            contadorCaracteres++;
                        }
                    }
                    if (contadorCaracteres < 2) {
                        mesString = "0" + mesString;
                    }

                    String dataString = anoString + mesString;

                    valorMenu0_1_1_4AnoMes = Integer.parseInt(dataString);

                    switch (matrizDadosMenu[1][0]) {
                        case 1:
                            for (int linha = 0; linha < AppCovanalytics.contaLinhasDadosDiaFinal; linha++) {
                                int temporario = AppCovanalytics.dadosCovidDiaFinal[linha][0];
                                String temporarioString = Integer.toString(temporario);
                                temporarioString = temporarioString.substring(0, 6);
                                temporario = Integer.parseInt(temporarioString);
                                if (valorMenu0_1_1_4AnoMes == temporario) {
                                    verificador++;
                                }
                            }

                        case 2:
                            for (int linha = 0; linha < AppCovanalytics.contaLinhasDadosAcumuladosFinal; linha++) {
                                int temporario = AppCovanalytics.dadosCovidAcumuladosFinal[linha][0];
                                String temporarioString = Integer.toString(temporario);
                                temporarioString = temporarioString.substring(0, 6);
                                temporario = Integer.parseInt(temporarioString);
                                if (valorMenu0_1_1_4AnoMes == temporario) {
                                    verificador++;
                                }
                            }
                    }

                    if (verificador == 0) {
                        System.out.println("Mês introduzido inválido ou sem dados.\nVolte a inserir o mês");
                    }

                    contadorCaracteres = 0;

                } while (verificador == 0);

                matrizDadosMenu[3][0] = valorMenu0_1_1_4AnoMes;

                limparConsola();
                ImprimirRelatorios.gerarRelatorioNumeroCasos_Mes(matrizDadosMenu);


                break;

            case 0:
                abrirMenu0();
                break;
        }

        System.out.println();
        System.out.println("======================================");
        System.out.println("|    9 - Exportar o relatório        |");
        System.out.println("|                                    |");
        System.out.println("|    0 - Regressar ao menu principal |");
        System.out.println("======================================\n");

        valorMenu0_1_1_4 = ler.nextInt();

        while (valorMenu0_1_1_4 != 9 && valorMenu0_1_1_4 != 0) {
            System.out.println("Opção inválida, introduza novamente a opção pretendida");
            valorMenu0_1_1_4 = ler.nextInt();
        }

        switch (valorMenu0_1_1_4) {
            case 0:
                abrirMenu0();
                break;
            case 9:
                ExportarRelatorios.exportarRelatorioNumeroCasos_Mes(matrizDadosMenu);
                System.out.println("Ficheiro gerado com sucesso!");
                abrirMenu0();
                break;
        }

    }


    //****************************************************************************************************************
    //*****Metodo para carregar o Menu 0.1.1.5 [Consulta de relatórios - modo visualizacao - comparacao periodos] ****
    //****************************************************************************************************************


    public static void abrirMenu0_1_1_5() throws FileNotFoundException {

        int valorMenu0_1_1_5DataInicial1, valorMenu0_1_1_5DataFinal1, valorMenu0_1_1_5DataInicial2, valorMenu0_1_1_5DataFinal2;

        limparConsola();

        System.out.println();
        System.out.println("======================================");
        System.out.println("|       Comparação de Periodos       |");
        System.out.println("|                                    |");
        System.out.println("|    1 - Definição de datas          |");
        System.out.println("|                                    |");
        System.out.println("|                                    |");
        System.out.println("|    0 - Regressar ao menu principal |");
        System.out.println("======================================\n");

        System.out.println("Introduza a opção desejada\n");

        int valorMenu0_1_1_5 = ler.nextInt();

        while (valorMenu0_1_1_5 != 1 && valorMenu0_1_1_5 != 0) {
            System.out.println("Opção inválida, introduza novamente a opção pretendida");
            valorMenu0_1_1_5 = ler.nextInt();
        }

        switch (valorMenu0_1_1_5) {
            case 1:

                System.out.println("Período de dados carregados disponiveis para consulta:");
                if (matrizDadosMenu[1][0] == 1) {
                    LocalDate dataDisponivelMin = LerFicheiroDados.converterData(LerFicheiroDados.calcularLimitesDatasMatrizDados(AppCovanalytics.dadosCovidDiaFinal, AppCovanalytics.contaLinhasDadosDiaFinal)[0]);
                    LocalDate dataDisponivelMax = LerFicheiroDados.converterData(LerFicheiroDados.calcularLimitesDatasMatrizDados(AppCovanalytics.dadosCovidDiaFinal, AppCovanalytics.contaLinhasDadosDiaFinal)[1]);
                    System.out.println("Desde: " + dataDisponivelMin + " Até: " + dataDisponivelMax);
                    System.out.println();
                }

                if (matrizDadosMenu[1][0] == 2) {
                    LocalDate dataDisponivelMin = LerFicheiroDados.converterData(LerFicheiroDados.calcularLimitesDatasMatrizDados(AppCovanalytics.dadosCovidAcumuladosFinal, AppCovanalytics.contaLinhasDadosAcumuladosFinal)[0]);
                    LocalDate dataDisponivelMax = LerFicheiroDados.converterData(LerFicheiroDados.calcularLimitesDatasMatrizDados(AppCovanalytics.dadosCovidAcumuladosFinal, AppCovanalytics.contaLinhasDadosAcumuladosFinal)[1]);
                    System.out.println("Desde: " + dataDisponivelMin + " Até: " + dataDisponivelMax);
                    System.out.println();
                }


                System.out.println("Definição do período inicial:");

                do {
                    System.out.println("Introduza a data inicial (AAAA-MM-DD):\n");

                    Scanner ler = new Scanner(System.in);

                    String dataInicialString1 = ler.nextLine();

                    valorMenu0_1_1_5DataInicial1 = LerFicheiroDados.conversorData(dataInicialString1);

                    int verificadorDataInicial1 = LerFicheiroDados.verificadorDatas(valorMenu0_1_1_5DataInicial1, AppCovanalytics.dadosCovidDiaFinal, AppCovanalytics.dadosCovidAcumuladosFinal, matrizDadosMenu);


                    while (verificadorDataInicial1 == 0) {
                        System.out.println();
                        System.out.println("Data inicial inválida ou sem dados.");
                        System.out.println();
                        System.out.println("Volte a inserir a data inicial:");

                        dataInicialString1 = ler.nextLine();

                        valorMenu0_1_1_5DataInicial1 = LerFicheiroDados.conversorData(dataInicialString1);

                        verificadorDataInicial1 = LerFicheiroDados.verificadorDatas(valorMenu0_1_1_5DataInicial1, AppCovanalytics.dadosCovidDiaFinal, AppCovanalytics.dadosCovidAcumuladosFinal, matrizDadosMenu);
                    }


                    System.out.println();
                    System.out.println("Introduza a data final (AAAA-MM-DD):");

                    String dataFinalString1 = ler.nextLine();

                    valorMenu0_1_1_5DataFinal1 = LerFicheiroDados.conversorData(dataFinalString1);

                    int verificadorDataFinal1 = LerFicheiroDados.verificadorDatas(valorMenu0_1_1_5DataFinal1, AppCovanalytics.dadosCovidDiaFinal, AppCovanalytics.dadosCovidAcumuladosFinal, matrizDadosMenu);

                    while (verificadorDataFinal1 == 0) {
                        System.out.println();
                        System.out.println("Data final inválida ou sem dados.");
                        System.out.println("Volte a inserir a data final:");

                        dataFinalString1 = ler.nextLine();

                        valorMenu0_1_1_5DataFinal1 = LerFicheiroDados.conversorData(dataFinalString1);

                        verificadorDataFinal1 = LerFicheiroDados.verificadorDatas(valorMenu0_1_1_5DataFinal1, AppCovanalytics.dadosCovidDiaFinal, AppCovanalytics.dadosCovidAcumuladosFinal, matrizDadosMenu);
                    }

                    System.out.println("\nDefinição do periodo final:");

                    System.out.println("Introduza a data inicial (AAAA-MM-DD):\n");

                    String dataInicialString2 = ler.nextLine();

                    valorMenu0_1_1_5DataInicial2 = LerFicheiroDados.conversorData(dataInicialString2);

                    int verificadorDataInicial2 = LerFicheiroDados.verificadorDatas(valorMenu0_1_1_5DataInicial2, AppCovanalytics.dadosCovidDiaFinal, AppCovanalytics.dadosCovidAcumuladosFinal, matrizDadosMenu);


                    while (verificadorDataInicial2 == 0) {
                        System.out.println();
                        System.out.println("Data inicial inválida ou sem dados.");
                        System.out.println();
                        System.out.println("Volte a inserir a data inicial:");

                        dataInicialString2 = ler.nextLine();

                        valorMenu0_1_1_5DataInicial2 = LerFicheiroDados.conversorData(dataInicialString2);

                        verificadorDataInicial2 = LerFicheiroDados.verificadorDatas(valorMenu0_1_1_5DataInicial2, AppCovanalytics.dadosCovidDiaFinal, AppCovanalytics.dadosCovidAcumuladosFinal, matrizDadosMenu);
                    }


                    System.out.println();
                    System.out.println("Introduza a data final (AAAA-MM-DD):");

                    String dataFinalString2 = ler.nextLine();

                    valorMenu0_1_1_5DataFinal2 = LerFicheiroDados.conversorData(dataFinalString2);

                    int verificadorDataFinal2 = LerFicheiroDados.verificadorDatas(valorMenu0_1_1_5DataFinal2, AppCovanalytics.dadosCovidDiaFinal, AppCovanalytics.dadosCovidAcumuladosFinal, matrizDadosMenu);

                    while (verificadorDataFinal2 == 0) {
                        System.out.println();
                        System.out.println("Data final inválida ou sem dados.");
                        System.out.println();
                        System.out.println("Volte a inserir a data final:");

                        dataFinalString2 = ler.nextLine();

                        valorMenu0_1_1_5DataFinal2 = LerFicheiroDados.conversorData(dataFinalString2);

                        verificadorDataFinal2 = LerFicheiroDados.verificadorDatas(valorMenu0_1_1_5DataFinal2, AppCovanalytics.dadosCovidDiaFinal, AppCovanalytics.dadosCovidAcumuladosFinal, matrizDadosMenu);
                    }


                    if (valorMenu0_1_1_5DataInicial1 > valorMenu0_1_1_5DataFinal1 || valorMenu0_1_1_5DataInicial2 > valorMenu0_1_1_5DataFinal2) {
                        System.out.println();
                        System.out.println("Data inicial introduzida é superior à data final!");
                        System.out.println("Introduzir novas datas:");
                        System.out.println();
                    }

                } while (valorMenu0_1_1_5DataInicial1 > valorMenu0_1_1_5DataFinal1 || valorMenu0_1_1_5DataInicial2 > valorMenu0_1_1_5DataFinal2);


                matrizDadosMenu[3][0] = valorMenu0_1_1_5DataInicial1;
                matrizDadosMenu[3][1] = valorMenu0_1_1_5DataFinal1;
                matrizDadosMenu[3][2] = valorMenu0_1_1_5DataInicial2;
                matrizDadosMenu[3][3] = valorMenu0_1_1_5DataFinal2;

                limparConsola();
                ImprimirRelatorios.gerarRelatorioComparativo_Dias(matrizDadosMenu);
                break;

            case 0:
                abrirMenu0();
                break;
        }

        System.out.println("\n======================================");
        System.out.println("|    9 - Exportar o relatório        |");
        System.out.println("|                                    |");
        System.out.println("|    0 - Regressar ao menu principal |");
        System.out.println("======================================\n");

        valorMenu0_1_1_5 = ler.nextInt();

        while (valorMenu0_1_1_5 != 9 && valorMenu0_1_1_5 != 0) {
            System.out.println("Opção inválida, introduza novamente a opção pretendida");
            valorMenu0_1_1_5 = ler.nextInt();
        }

        switch (valorMenu0_1_1_5) {
            case 0:
                abrirMenu0();
                break;
            case 9:
                ExportarRelatorios.exportarRelatorioComparativo_Dias(matrizDadosMenu);
                System.out.println("Ficheiro gerado com sucesso!");
                abrirMenu0();
                break;
        }

    }

    //****************************************************************************************************************
    //*****Metodo para carregar o Menu 0.3 [Carregar novos dados] ****************************************************
    //****************************************************************************************************************


    public static void abrirMenu0_3() throws FileNotFoundException {

        limparConsola();

        System.out.println();
        System.out.println("============================================");
        System.out.println("|             Tipo de dados                |");
        System.out.println("|                                          |");
        System.out.println("|  Que tipo de dados pretende carregar?    |");
        System.out.println("|                                          |");
        System.out.println("|                                          |");
        System.out.println("|    1 - Dados diários                     |");
        System.out.println("|    2 - Dados acumulados                  |");
        System.out.println("|    3 - Matriz transições                 |");
        System.out.println("|                                          |");
        System.out.println("|                                          |");
        System.out.println("|    9 - Info sobre dados carregados       |");
        System.out.println("|    0 - Regressar ao menu principal       |");
        System.out.println("============================================\n");
        System.out.println("Introduza a opção desejada\n");


        int valorMenu0_3 = ler.nextInt();

        while (valorMenu0_3 != 1 && valorMenu0_3 != 2 && valorMenu0_3 != 0 && valorMenu0_3 != 3 && valorMenu0_3 != 9) {
            System.out.println("Opção inválida, introduza novamente a opção pretendida");
            valorMenu0_3 = ler.nextInt();
        }

        matrizDadosMenu[0][1] = valorMenu0_3;


        switch (valorMenu0_3) {
            case 1:
                System.out.println("Indique o ficheiro com a sua localização:");
                nomeficheiro = ler.next();
                if (LerFicheiroDados.verificarExistenciaFicheiro()) {
                    if (LerFicheiroDados.verificarTipodeFicheiroDia()) {
                        System.out.println("Ficheiro encontrado e estrutura validada! Dados carregados com sucesso!");
                        LerFicheiroDados.lerFicheiroDeDadosDiaTemp();
                        LerFicheiroDados.transferirDadosMatrizDiaFinal(AppCovanalytics.dadosCovidDiaTemp, AppCovanalytics.dadosCovidDiaFinal);
                        LerFicheiroDados.apagarDadosDiaTemp();
                    } else {
                        System.out.println("Ficheiro não encontrado ou não corresponde à tipologia definida para importação de dados!");
                    }
                } else {
                    System.out.println("Ficheiro não encontrado ou não corresponde à tipologia definida para importação de dados!");
                }
                break;

            case 2:
                System.out.println("Indique o ficheiro com a sua localização:");
                nomeficheiro = ler.next();
                if (LerFicheiroDados.verificarExistenciaFicheiro()) {
                    if (LerFicheiroDados.verificarTipodeFicheiroAcumulado()) {
                        System.out.println("Ficheiro encontrado e estrutura validada! Dados carregados com sucesso!");
                        LerFicheiroDados.lerFicheiroDeDadosAcumuladosTemp();
                        LerFicheiroDados.transferirDadosMatrizAcumFinal(AppCovanalytics.dadosCovidAcumuladosTemp, AppCovanalytics.dadosCovidAcumuladosFinal);
                        LerFicheiroDados.apagarDadosAcumuladosTemp();
                    } else {
                        System.out.println("Ficheiro não encontrado ou não corresponde à tipologia definida para importação de dados!");
                    }
                } else {
                    System.out.println("Ficheiro não encontrado ou não corresponde à tipologia definida para importação de dados!");
                }
                break;

            case 3:
                System.out.println("Indique o ficheiro com a sua localização:");
                nomeficheiro = ler.next();
                if (LerFicheiroDados.verificarExistenciaFicheiro()) {
                    if (LerFicheiroDados.verificarTipodeFicheiroTransicoes()) {
                        System.out.println("Ficheiro encontrado e estrutura validada! Dados carregados com sucesso!");
                        LerFicheiroDados.lerMatrizTransicao();
                    } else {
                        System.out.println("Ficheiro não encontrado ou não corresponde à tipologia definida para importação de dados!");
                    }
                } else {
                    System.out.println("Ficheiro não encontrado ou não corresponde à tipologia definida para importação de dados!");
                }
                break;
            case 9:
                abrirMenu0_3_9();
                break;
            case 0:
                abrirMenu0();
                break;
        }

        if (valorMenu0_3 != 9) {

            System.out.println();
            System.out.println("======================================");
            System.out.println("|    9 - Carregar novo ficheiro      |");
            System.out.println("|                                    |");
            System.out.println("|    0 - Regressar ao menu principal |");
            System.out.println("======================================\n");

            valorMenu0_3 = ler.nextInt();

            while (valorMenu0_3 != 9 && valorMenu0_3 != 0) {
                System.out.println("Opção inválida, introduza novamente a opção pretendida");
                valorMenu0_3 = ler.nextInt();
            }

            switch (valorMenu0_3) {
                case 0:
                    abrirMenu0();
                    break;
                case 9:
                    limparConsola();
                    abrirMenu0_3();
                    break;
            }
        }
    }

    //****************************************************************************************************************
    //*****Metodo para carregar o Menu 0.3.9 [Carregar informação sobre as matrizes] *********************************
    //****************************************************************************************************************


    public static void abrirMenu0_3_9() throws FileNotFoundException {

        limparConsola();

        System.out.println();
        System.out.println("============================================================");
        System.out.println("|         Informações sobre os dados carregados            |");
        System.out.println("|                                                          |");
        System.out.println("|         Matriz de dados diários:                         |");
        if (AppCovanalytics.contaLinhasDadosDiaFinal == 0) {
            System.out.println("|       Não existem dados carregados em sistema            |");
        } else {
            System.out.println("| Número linhas:" + AppCovanalytics.contaLinhasDadosDiaFinal + "   Data do dado mais recente:" + LerFicheiroDados.converterData(LerFicheiroDados.calcularLimitesDatasMatrizDados(AppCovanalytics.dadosCovidDiaFinal, AppCovanalytics.contaLinhasDadosDiaFinal)[1]) + " |");
        }
        System.out.println("|                                                          |");
        System.out.println("|         Matriz de dados acumulados:                      |");
        if (AppCovanalytics.contaLinhasDadosAcumuladosFinal == 0) {
            System.out.println("|       Não existem dados carregados em sistema            |");
        } else {
            System.out.println("|  Número linhas:" + AppCovanalytics.contaLinhasDadosAcumuladosFinal + "   Data do dado mais recente:" + LerFicheiroDados.converterData(LerFicheiroDados.calcularLimitesDatasMatrizDados(AppCovanalytics.dadosCovidAcumuladosFinal, AppCovanalytics.contaLinhasDadosAcumuladosFinal)[1]) + " |");
        }
        System.out.println("|                                                          |");
        System.out.println("|                                                          |");
        System.out.println("|                                                          |");
        System.out.println("|    9 - Regressar ao menu anterior                        |");
        System.out.println("============================================================\n");


        int valorMenu0_3_9 = ler.nextInt();

        while (valorMenu0_3_9 != 9) {
            System.out.println("Opção inválida, introduza novamente a opção pretendida");
            valorMenu0_3_9 = ler.nextInt();
        }
        abrirMenu0_3();

    }

//****************************************************************************************************************
    //*****Metodo para carregar o Menu de Cadeias de Markov **********************************************************
    //****************************************************************************************************************

    public static void abrirMenuVoltarPrevisional() throws FileNotFoundException {
        limparConsola();

        Scanner teclado = new Scanner(System.in);

        System.out.println("\n====================================================");
        System.out.println("|                                                  |");
        System.out.println("|    1 - Voltar ao Menu Previsional                |");
        System.out.println("|                                                  |");
        System.out.println("|    0 - Voltar ao Menu Principal                  |");
        System.out.println("====================================================\n");
        System.out.println("Introduza a opção desejada\n");

        int selecao = teclado.nextInt();
        while (selecao != 1 && selecao != 0) {
            System.out.println("Opção inválida, introduza novamente a opção pretendida");
            selecao = teclado.nextInt();
        }
        switch (selecao) {
            case 1:
                AbrirMenus.menuPrevisional();
                break;
            case 0:
                AbrirMenus.abrirMenu0();
                break;
        }
    }
    public static void menuPrevisional() throws FileNotFoundException {
        Scanner teclado = new Scanner(System.in);

        System.out.println("\n=================================================");
        System.out.println("|               Menu Previsional                |");
        System.out.println("=================================================");
        System.out.println("|                                               |");
        System.out.println("|    1 - Previsão de Transição entre Estados    |");
        System.out.println("|    2 - Evolução até Óbito                     |");
        System.out.println("|                                               |");
        System.out.println("|                                               |");
        System.out.println("|    0 - Voltar ao Menu Principal               |");
        System.out.println("=================================================\n");
        System.out.println("Introduza a opção desejada\n");

        int selecao = teclado.nextInt();
        while (selecao != 1 && selecao != 2 && selecao != 0) {
            System.out.println("Opção inválida, introduza novamente a opção pretendida");
            selecao = teclado.nextInt();
        }
        switch (selecao) {
            case 1:
                AbrirMenus.matrizDadosMenu[1][0] = 1;
                MetodoPrevisional.cadeiaDeMarkovUserInterface();
                break;
            case 2:
                AbrirMenus.matrizDadosMenu[1][0] = 2;
                MetodoPrevisional.fatorizacaoDeCroutUserInterface();
                break;
            case 0:
                AbrirMenus.abrirMenu0();
                break;
        }

        System.out.println("\n======================================");
        System.out.println("|    9 - Exportar o relatório        |");
        System.out.println("|                                    |");
        System.out.println("|    0 - Regressar ao menu principal |");
        System.out.println("======================================\n");

        int opcao = ler.nextInt();

        while (opcao != 9 && opcao != 0) {
            System.out.println("Opção inválida, introduza novamente a opção pretendida");
            opcao = ler.nextInt();
        }

        switch (opcao) {
            case 0:
                abrirMenu0();
                break;
            case 9:
                ExportarRelatorios.exportarRelatorioPrevisional(matrizDadosMenu);
                System.out.println("Ficheiro gerado com sucesso!");
                abrirMenu0();
                break;
        }

    }


//****************************************************************************************************************
//*****Metodo para limpar o ecran ********************************************************************************
//****************************************************************************************************************

    public static void limparConsola() {

        System.out.print("\033[H\033[2J");

    }
}
















